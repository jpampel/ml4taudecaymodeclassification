#include "MLTreeMaker.h"

// Tracks
#include "TrkTrack/Track.h"
#include "TrkParameters/TrackParameters.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

//Jets
#include "xAODJet/JetTypes.h"
#include "xAODJet/JetContainer.h"

// Tau Truth Matching Tool Christina
#include "TauAnalysisTools/ITauTruthMatchingTool.h"
#include "xAODTau/TauJet.h"
#include "TauAnalysisTools/HelperFunctions.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"

#include "AthLinks/ElementLink.h"
#include "xAODTau/TauxAODHelpers.h"

// Track selection
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"

// Extrapolation to the calo
#include "TrkCaloExtension/CaloExtension.h"
#include "TrkCaloExtension/CaloExtensionCollection.h"
#include "TrkParametersIdentificationHelpers/TrackParametersIdHelper.h"
#include "CaloDetDescr/CaloDepthTool.h"
#include "Identifier/IdentifierHash.h"

// Calo and cell information
#include "TileEvent/TileContainer.h"
#include "TileIdentifier/TileTBID.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloTrackingGeometry/ICaloSurfaceHelper.h"
#include "TrkSurfaces/DiscSurface.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include "CaloEvent/CaloClusterContainer.h"
#include "CaloEvent/CaloCluster.h"
#include "CaloUtils/CaloClusterSignalState.h"
#include "CaloEvent/CaloClusterCellLinkContainer.h"
#include "xAODCaloEvent/CaloClusterChangeSignalState.h"
#include "CaloSimEvent/CaloCalibrationHitContainer.h"
// Other xAOD incudes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODEventShape/EventShape.h"

#include <string>
#include <vector>
#include <cmath>
#include <utility>
#include <limits>
#include <map>

MLTreeMaker::MLTreeMaker( const std::string& name, ISvcLocator* pSvcLocator ) :
  AthHistogramAlgorithm( name, pSvcLocator ),
  m_doClusters(true),
  m_doClusterCells(true),
  m_doCalibHits(true),
  m_doCalibHitsPerCell(true),
  m_numClusterTruthAssoc(5),
  m_doClusterMoments(true),
  m_doUncalibratedClusters(true),
  m_doTracking(false),
  m_doJets(false),
  m_doTaus(true),
  m_doCellCorrection(false),
  m_doEventCleaning(false),
  m_doPileup(false),
  m_doShapeEM(false),
  m_doShapeLC(false),
  m_doEventTruth(false),
  m_doTruthParticles(false),
  m_keepOnlyStableTruthParticles(true),
  m_keepG4TruthParticles(false),
  m_prefix(""),
  m_eventInfoContainerName("EventInfo"),
  m_truthContainerName("TruthParticles"),
  m_vxContainerName("PrimaryVertices"),
  m_trackContainerName("InDetTrackParticles"),
  m_caloClusterContainerName("CaloCalTopoClusters"),
  m_extrapolator("Trk::Extrapolator"),
  m_theTrackExtrapolatorTool("Trk::ParticleCaloExtensionTool"),
  m_trkSelectionTool("InDet::InDetTrackSelectionTool/TrackSelectionTool", this),
  m_trackParametersIdHelper(new Trk::TrackParametersIdHelper),
  m_surfaceHelper("CaloSurfaceHelper/CaloSurfaceHelper"),
  m_tileTBID(0),
  m_clusterE_min(0.),
  m_clusterE_max(1e4),
  m_clusterEtaAbs_max(2.5),
  m_cellE_thres(0.005)  // 5 MeV threshold
{
  declareProperty("Clusters", m_doClusters);
  declareProperty("ClusterCells", m_doClusterCells);
  declareProperty("ClusterCalibHits", m_doCalibHits);
  declareProperty("ClusterCalibHitsPerCell", m_doCalibHitsPerCell);
  declareProperty("CalibrationHitContainerNames",m_CalibrationHitContainerKeys);
  declareProperty("ClusterMoments", m_doClusterMoments);
  declareProperty("UncalibratedClusters", m_doUncalibratedClusters);
  declareProperty("ClusterEmin", m_clusterE_min);
  declareProperty("ClusterEmax", m_clusterE_max);
  declareProperty("ClusterEtaAbsmax", m_clusterEtaAbs_max);

  declareProperty("Tracking", m_doTracking);
  declareProperty("Jets", m_doJets);
  declareProperty("Taus", m_doTaus);
  declareProperty("EventCleaning", m_doEventCleaning);
  declareProperty("Pileup", m_doPileup);
  declareProperty("ShapeEM", m_doShapeEM);
  declareProperty("ShapeLC", m_doShapeLC);
  declareProperty("EventTruth", m_doEventTruth);
  declareProperty("TruthParticles", m_doTruthParticles);
  declareProperty("OnlyStableTruthParticles", m_keepOnlyStableTruthParticles);
  declareProperty("G4TruthParticles", m_keepG4TruthParticles);
  declareProperty("Prefix", m_prefix);
  declareProperty("EventContainer", m_eventInfoContainerName);
  declareProperty("TrackContainer", m_trackContainerName);
  declareProperty("CaloClusterContainer", m_caloClusterContainerName);
  declareProperty("JetContainers", m_jetContainerNames);
  declareProperty("Extrapolator", m_extrapolator);
  declareProperty("TheTrackExtrapolatorTool", m_theTrackExtrapolatorTool);
  declareProperty("TrackSelectionTool", m_trkSelectionTool);
}

MLTreeMaker::~MLTreeMaker() {}

StatusCode MLTreeMaker::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  if (m_prefix=="") {
    ATH_MSG_WARNING("No decoration prefix name provided");
  }

  // const xAOD::EventInfo* eventInfo(nullptr);
  // CHECK( evtStore()->retrieve(eventInfo, m_eventInfoContainerName) );
  // m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) );

  ATH_CHECK( m_extrapolator.retrieve() );
  ATH_CHECK( m_theTrackExtrapolatorTool.retrieve() );
  ATH_CHECK( m_surfaceHelper.retrieve() );
  ATH_CHECK( m_trkSelectionTool.retrieve() );
  // Get the test beam identifier for the MBTS
  ATH_CHECK( detStore()->retrieve(m_tileTBID) );

  // initialize tauTruthMatchingTool
  //m_tauTruthMatchingTool.declarePropertyFor( this, "TauTruthMatchingTool", "The TTMT" );
  m_tauTruthMatchingTool.setTypeAndName("TauAnalysisTools::TauTruthMatchingTool/TauTruthMatchingTool");
  ATH_CHECK(m_tauTruthMatchingTool.retrieve());
  //m_tauTruthMatchingTool.reset(new TauAnalysisTools::TauTruthMatchingTool("T2MT"));
  m_tauTruthMatchingTool->msg().setLevel( MSG::INFO );
  ATH_CHECK(m_tauTruthMatchingTool->initialize());
  ATH_CHECK(m_tauTruthMatchingTool->setProperty("WriteTruthTaus",false)); 
  ATH_CHECK(m_tauTruthMatchingTool->setProperty("WriteVisibleChargedFourMomentum",true));
  ATH_CHECK(m_tauTruthMatchingTool->setProperty("WriteVisibleNeutralFourMomentum",true)); 

  // Setup the event level TTree and its branches
  CHECK( book(TTree("EventTree", "EventTree")) );
  m_eventTree = tree("EventTree");
  
  // Event info 
  m_eventTree->Branch("runNumber",             &m_runNumber,      "runNumber/I");
  m_eventTree->Branch("eventNumber",           &m_eventNumber,    "eventNumber/LI");
  m_eventTree->Branch("lumiBlock",             &m_lumiBlock,      "lumiBlock/I");
  m_eventTree->Branch("coreFlags",             &m_coreFlags,      "coreFlags/i");
  // if (m_isMC ) {
  m_eventTree->Branch("mcEventNumber",       &m_mcEventNumber,  "mcEventNumber/I");
  m_eventTree->Branch("mcChannelNumber",     &m_mcChannelNumber,"mcChannelNumber/I");
  m_eventTree->Branch("mcEventWeight",       &m_mcEventWeight,  "mcEventWeight/F");
  // } else {
  //   m_eventTree->Branch("bcid",                &m_bcid,           "bcid/I");
  //   m_eventTree->Branch("prescale_DataWeight", &m_prescale_DataWeight,  "prescale_DataWeight/F");
  // }
  if (m_doEventCleaning ) 
  {
    m_eventTree->Branch("timeStamp",           &m_timeStamp,         "timeStamp/i");
    m_eventTree->Branch("timeStampNSOffset",   &m_timeStampNSOffset, "timeStampNSOffset/i");
    m_eventTree->Branch("TileError",           &m_TileError,         "TileError/O");
    m_eventTree->Branch("SCTError",            &m_SCTError,          "SCTError/O");
    m_eventTree->Branch("LArError",            &m_LArError,          "LArError/O");
    m_eventTree->Branch("TileFlags",           &m_TileFlags,         "TileFlags/i");
    m_eventTree->Branch("SCTFlags",            &m_SCTFlags,          "SCTFlags/i");
    m_eventTree->Branch("LArFlags",            &m_LArFlags,          "LArFlags/i");
  }
  if (m_doPileup ) 
  {
      m_eventTree->Branch("NPV",                 &m_npv,            "NPV/I");
      m_eventTree->Branch("actualInteractionsPerCrossing",  &m_actualMu,  "actualInteractionsPerCrossing/F");
      m_eventTree->Branch("averageInteractionsPerCrossing", &m_averageMu, "averageInteractionsPerCrossing/F");
      m_eventTree->Branch("weight_pileup",       &m_weight_pileup,  "weight_pileup/F");
      // if (m_isMC){
      m_eventTree->Branch("correct_mu"       , &m_correct_mu       ,"correct_mu/F"       );          
      m_eventTree->Branch("rand_run_nr"      , &m_rand_run_nr      ,"rand_run_nr/I"      );         
      m_eventTree->Branch("rand_lumiblock_nr", &m_rand_lumiblock_nr,"rand_lumiblock_nr/I");  
      // }
    }
  if (m_doShapeEM ) m_eventTree->Branch("rhoEM",               &m_rhoEM,            "rhoEM/D");
  if (m_doShapeLC ) m_eventTree->Branch("rhoLC",               &m_rhoLC,            "rhoLC/D");
  if (m_doEventTruth /*&& m_isMC */ ) 
  {
    m_eventTree->Branch("pdgId1",              &m_pdgId1,        "pdgId1/I" );
    m_eventTree->Branch("pdgId2",              &m_pdgId2,        "pdgId2/I" );
    m_eventTree->Branch("pdfId1",              &m_pdfId1,        "pdfId1/I" );
    m_eventTree->Branch("pdfId2",              &m_pdfId2,        "pdfId2/I" );
    m_eventTree->Branch("x1",                  &m_x1,            "x1/F"  );
    m_eventTree->Branch("x2",                  &m_x2,            "x2/F"  );
    // m_eventTree->Branch("scale",               &m_scale,         "scale/F");
    // m_eventTree->Branch("q",                   &m_q,             "q/F");
    // m_eventTree->Branch("pdf1",                &m_pdf1,          "pdf1/F");
    // m_eventTree->Branch("pdf2",                &m_pdf2,          "pdf2/F");
    m_eventTree->Branch("xf1",                 &m_xf1,           "xf1/F");
    m_eventTree->Branch("xf2",                 &m_xf2,           "xf2/F");
  } 

  // Truth particles
  if(m_doTruthParticles)
  {
    m_eventTree->Branch("nTruthPart",          &m_nTruthPart, "nTruthPart/I");
    m_eventTree->Branch("G4PreCalo_n_EM",         &m_G4PreCalo_n_EM);
    m_eventTree->Branch("G4PreCalo_E_EM",      &m_G4PreCalo_E_EM);
    m_eventTree->Branch("G4PreCalo_n_Had",         &m_G4PreCalo_n_Had);
    m_eventTree->Branch("G4PreCalo_E_Had",      &m_G4PreCalo_E_Had);
    m_eventTree->Branch("truthVertexX",      &m_truthVertexX);
    m_eventTree->Branch("truthVertexY",      &m_truthVertexY);
    m_eventTree->Branch("truthVertexZ",      &m_truthVertexZ);
    m_eventTree->Branch("truthPartPdgId",               &m_truthPartPdgId);
    m_eventTree->Branch("truthPartStatus",              &m_truthPartStatus);
    m_eventTree->Branch("truthPartBarcode",             &m_truthPartBarcode);
    m_eventTree->Branch("truthPartPt",         &m_truthPartPt);
    m_eventTree->Branch("truthPartE",          &m_truthPartE);
    m_eventTree->Branch("truthPartMass",       &m_truthPartMass);
    m_eventTree->Branch("truthPartEta",        &m_truthPartEta);
    m_eventTree->Branch("truthPartPhi",        &m_truthPartPhi);
  }

  // Track variables
  if (m_doTracking)
  {
    m_eventTree->Branch("nTrack",              &m_nTrack, "nTrack/I");
    m_eventTree->Branch("trackPt",             &m_trackPt);
    m_eventTree->Branch("trackP",              &m_trackP);
    m_eventTree->Branch("trackMass",           &m_trackMass);
    m_eventTree->Branch("trackEta",            &m_trackEta);
    m_eventTree->Branch("trackPhi",            &m_trackPhi);

    // Track quality variables
    m_eventTree->Branch("trackNumberOfPixelHits",   &m_trackNumberOfPixelHits);
    m_eventTree->Branch("trackNumberOfSCTHits",   &m_trackNumberOfSCTHits);
    m_eventTree->Branch("trackNumberOfPixelDeadSensors",   &m_trackNumberOfPixelDeadSensors);
    m_eventTree->Branch("trackNumberOfSCTDeadSensors",   &m_trackNumberOfSCTDeadSensors);
    m_eventTree->Branch("trackNumberOfPixelSharedHits",   &m_trackNumberOfPixelSharedHits);
    m_eventTree->Branch("trackNumberOfSCTSharedHits",   &m_trackNumberOfSCTSharedHits);
    m_eventTree->Branch("trackNumberOfPixelHoles",   &m_trackNumberOfPixelHoles);
    m_eventTree->Branch("trackNumberOfSCTHoles",   &m_trackNumberOfSCTHoles);
    m_eventTree->Branch("trackNumberOfInnermostPixelLayerHits",   &m_trackNumberOfInnermostPixelLayerHits);
    m_eventTree->Branch("trackNumberOfNextToInnermostPixelLayerHits",   &m_trackNumberOfNextToInnermostPixelLayerHits);
    m_eventTree->Branch("trackExpectInnermostPixelLayerHit",   &m_trackExpectInnermostPixelLayerHit);
    m_eventTree->Branch("trackExpectNextToInnermostPixelLayerHit",   &m_trackExpectNextToInnermostPixelLayerHit);
    m_eventTree->Branch("trackNumberOfTRTHits",   &m_trackNumberOfTRTHits);
    m_eventTree->Branch("trackNumberOfTRTOutliers",   &m_trackNumberOfTRTOutliers);
    m_eventTree->Branch("trackChiSquared",   &m_trackChiSquared);
    m_eventTree->Branch("trackNumberDOF",   &m_trackNumberDOF);
    m_eventTree->Branch("trackD0",   &m_trackD0);
    m_eventTree->Branch("trackZ0",   &m_trackZ0);

    // Track extrapolation
    // Presampler
    m_eventTree->Branch("trackEta_PreSamplerB",  &m_trackEta_PreSamplerB);
    m_eventTree->Branch("trackPhi_PreSamplerB",  &m_trackPhi_PreSamplerB);
    m_eventTree->Branch("trackEta_PreSamplerE",  &m_trackEta_PreSamplerE);
    m_eventTree->Branch("trackPhi_PreSamplerE",  &m_trackPhi_PreSamplerE);
    // LAr EM Barrel layers
    m_eventTree->Branch("trackEta_EMB1",         &m_trackEta_EMB1); 
    m_eventTree->Branch("trackPhi_EMB1",         &m_trackPhi_EMB1); 
    m_eventTree->Branch("trackEta_EMB2",         &m_trackEta_EMB2); 
    m_eventTree->Branch("trackPhi_EMB2",         &m_trackPhi_EMB2); 
    m_eventTree->Branch("trackEta_EMB3",         &m_trackEta_EMB3); 
    m_eventTree->Branch("trackPhi_EMB3",         &m_trackPhi_EMB3); 
    // LAr EM Endcap layers
    m_eventTree->Branch("trackEta_EME1",         &m_trackEta_EME1); 
    m_eventTree->Branch("trackPhi_EME1",         &m_trackPhi_EME1); 
    m_eventTree->Branch("trackEta_EME2",         &m_trackEta_EME2); 
    m_eventTree->Branch("trackPhi_EME2",         &m_trackPhi_EME2); 
    m_eventTree->Branch("trackEta_EME3",         &m_trackEta_EME3); 
    m_eventTree->Branch("trackPhi_EME3",         &m_trackPhi_EME3); 
    // Hadronic Endcap layers
    m_eventTree->Branch("trackEta_HEC0",         &m_trackEta_HEC0); 
    m_eventTree->Branch("trackPhi_HEC0",         &m_trackPhi_HEC0); 
    m_eventTree->Branch("trackEta_HEC1",         &m_trackEta_HEC1); 
    m_eventTree->Branch("trackPhi_HEC1",         &m_trackPhi_HEC1); 
    m_eventTree->Branch("trackEta_HEC2",         &m_trackEta_HEC2); 
    m_eventTree->Branch("trackPhi_HEC2",         &m_trackPhi_HEC2); 
    m_eventTree->Branch("trackEta_HEC3",         &m_trackEta_HEC3); 
    m_eventTree->Branch("trackPhi_HEC3",         &m_trackPhi_HEC3); 
    // Tile Barrel layers
    m_eventTree->Branch("trackEta_TileBar0",     &m_trackEta_TileBar0); 
    m_eventTree->Branch("trackPhi_TileBar0",     &m_trackPhi_TileBar0); 
    m_eventTree->Branch("trackEta_TileBar1",     &m_trackEta_TileBar1); 
    m_eventTree->Branch("trackPhi_TileBar1",     &m_trackPhi_TileBar1); 
    m_eventTree->Branch("trackEta_TileBar2",     &m_trackEta_TileBar2); 
    m_eventTree->Branch("trackPhi_TileBar2",     &m_trackPhi_TileBar2); 
    // Tile Gap layers
    m_eventTree->Branch("trackEta_TileGap1",     &m_trackEta_TileGap1); 
    m_eventTree->Branch("trackPhi_TileGap1",     &m_trackPhi_TileGap1); 
    m_eventTree->Branch("trackEta_TileGap2",     &m_trackEta_TileGap2); 
    m_eventTree->Branch("trackPhi_TileGap2",     &m_trackPhi_TileGap2); 
    m_eventTree->Branch("trackEta_TileGap3",     &m_trackEta_TileGap3); 
    m_eventTree->Branch("trackPhi_TileGap3",     &m_trackPhi_TileGap3); 
    // Tile Extended Barrel layers
    m_eventTree->Branch("trackEta_TileExt0",     &m_trackEta_TileExt0);
    m_eventTree->Branch("trackPhi_TileExt0",     &m_trackPhi_TileExt0);
    m_eventTree->Branch("trackEta_TileExt1",     &m_trackEta_TileExt1);
    m_eventTree->Branch("trackPhi_TileExt1",     &m_trackPhi_TileExt1);
    m_eventTree->Branch("trackEta_TileExt2",     &m_trackEta_TileExt2);
    m_eventTree->Branch("trackPhi_TileExt2",     &m_trackPhi_TileExt2);
  }


  if(m_doJets)
  {
    m_jet_pt.assign(m_jetContainerNames.size(),std::vector<float>());
    m_jet_eta.assign(m_jetContainerNames.size(),std::vector<float>());
    m_jet_phi.assign(m_jetContainerNames.size(),std::vector<float>());
    m_jet_E.assign(m_jetContainerNames.size(),std::vector<float>());
    m_jet_flavor.assign(m_jetContainerNames.size(),std::vector<int>());
    for(unsigned int iColl=0; iColl < m_jetContainerNames.size(); iColl++)
    {
      std::string jet_name(m_jetContainerNames.at(iColl));
      std::stringstream ss;
      ss << jet_name << "Pt";
      m_eventTree->Branch(ss.str().c_str(),&(m_jet_pt[iColl]));
	
      ss.str("");
      ss << jet_name << "Eta";
      m_eventTree->Branch(ss.str().c_str(),&(m_jet_eta[iColl]));

      ss.str("");
      ss << jet_name << "Phi";
      m_eventTree->Branch(ss.str().c_str(),&(m_jet_phi[iColl]));

      ss.str("");
      ss << jet_name << "E";
      m_eventTree->Branch(ss.str().c_str(),&(m_jet_E[iColl]));

      if(jet_name.find("Truth")!=std::string::npos)
      {
	ss.str("");
	ss << jet_name << "Flavor";
	m_eventTree->Branch(ss.str().c_str(),&(m_jet_flavor[iColl]));
      }
    }
  }
  if(m_doClusters)
  {
    // Clusters 
    m_eventTree->Branch("nCluster",               &m_nCluster, "nCluster/I");
    m_eventTree->Branch("nClusterVec",            &m_nClusterVec);
    m_eventTree->Branch("nClusterFound",           &m_nClusterFound, "nClusterFound/I");
    m_eventTree->Branch("nClusterReal",            &m_nClusterReal, "nClusterReal/I");
    m_eventTree->Branch("cluster_E",               &m_cluster_E);
    m_eventTree->Branch("cluster_E_LCCalib",       &m_cluster_E_LCCalib);
    m_eventTree->Branch("cluster_Pt",              &m_cluster_Pt);
    m_eventTree->Branch("cluster_Eta",             &m_cluster_Eta);
    m_eventTree->Branch("cluster_Phi",             &m_cluster_Phi);
    m_eventTree->Branch("cluster_nCells",         &m_cluster_nCells);
    m_eventTree->Branch("cluster_TauAssoc",         &m_cluster_TauAssoc);
    if(m_doClusterMoments)
    {
      m_eventTree->Branch("cluster_ENG_CALIB_TOT",     &m_cluster_ENG_CALIB_TOT);
      m_eventTree->Branch("cluster_ENG_CALIB_OUT_T",   &m_cluster_ENG_CALIB_OUT_T);
      m_eventTree->Branch("cluster_ENG_CALIB_DEAD_TOT",&m_cluster_ENG_CALIB_DEAD_TOT);
      m_eventTree->Branch("cluster_EM_PROBABILITY",   &m_cluster_EM_PROBABILITY);
      m_eventTree->Branch("cluster_HAD_WEIGHT", &m_cluster_HAD_WEIGHT);
      m_eventTree->Branch("cluster_OOC_WEIGHT", &m_cluster_OOC_WEIGHT);
      m_eventTree->Branch("cluster_DM_WEIGHT", &m_cluster_DM_WEIGHT);
      m_eventTree->Branch("cluster_CENTER_MAG", &m_cluster_CENTER_MAG);
      m_eventTree->Branch("cluster_FIRST_ENG_DENS", &m_cluster_FIRST_ENG_DENS);
      m_eventTree->Branch("cluster_CENTER_LAMBDA", &m_cluster_CENTER_LAMBDA);
      m_eventTree->Branch("cluster_ISOLATION", &m_cluster_ISOLATION);
      m_eventTree->Branch("cluster_ENERGY_DigiHSTruth", &m_cluster_ENERGY_DigiHSTruth);
    }

    if(m_doClusterCells)
    {
      m_eventTree->Branch("cluster_cell_ID",&m_cluster_cell_ID);
      m_eventTree->Branch("cluster_cell_E",&m_cluster_cell_E);
      m_eventTree->Branch("cluster_cell_Eta",&m_cluster_cell_Eta);
      m_eventTree->Branch("cluster_cell_Phi",&m_cluster_cell_Phi);
      m_eventTree->Branch("cluster_cell_Sampling",&m_cluster_cell_Sampling);

      if(m_doCalibHits)
      {
	if(m_doCalibHitsPerCell)
	{
	  m_eventTree->Branch("cluster_cell_hitsE_EM",&m_cluster_cell_hitsE_EM);	  
	  m_eventTree->Branch("cluster_cell_hitsE_nonEM",&m_cluster_cell_hitsE_nonEM);	  
	  m_eventTree->Branch("cluster_cell_hitsE_Invisible",&m_cluster_cell_hitsE_Invisible);
	  m_eventTree->Branch("cluster_cell_hitsE_Escaped", &m_cluster_cell_hitsE_Escaped);  
	}
	if(m_doTruthParticles) 
	{
	  m_eventTree->Branch("cluster_hitsTruthIndex", &m_cluster_hitsTruthIndex);  
	  m_eventTree->Branch("cluster_hitsTruthE", &m_cluster_hitsTruthE);  
	}
  if (m_doTaus) 
  {
    m_eventTree->Branch("truthTauPhi",    &m_truthTauPhi);

    m_eventTree->Branch("truthTauPtVis",   &m_truthTauPtVis);
    m_eventTree->Branch("truthTauEtaVis",   &m_truthTauEtaVis);
    m_eventTree->Branch("truthTauPhiVis",   &m_truthTauPhiVis);
    m_eventTree->Branch("truthTauMVis",    &m_truthTauMVis);

    m_eventTree->Branch("truthTauPtVisNeutral",   &m_truthTauPtVisNeutral);
    m_eventTree->Branch("truthTauEtaVisNeutral",   &m_truthTauEtaVisNeutral);
    m_eventTree->Branch("truthTauPhiVisNeutral",   &m_truthTauPhiVisNeutral);
    m_eventTree->Branch("truthTauMVisNeutral",    &m_truthTauMVisNeutral);   

    m_eventTree->Branch("truthTauPtVisCharged",   &m_truthTauPtVisCharged);
    m_eventTree->Branch("truthTauEtaVisCharged",   &m_truthTauEtaVisCharged);
    m_eventTree->Branch("truthTauPhiVisCharged",   &m_truthTauPhiVisCharged);
    m_eventTree->Branch("truthTauMVisCharged",    &m_truthTauMVisCharged);
    
    m_eventTree->Branch("truthTauDecayMode", &m_truthTauDecayMode); 
    m_eventTree->Branch("tauDecayMode", &m_tauDecayMode);
    m_eventTree->Branch("tauDecayModeNN", &m_tauDecayModeNN);

    m_eventTree->Branch("nTruthTaus", &m_nTruthTaus); 
    m_eventTree->Branch("nTaus", &m_nTaus); 
    m_eventTree->Branch("nMatchedTaus", &m_nMatchedTaus); 


    m_eventTree->Branch("tauPt", &m_tauPt);
    m_eventTree->Branch("tauEta", &m_tauEta);
    m_eventTree->Branch("tauPhi", &m_tauPhi);
    m_eventTree->Branch("tauE",  &m_tauE);

    m_eventTree->Branch("tauCaloOnlyPt", &m_tauCaloOnlyPt);
    m_eventTree->Branch("tauCaloOnlyEta", &m_tauCaloOnlyEta);
    m_eventTree->Branch("tauCaloOnlyPhi", &m_tauCaloOnlyPhi);
    m_eventTree->Branch("tauCaloOnlyE",  &m_tauCaloOnlyE);

    m_eventTree->Branch("tauPtJetSeed", &m_tauPtJetSeed);
    m_eventTree->Branch("tauPtDetectorAxis", &m_tauPtDetectorAxis);
    m_eventTree->Branch("tauPtIntermediateAxis", &m_tauPtIntermediateAxis);
    m_eventTree->Branch("tauPtTauEnergyScale", &m_tauPtTauEnergyScale);
    m_eventTree->Branch("tauPtTauEtaCalib",  &m_tauPtTauEtaCalib);
    m_eventTree->Branch("tauPtPanTauCellBasedProto", &m_tauPtPanTauCellBasedProto);
    m_eventTree->Branch("tauPtPanTauCellBased", &m_tauPtPanTauCellBased);
    m_eventTree->Branch("tauPtTrigCaloOnly", &m_tauPtTrigCaloOnly);
    m_eventTree->Branch("tauPtFinalCalib", &m_tauPtFinalCalib);

    m_eventTree->Branch("tauTruthPtVis",   &m_tauTruthPtVis);
    m_eventTree->Branch("tauTruthPhiVis",   &m_tauTruthPhiVis);
    m_eventTree->Branch("tauTruthEtaVis",   &m_tauTruthEtaVis);
    m_eventTree->Branch("tauTruthMVis",   &m_tauTruthMVis);
  
    m_eventTree->Branch("tauTruthCharge", &m_tauTruthCharge);
    m_eventTree->Branch("tauTruthProng", &m_tauTruthProng);
    m_eventTree->Branch("tauTruthDecayMode", &m_tauTruthDecayMode);

    m_eventTree->Branch("tauTruthMVisNeutral", &m_tauTruthMVisNeutral);
    m_eventTree->Branch("tauTruthEtaVisNeutral", &m_tauTruthEtaVisNeutral);
    m_eventTree->Branch("tauTruthPtVisNeutral", &m_tauTruthPtVisNeutral);
    m_eventTree->Branch("tauTruthPhiVisNeutral", &m_tauTruthPhiVisNeutral);
  
    m_eventTree->Branch("tauTruthMVisCharged", &m_tauTruthMVisCharged);
    m_eventTree->Branch("tauTruthEtaVisCharged", &m_tauTruthEtaVisCharged);
    m_eventTree->Branch("tauTruthPtVisCharged", &m_tauTruthPtVisCharged);
    m_eventTree->Branch("tauTruthPhiVisCharged", &m_tauTruthPhiVisCharged);

    m_eventTree->Branch("tauTrackAssocPt", &m_tauTrackAssocPt);

    if (m_doClusters) m_eventTree->Branch("tauClusterDeltaR", &m_cluster_tau_dR);
  }
      }
    }
  }
  


  return StatusCode::SUCCESS;
}

StatusCode MLTreeMaker::execute() {  


  // Clear all variables from previous event
  m_runNumber = m_eventNumber = m_mcEventNumber = m_mcChannelNumber = m_bcid = m_lumiBlock = 0;
  m_coreFlags = 0;
  // event cleaning
  m_LArError = false;
  m_TileError = false;
  m_SCTError = false;
  m_LArFlags = 0;
  m_TileFlags = 0;
  m_SCTFlags = 0;
  m_mcEventWeight = 1.;
  m_prescale_DataWeight = 1.;
  m_weight_pileup = 1.;
  m_timeStamp = -999;
  m_timeStampNSOffset = -999;
  // pileup
  m_npv = -999;
  m_actualMu = m_averageMu = -999;
  // shapeEM
  m_rhoEM = -999;
  // shapeLC
  m_rhoLC = -999;
  // truth
  m_pdgId1 = m_pdgId2 = m_pdfId1 = m_pdfId2 = -999;
  m_x1 = m_x2 = -999;
  m_xf1 = m_xf2 = -999;
  //m_scale = m_q = m_pdf1 = m_pdf2 = -999;

  m_truthPartPdgId.clear();
  m_truthPartStatus.clear();
  m_truthPartBarcode.clear();
  m_truthPartPt.clear();
  m_truthPartE.clear();
  m_truthPartMass.clear();
  m_truthPartEta.clear();
  m_truthPartPhi.clear();

  m_trackPt.clear();
  m_trackP.clear();
  m_trackMass.clear();
  m_trackEta.clear();
  m_trackPhi.clear();

  m_trackNumberOfPixelHits.clear();
  m_trackNumberOfSCTHits.clear();
  m_trackNumberOfPixelDeadSensors.clear();
  m_trackNumberOfSCTDeadSensors.clear();
  m_trackNumberOfPixelSharedHits.clear();
  m_trackNumberOfSCTSharedHits.clear();
  m_trackNumberOfPixelHoles.clear();
  m_trackNumberOfSCTHoles.clear();
  m_trackNumberOfInnermostPixelLayerHits.clear();
  m_trackNumberOfNextToInnermostPixelLayerHits.clear();
  m_trackExpectInnermostPixelLayerHit.clear();
  m_trackExpectNextToInnermostPixelLayerHit.clear();
  m_trackNumberOfTRTHits.clear();
  m_trackNumberOfTRTOutliers.clear();
  m_trackChiSquared.clear();
  m_trackNumberDOF.clear();
  m_trackD0.clear();
  m_trackZ0.clear();

  m_trackEta_PreSamplerB.clear();
  m_trackPhi_PreSamplerB.clear();
  m_trackEta_PreSamplerE.clear();
  m_trackPhi_PreSamplerE.clear();

  m_trackEta_EMB1.clear(); 
  m_trackPhi_EMB1.clear(); 
  m_trackEta_EMB2.clear(); 
  m_trackPhi_EMB2.clear(); 
  m_trackEta_EMB3.clear(); 
  m_trackPhi_EMB3.clear(); 

  m_trackEta_EME1.clear(); 
  m_trackPhi_EME1.clear(); 
  m_trackEta_EME2.clear(); 
  m_trackPhi_EME2.clear(); 
  m_trackEta_EME3.clear(); 
  m_trackPhi_EME3.clear(); 

  m_trackEta_HEC0.clear(); 
  m_trackPhi_HEC0.clear(); 
  m_trackEta_HEC1.clear(); 
  m_trackPhi_HEC1.clear(); 
  m_trackEta_HEC2.clear(); 
  m_trackPhi_HEC2.clear(); 
  m_trackEta_HEC3.clear(); 
  m_trackPhi_HEC3.clear(); 

  m_trackEta_TileBar0.clear(); 
  m_trackPhi_TileBar0.clear(); 
  m_trackEta_TileBar1.clear(); 
  m_trackPhi_TileBar1.clear(); 
  m_trackEta_TileBar2.clear(); 
  m_trackPhi_TileBar2.clear(); 

  m_trackEta_TileGap1.clear(); 
  m_trackPhi_TileGap1.clear(); 
  m_trackEta_TileGap2.clear(); 
  m_trackPhi_TileGap2.clear(); 
  m_trackEta_TileGap3.clear(); 
  m_trackPhi_TileGap3.clear(); 

  m_trackEta_TileExt0.clear();
  m_trackPhi_TileExt0.clear();
  m_trackEta_TileExt1.clear();
  m_trackPhi_TileExt1.clear();
  m_trackEta_TileExt2.clear();
  m_trackPhi_TileExt2.clear();


  // General event information
  const xAOD::EventInfo* eventInfo(nullptr);
  CHECK( evtStore()->retrieve(eventInfo, m_eventInfoContainerName) );

  m_runNumber         = eventInfo->runNumber();
  m_eventNumber       = eventInfo->eventNumber();
  m_lumiBlock         = eventInfo->lumiBlock();
  m_coreFlags         = eventInfo->eventFlags(xAOD::EventInfo::Core);
  // if (m_isMC ) {
  m_mcEventNumber   = eventInfo->mcEventNumber();
  m_mcChannelNumber = eventInfo->mcChannelNumber();
  m_mcEventWeight   = eventInfo->mcEventWeight();
  // } else {
  //   m_bcid            = eventInfo->bcid();
  // }

  if (m_doEventCleaning ) 
  {

    if (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) m_LArError = true;
    else m_LArError = false;
    m_LArFlags = eventInfo->eventFlags(xAOD::EventInfo::LAr);

    if (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) m_TileError = true;
    else m_TileError = false;
    m_TileFlags = eventInfo->eventFlags(xAOD::EventInfo::Tile);

    if (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) m_SCTError = true;
    else m_SCTError = false;
    m_SCTFlags = eventInfo->eventFlags(xAOD::EventInfo::SCT);

    m_timeStamp = eventInfo->timeStamp();
    m_timeStampNSOffset = eventInfo->timeStampNSOffset();
  }

  if (m_doPileup ) 
  {

    const xAOD::VertexContainer* vertices = 0;
    CHECK( evtStore()->retrieve( vertices, m_vxContainerName));
    m_npv = 0;
    unsigned int Ntracks = 2;
    for (const auto& vertex : *vertices)
    {
      if (vertex->vertexType() == xAOD::VxType::NoVtx) continue;
      if (vertex->nTrackParticles() < Ntracks ) continue;
      m_npv++;
    }
    m_actualMu  = eventInfo->actualInteractionsPerCrossing();
    m_averageMu = eventInfo->averageInteractionsPerCrossing();

    // if (m_isMC ) {
    static SG::AuxElement::ConstAccessor< float > weight_pileup ("PileupWeight");
    static SG::AuxElement::ConstAccessor< float >  correct_mu("corrected_averageInteractionsPerCrossing");
    static SG::AuxElement::ConstAccessor< unsigned int > rand_run_nr("RandomRunNumber");
    static SG::AuxElement::ConstAccessor< unsigned int > rand_lumiblock_nr("RandomLumiBlockNumber");

    if ( weight_pileup.isAvailable( *eventInfo ) )	 { m_weight_pileup = weight_pileup( *eventInfo ); }	    else { m_weight_pileup = 1.0; }
    if ( correct_mu.isAvailable( *eventInfo ) )	 { m_correct_mu = correct_mu( *eventInfo ); }		    else { m_correct_mu = -1.0; }
    if ( rand_run_nr.isAvailable( *eventInfo ) )	 { m_rand_run_nr = rand_run_nr( *eventInfo ); } 	    else { m_rand_run_nr = 900000; }
    if ( rand_lumiblock_nr.isAvailable( *eventInfo ) ) { m_rand_lumiblock_nr = rand_lumiblock_nr( *eventInfo ); } else { m_rand_lumiblock_nr = 0; }

    // } else {
    //   static SG::AuxElement::ConstAccessor< float > prsc_DataWeight ("prescale_DataWeight");
    //
    //   if (prsc_DataWeight.isAvailable( *eventInfo ) )	 {
    //     m_prescale_DataWeight = prsc_DataWeight( *eventInfo ); 
    //   }
    //   else { 
    //     m_prescale_DataWeight = 1.0; 
    //   }
    // }
  }

  if (m_doShapeLC )
  {
    const xAOD::EventShape* evtShape(nullptr);
    CHECK(evtStore()->retrieve( evtShape, "Kt4LCTopoEventShape"));
    if ( !evtShape->getDensity( xAOD::EventShape::Density, m_rhoLC ) ) {
      Info("FillEvent()","Could not retrieve xAOD::EventShape::Density from xAOD::EventShape");
      m_rhoLC = -999;
    }
  }
  
  if (m_doShapeEM ) 
  {
    const xAOD::EventShape* evtShape(nullptr);
    CHECK(evtStore()->retrieve( evtShape, "Kt4EMTopoEventShape"));
    if ( !evtShape->getDensity( xAOD::EventShape::Density, m_rhoEM ) ) {
      Info("FillEvent()","Could not retrieve xAOD::EventShape::Density from xAOD::EventShape");
      m_rhoEM = -999;
    }
  }
  if (m_doEventTruth /*&& m_isMC*/ ) 
  {

    const xAOD::TruthEventContainer* truthEventContainer = 0;
    CHECK(evtStore()->retrieve( truthEventContainer, "TruthEvents"));
    if (truthEventContainer ) 
    {
      const xAOD::TruthEvent* truthEventContainervent = truthEventContainer->at(0);

      truthEventContainervent->pdfInfoParameter(m_pdgId1,   xAOD::TruthEvent::PDGID1);
      truthEventContainervent->pdfInfoParameter(m_pdgId2,   xAOD::TruthEvent::PDGID2);
      truthEventContainervent->pdfInfoParameter(m_pdfId1,   xAOD::TruthEvent::PDFID1);
      truthEventContainervent->pdfInfoParameter(m_pdfId2,   xAOD::TruthEvent::PDFID2);
      truthEventContainervent->pdfInfoParameter(m_x1,       xAOD::TruthEvent::X1);
      truthEventContainervent->pdfInfoParameter(m_x2,       xAOD::TruthEvent::X2);
      // truthEventContainervent->pdfInfoParameter(m_scale,    xAOD::TruthEvent::SCALE);
      // truthEventContainervent->pdfInfoParameter(m_q,        xAOD::TruthEvent::Q);
      // truthEventContainervent->pdfInfoParameter(m_pdf1,     xAOD::TruthEvent::PDF1);
      // truthEventContainervent->pdfInfoParameter(m_pdf2,     xAOD::TruthEvent::PDF2);
      truthEventContainervent->pdfInfoParameter(m_xf1,      xAOD::TruthEvent::XF1);
      truthEventContainervent->pdfInfoParameter(m_xf2,      xAOD::TruthEvent::XF2);

      // // crashes because of q?`
      //   const xAOD::TruthEvent::PdfInfo info = truthEventContainervent->pdfInfo();
      //   if (info.valid() ) {
      //     m_pdgId1 = info.pdgId1;
      //     m_pdgId2 = info.pdgId2;
      //     m_pdfId1 = info.pdfId1;
      //     m_pdfId2 = info.pdfId2;
      //     m_x1     = info.x1;
      //     m_x2     = info.x2;
      //     //m_q      = info.Q;
      //     m_xf1    = info.xf1;
      //     m_xf2    = info.xf2;
      //   }
    }
  }
  std::vector<const CaloCalibrationHitContainer *> v_calibHitContainer;
  if(m_doClusterCells && m_doCalibHits)
  {
    const DataHandle<CaloCalibrationHitContainer> calibHitContainer;
    for(auto cname : m_CalibrationHitContainerKeys)
    {
      CHECK(evtStore()->retrieve(calibHitContainer,cname));
      v_calibHitContainer.push_back(calibHitContainer);
    }
  }
  // Truth particles

  //calibration hits are associated with truth particles via barcode
  //construct mapping between barcode and index in output particle list for lookup later
  //note this index is not necessarily the same as the index in the truthContainer
  //e.g. you filter some of the particles
  std::map<int,unsigned int> truthBarcodeMap;
  if(m_doTruthParticles)
  {
    const xAOD::TruthParticleContainer* truthContainer = 0;
    CHECK(evtStore()->retrieve(truthContainer, m_truthContainerName));
    
    m_nTruthPart = 0;
    m_G4PreCalo_n_EM=0;
    m_G4PreCalo_E_EM=0;
    m_G4PreCalo_n_Had=0;
    m_G4PreCalo_E_Had=0;
    m_truthVertexX=0;
    m_truthVertexY=0;
    m_truthVertexZ=0;
    //
    for(unsigned int iTruth=0; iTruth<truthContainer->size(); iTruth++)
    {
      const auto& truth=truthContainer->at(iTruth);
      if(truth->hasProdVtx())
      {
	auto prodVtx=truth->prodVtx();
	if(prodVtx->barcode()==-1)
	{
	  m_truthVertexX=prodVtx->x();
	  m_truthVertexY=prodVtx->y();
	  m_truthVertexZ=prodVtx->z();
	}
      }
      if(truth->status()!=1 && m_keepOnlyStableTruthParticles) continue;
      if(truth->barcode() > m_G4BarcodeOffset)
      {
	//compute event-level observable for early showers
	//assume G4 particles in list are produced before calorimeter
	//thus particles w/o decay vertex hit the calorimeter
	//skip neutrinos, also skip nuclear fragments produced in hadronic showers
	//these are typically low E and should produce large ionization ~immediately
	if(!truth->hasDecayVtx() ) 
	{
	  //since many of these particles will be low energy hadrons
	  //use kinetic energy = E-M, since mass energy unlikely to be recovered
	  //also, w/o this modification, the "energy" can be greater than incident particle E
	  float truthE=(truth->e()-truth->m())*1e-3;
	  if( (truth->isHadron()) ) 
	  {
	    m_G4PreCalo_E_Had+=truthE;
	    m_G4PreCalo_n_Had++;
	  }
	  else if(!truth->isNeutrino()) 
	  {
	    m_G4PreCalo_E_EM+=truthE;
	    m_G4PreCalo_n_EM++;
	  }
	}
	if(!m_keepG4TruthParticles) continue;
      }
      m_truthPartPdgId.push_back(truth->pdgId());
      m_truthPartStatus.push_back(truth->status());
      m_truthPartBarcode.push_back(truth->barcode());
      m_truthPartPt.push_back(truth->pt()*1e-3);
      m_truthPartE.push_back(truth->e()*1e-3);
      m_truthPartMass.push_back(truth->m()*1e-3);
      m_truthPartEta.push_back(truth->eta());
      m_truthPartPhi.push_back(truth->phi());
      if(m_doCalibHits) truthBarcodeMap.emplace_hint(truthBarcodeMap.end(),truth->barcode(),m_nTruthPart);
      m_nTruthPart++;

    }
  }
  if(m_doTaus) 
  {

   // Clear variables
   m_truthTauEta.clear();
   m_truthTauPhi.clear();
   m_truthTauPt.clear();
   m_truthTauE.clear();  

   m_truthTauPdgId.clear();
 
   m_truthTauPtVis.clear();
   m_truthTauEtaVis.clear();
   m_truthTauPhiVis.clear();
   m_truthTauMVis.clear();

   m_truthTauPtVisNeutral.clear();
   m_truthTauEtaVisNeutral.clear();
   m_truthTauPhiVisNeutral.clear();
   m_truthTauMVisNeutral.clear();

   m_truthTauPtVisCharged.clear();
   m_truthTauEtaVisCharged.clear();
   m_truthTauPhiVisCharged.clear();
   m_truthTauMVisCharged.clear();
   
   m_truthTauDecayMode.clear();
   // Get TruthTaus from the TruthParticleContainer using the tauTruthMatchingTool
   //xAOD::TruthParticleContainer* xTruthTauContainer = m_tauTruthMatchingTool->getTruthTauContainer();
   //xAOD::TruthParticleAuxContainer* xTruthTauAuxContainer = m_tauTruthMatchingTool->getTruthTauAuxContainer();
  
   //const xAOD::TruthParticleContainer* xTruthTauContainer = nullptr;
   //const xAOD::TruthParticleAuxContainer* xTruthTauAuxContainer = nullptr;
   //CHECK(evtStore()->retrieve(xTruthTauContainer, "TruthTaus"));
   //CHECK(evtStore()->retrieve(xTruthTauAuxContainer, "TruthTausAux"));
 
   //ATH_MSG_INFO("About to print nTruthTaus...");
   //ATH_MSG_INFO(xTruthTauContainer->size());
   // Loop over truth taus and fill variables
   //for (const xAOD::TruthParticle* truthTau: *xTruthTauContainer){

  // 	ATH_MSG_INFO("In loop, if you don't see this this is the problem!");

    //    m_truthTauPdgId.push_back(truthTau->pdgId());
      //  m_nTruthTaus = xTruthTauContainer->size();
     
       // m_truthTauDecayMode.push_back(m_tauTruthMatchingTool->getDecayMode(*truthTau));  

     //   m_truthTauEta.push_back(truthTau->eta());
     //   m_truthTauPhi.push_back(truthTau->phi());
     //   m_truthTauE.push_back(truthTau->e()*1e-3);
     //   m_truthTauPt.push_back(truthTau->pt()*1e-3);

     //   m_truthTauPtVis.push_back(truthTau->auxdata<double>("pt_vis")/1e3);
     //   m_truthTauEtaVis.push_back(truthTau->auxdata<double>("eta_vis"));
     //   m_truthTauPhiVis.push_back(truthTau->auxdata<double>("phi_vis"));
     //   m_truthTauMVis.push_back(truthTau->auxdata<double>("m_vis")/1e3);

      //  m_truthTauPtVisNeutral.push_back(truthTau->auxdata<double>("pt_vis_neutral")/1e3);
     //   m_truthTauEtaVisNeutral.push_back(truthTau->auxdata<double>("eta_vis_neutral"));
     //   m_truthTauPhiVisNeutral.push_back(truthTau->auxdata<double>("phi_vis_neutral"));
     //   m_truthTauMVisNeutral.push_back(truthTau->auxdata<double>("m_vis_neutral")/1e3);

    //    m_truthTauPtVisCharged.push_back(truthTau->auxdata<double>("pt_vis_charged")/1e3);
    //    m_truthTauEtaVisCharged.push_back(truthTau->auxdata<double>("eta_vis_charged"));
    //    m_truthTauPhiVisCharged.push_back(truthTau->auxdata<double>("phi_vis_charged"));
   //     m_truthTauMVisCharged.push_back(truthTau->auxdata<double>("m_vis_charged")/1e3);
   //}


   // Clear reco-level variables 
   m_tauDecayMode.clear();
   m_tauDecayModeNN.clear();
   m_tauPt.clear();
   m_tauEta.clear();
   m_tauPhi.clear();
   m_tauE.clear();

   m_tauCaloOnlyPt.clear();
   m_tauCaloOnlyEta.clear();
   m_tauCaloOnlyPhi.clear();
   m_tauCaloOnlyE.clear();

   m_tauPtJetSeed.clear();
   m_tauPtDetectorAxis.clear();
   m_tauPtIntermediateAxis.clear();
   m_tauPtTauEnergyScale.clear();
   m_tauPtTauEtaCalib.clear();
   m_tauPtPanTauCellBasedProto.clear();
   m_tauPtPanTauCellBased.clear();
   m_tauPtTrigCaloOnly.clear();
   m_tauPtFinalCalib.clear();



   m_tauTruthPtVis.clear();
   m_tauTruthEtaVis.clear();
   m_tauTruthPhiVis.clear();
   m_tauTruthMVis.clear();   

   m_tauTruthCharge.clear();
   m_tauTruthProng.clear();
   m_tauTruthDecayMode.clear();

   m_tauTruthPtVisNeutral.clear();
   m_tauTruthPhiVisNeutral.clear();
   m_tauTruthEtaVisNeutral.clear();
   m_tauTruthMVisNeutral.clear();

   m_tauTruthPtVisCharged.clear();
   m_tauTruthPhiVisCharged.clear();
   m_tauTruthEtaVisCharged.clear();
   m_tauTruthMVisCharged.clear();
   
   const xAOD::TauJetContainer* tau_cont = nullptr;
   CHECK(evtStore()->retrieve(tau_cont, "TauJets"));
   m_nTaus = tau_cont->size(); 
   m_nMatchedTaus = 0;
   for(const xAOD::TauJet* recoTau: *tau_cont)
   { 

    //std::vector<const xAOD::IParticle*> particleList = recoTau->clusters();  
    //for (const xAOD::IParticle* particle : particleList) {
    //  const xAOD::CaloCluster* cluster = static_cast<const xAOD::CaloCluster*>(particle);
    //  ATH_MSG_INFO(cluster->e());
    //}
 
    bool pass_selection = false;
    const auto truthTau = m_tauTruthMatchingTool->applyTruthMatch(*recoTau);
    
    if(recoTau->auxdata<char>("IsTruthMatched")&& truthTau)
    {
    bool isHadronic = truthTau->auxdata<char>("IsHadronicTau");
    if (isHadronic) { 
      pass_selection = true;
      m_nMatchedTaus = m_nMatchedTaus + 1;
      }
    }
    
    if(pass_selection){ 
  
      ATH_MSG_INFO("Tau passes selection");

    int decayMode = xAOD::TauJetParameters::DecayMode::Mode_Error;
    recoTau->panTauDetail(xAOD::TauJetParameters::PanTauDetails::PanTau_DecayMode, decayMode);    
    m_tauDecayMode.push_back(recoTau->auxdata<int>("PanTau_DecayMode")); 
    m_tauDecayModeNN.push_back(recoTau->auxdata<int>("NNDecayMode")); 
   
    m_tauPt.push_back(recoTau->pt()/1e3);
    m_tauEta.push_back(recoTau->eta());
    m_tauPhi.push_back(recoTau->phi());
    m_tauE.push_back(recoTau->e()/1e3); 

    auto mTauVec = recoTau->p4(xAOD::TauJetParameters::TauEnergyScale);

    m_tauCaloOnlyPt.push_back(mTauVec.Pt()/1e3);
    m_tauCaloOnlyEta.push_back(mTauVec.Eta());
    m_tauCaloOnlyPhi.push_back(mTauVec.Phi());
    m_tauCaloOnlyE.push_back(mTauVec.E()/1e3); 

    // pt of tau at different calibration stages
    m_tauPtJetSeed.push_back(recoTau->ptJetSeed()/1e3);
    m_tauPtDetectorAxis.push_back(recoTau->ptDetectorAxis()/1e3);
    m_tauPtIntermediateAxis.push_back(recoTau->ptIntermediateAxis()/1e3);
    m_tauPtTauEnergyScale.push_back(recoTau->ptTauEnergyScale()/1e3);
    m_tauPtTauEtaCalib.push_back(recoTau->ptTauEtaCalib()/1e3);
    m_tauPtPanTauCellBasedProto.push_back(recoTau->ptPanTauCellBasedProto()/1e3);
    m_tauPtPanTauCellBased.push_back(recoTau->ptPanTauCellBased()/1e3);
    m_tauPtTrigCaloOnly.push_back(recoTau->ptTrigCaloOnly()/1e3);
    m_tauPtFinalCalib.push_back(recoTau->ptFinalCalib()/1e3);


    // truth information for reco taus

   recoTau->auxdecor<double>("truthPtVis")=truthTau->auxdata<double>("pt_vis"); 
   m_tauTruthPtVis.push_back(recoTau->auxdata<double>("truthPtVis")/1e3);

   recoTau->auxdecor<double>("truthEtaVis")=truthTau->auxdata<double>("eta_vis");
   m_tauTruthEtaVis.push_back(recoTau->auxdata<double>("truthEtaVis"));

   recoTau->auxdecor<double>("truthPhiVis")=truthTau->auxdata<double>("phi_vis");
   m_tauTruthPhiVis.push_back(recoTau->auxdata<double>("truthPhiVis"));

   recoTau->auxdecor<double>("truthMVis")=truthTau->auxdata<double>("m_vis");
   m_tauTruthMVis.push_back(recoTau->auxdata<double>("truthMVis")/1e3);

   recoTau->auxdecor<int>("truthCharge")= truthTau->charge();
   m_tauTruthCharge.push_back(recoTau->auxdecor<int>("truthCharge"));

   recoTau->auxdecor<size_t>("truthProng") = truthTau->auxdata<size_t>("numCharged");
   m_tauTruthProng.push_back(recoTau->auxdecor<size_t>("truthProng"));

   recoTau->auxdecor<size_t>("truthDecayMode")=m_tauTruthMatchingTool->getDecayMode(*truthTau);
   m_tauTruthDecayMode.push_back(recoTau->auxdecor<size_t>("truthDecayMode"));
   

   recoTau->auxdecor<double>("truthPtVisNeutral") = truthTau->auxdata<double>("pt_vis_neutral");
   m_tauTruthPtVisNeutral.push_back(recoTau->auxdecor<double>("truthPtVisNeutral")/1e3);

   recoTau->auxdecor<double>("truthEtaVisNeutral") = truthTau->auxdata<double>("eta_vis_neutral");
   m_tauTruthEtaVisNeutral.push_back(recoTau->auxdecor<double>("truthEtaVisNeutral"));

   recoTau->auxdecor<double>("truthPhiVisNeutral") = truthTau->auxdata<double>("phi_vis_neutral");
   m_tauTruthPhiVisNeutral.push_back(recoTau->auxdecor<double>("truthPhiVisNeutral"));

   recoTau->auxdecor<double>("truthMVisNeutral") = truthTau->auxdata<double>("m_vis_neutral");
   m_tauTruthMVisNeutral.push_back(recoTau->auxdecor<double>("truthMVisNeutral")/1e3);

   recoTau->auxdecor<double>("truthPtVisCharged") = truthTau->auxdata<double>("pt_vis_charged");
   m_tauTruthPtVisCharged.push_back(recoTau->auxdecor<double>("truthPtVisCharged")/1e3);

   recoTau->auxdecor<double>("truthEtaVisCharged") = truthTau->auxdata<double>("eta_vis_charged");
   m_tauTruthEtaVisCharged.push_back(recoTau->auxdecor<double>("truthEtaVisCharged"));

   recoTau->auxdecor<double>("truthPhiVisCharged") = truthTau->auxdata<double>("phi_vis_charged");
   m_tauTruthPhiVisCharged.push_back(recoTau->auxdecor<double>("truthPhiVisCharged"));

   recoTau->auxdecor<double>("truthMVisCharged") = truthTau->auxdata<double>("m_vis_charged");
   m_tauTruthMVisCharged.push_back(recoTau->auxdecor<double>("truthMVisCharged")/1e3);

    } // end if pass selection
   } // end for loop 
  } // end if(m_doTaus)
  if (m_doTracking) 
  {
      // Tracks
    const xAOD::TrackParticleContainer* trackContainer = 0;
    CHECK(evtStore()->retrieve(trackContainer, m_trackContainerName));
    m_nTrack = 0;
    for (const auto& track : *trackContainer) 
    {
      
      if ( !m_trkSelectionTool->accept(track) ) continue;

      m_trackPt.push_back(track->pt()*1e-3);
      m_trackP.push_back(TMath::Abs(1./track->qOverP())*1e-3);
      m_trackMass.push_back(track->m()*1e-3);
      m_trackEta.push_back(track->eta());
      m_trackPhi.push_back(track->phi());

      // Load track quality variables
      track->summaryValue(m_numberOfPixelHits, xAOD::numberOfPixelHits);
      track->summaryValue(m_numberOfSCTHits, xAOD::numberOfSCTHits);
      track->summaryValue(m_numberOfPixelDeadSensors, xAOD::numberOfPixelDeadSensors);
      track->summaryValue(m_numberOfSCTDeadSensors, xAOD::numberOfSCTDeadSensors);
      track->summaryValue(m_numberOfPixelDeadSensors, xAOD::numberOfPixelDeadSensors);
      track->summaryValue(m_numberOfSCTDeadSensors, xAOD::numberOfSCTDeadSensors);
      track->summaryValue(m_numberOfPixelHoles, xAOD::numberOfPixelHoles);
      track->summaryValue(m_numberOfSCTHoles, xAOD::numberOfSCTHoles);
      track->summaryValue(m_numberOfInnermostPixelLayerHits, xAOD::numberOfInnermostPixelLayerHits);
      track->summaryValue(m_numberOfNextToInnermostPixelLayerHits, xAOD::numberOfNextToInnermostPixelLayerHits);
      track->summaryValue(m_expectInnermostPixelLayerHit, xAOD::expectInnermostPixelLayerHit);
      track->summaryValue(m_expectNextToInnermostPixelLayerHit, xAOD::expectNextToInnermostPixelLayerHit);
      track->summaryValue(m_numberOfTRTHits, xAOD::numberOfTRTHits);
      track->summaryValue(m_numberOfTRTOutliers, xAOD::numberOfTRTOutliers);

      m_trackNumberOfPixelHits.push_back(m_numberOfPixelHits);
      m_trackNumberOfSCTHits.push_back(m_numberOfSCTHits);
      m_trackNumberOfPixelDeadSensors.push_back(m_numberOfPixelDeadSensors);
      m_trackNumberOfSCTDeadSensors.push_back(m_numberOfSCTDeadSensors);
      m_trackNumberOfPixelSharedHits.push_back(m_numberOfPixelSharedHits);
      m_trackNumberOfSCTSharedHits.push_back(m_numberOfSCTSharedHits);
      m_trackNumberOfPixelHoles.push_back(m_numberOfPixelHoles);
      m_trackNumberOfSCTHoles.push_back(m_numberOfSCTHoles);
      m_trackNumberOfInnermostPixelLayerHits.push_back(m_numberOfInnermostPixelLayerHits);
      m_trackNumberOfNextToInnermostPixelLayerHits.push_back(m_numberOfNextToInnermostPixelLayerHits);
      m_trackExpectInnermostPixelLayerHit.push_back(m_expectInnermostPixelLayerHit);
      m_trackExpectNextToInnermostPixelLayerHit.push_back(m_expectNextToInnermostPixelLayerHit);
      m_trackNumberOfTRTHits.push_back(m_numberOfTRTHits);
      m_trackNumberOfTRTOutliers.push_back(m_numberOfTRTOutliers);
      m_trackChiSquared.push_back(track->chiSquared());
      m_trackNumberDOF.push_back(track->numberDoF());
      m_trackD0.push_back(track->definingParameters()[0]);
      m_trackZ0.push_back(track->definingParameters()[1]);

      // A map to store the track parameters (eta,phi) associated with the different layers of the calorimeter system      
      std::map<CaloCell_ID::CaloSample,std::pair<float,float> > parametersMap;      
      
      std::unique_ptr<Trk::CaloExtension> extension=m_theTrackExtrapolatorTool->caloExtension(Gaudi::Hive::currentContext(),*track);
      if (extension)
      {

      	// Extract the CurvilinearParameters per each layer-track intersection
	      const std::vector<Trk::CurvilinearParameters>& clParametersVector = extension->caloLayerIntersections();

      	for (auto clParameter : clParametersVector) {

      	  unsigned int parametersIdentifier = clParameter.cIdentifier();
	        CaloCell_ID::CaloSample intLayer;

      	  if (!m_trackParametersIdHelper->isValid(parametersIdentifier)) {
      	    std::cout << "Invalid Track Identifier"<< std::endl;
      	    intLayer = CaloCell_ID::CaloSample::Unknown;
      	  } else {
	        intLayer = m_trackParametersIdHelper->caloSample(parametersIdentifier);
      	  }
	      
	        parametersMap[intLayer] = std::make_pair<float,float>(clParameter.momentum().eta(),clParameter.momentum().phi());
        }        
      }
      else {
  	    ATH_MSG_WARNING("TrackExtension failed for track with pt and eta " << track->pt() << " and " << track->eta());
	    continue;
      }

      //  ---------Calo Sample layer Variables---------
      //  PreSamplerB=0, EMB1, EMB2, EMB3, // LAr barrel
      //  PreSamplerE, EME1, EME2, EME3,   // LAr EM endcap
      //  HEC0, HEC1, HEC2, HEC3,          // Hadronic end cap cal.
      //  TileBar0, TileBar1, TileBar2,    // Tile barrel
      //  TileGap1, TileGap2, TileGap3,    // Tile gap (ITC & scint)
      //  TileExt0, TileExt1, TileExt2,    // Tile extended barrel
      //  FCAL0, FCAL1, FCAL2,             // Forward EM endcap (excluded)
      //  Unknown

      // Presampler
      float trackEta_PreSamplerB_tmp = -999999999;
      float trackPhi_PreSamplerB_tmp = -999999999;
      float trackEta_PreSamplerE_tmp = -999999999;
      float trackPhi_PreSamplerE_tmp = -999999999;
      // LAr EM Barrel layers
      float trackEta_EMB1_tmp = -999999999;
      float trackPhi_EMB1_tmp = -999999999;
      float trackEta_EMB2_tmp = -999999999;
      float trackPhi_EMB2_tmp = -999999999;
      float trackEta_EMB3_tmp = -999999999;
      float trackPhi_EMB3_tmp = -999999999;
      // LAr EM Endcap layers
      float trackEta_EME1_tmp = -999999999;
      float trackPhi_EME1_tmp = -999999999;
      float trackEta_EME2_tmp = -999999999;
      float trackPhi_EME2_tmp = -999999999;
      float trackEta_EME3_tmp = -999999999;
      float trackPhi_EME3_tmp = -999999999;
      // Hadronic Endcap layers
      float trackEta_HEC0_tmp = -999999999;
      float trackPhi_HEC0_tmp = -999999999;
      float trackEta_HEC1_tmp = -999999999;
      float trackPhi_HEC1_tmp = -999999999;
      float trackEta_HEC2_tmp = -999999999;
      float trackPhi_HEC2_tmp = -999999999;
      float trackEta_HEC3_tmp = -999999999;
      float trackPhi_HEC3_tmp = -999999999;
      // Tile Barrel layers
      float trackEta_TileBar0_tmp = -999999999;
      float trackPhi_TileBar0_tmp = -999999999;
      float trackEta_TileBar1_tmp = -999999999;
      float trackPhi_TileBar1_tmp = -999999999;
      float trackEta_TileBar2_tmp = -999999999;
      float trackPhi_TileBar2_tmp = -999999999;
      // Tile Gap layers
      float trackEta_TileGap1_tmp = -999999999;
      float trackPhi_TileGap1_tmp = -999999999;
      float trackEta_TileGap2_tmp = -999999999;
      float trackPhi_TileGap2_tmp = -999999999;
      float trackEta_TileGap3_tmp = -999999999;
      float trackPhi_TileGap3_tmp = -999999999;
      // Tile Extended Barrel layers
      float trackEta_TileExt0_tmp = -999999999;
      float trackPhi_TileExt0_tmp = -999999999;
      float trackEta_TileExt1_tmp = -999999999;
      float trackPhi_TileExt1_tmp = -999999999;
      float trackEta_TileExt2_tmp = -999999999;
      float trackPhi_TileExt2_tmp = -999999999;

      //first is eta, second is phi
      if ( parametersMap.find(CaloCell_ID::CaloSample::PreSamplerB) != parametersMap.end() ){
        trackEta_PreSamplerB_tmp = parametersMap[CaloCell_ID::CaloSample::PreSamplerB].first;
        trackPhi_PreSamplerB_tmp = parametersMap[CaloCell_ID::CaloSample::PreSamplerB].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::PreSamplerE) != parametersMap.end() ){
        trackEta_PreSamplerE_tmp = parametersMap[CaloCell_ID::CaloSample::PreSamplerE].first;
        trackPhi_PreSamplerE_tmp = parametersMap[CaloCell_ID::CaloSample::PreSamplerE].second;
      }

      if ( parametersMap.find(CaloCell_ID::CaloSample::EMB1) != parametersMap.end() ){
        trackEta_EMB1_tmp = parametersMap[CaloCell_ID::CaloSample::EMB1].first;
        trackPhi_EMB1_tmp = parametersMap[CaloCell_ID::CaloSample::EMB1].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::EMB2) != parametersMap.end() ){
        trackEta_EMB2_tmp = parametersMap[CaloCell_ID::CaloSample::EMB2].first;
        trackPhi_EMB2_tmp = parametersMap[CaloCell_ID::CaloSample::EMB2].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::EMB3) != parametersMap.end() ){
        trackEta_EMB3_tmp = parametersMap[CaloCell_ID::CaloSample::EMB3].first;
        trackPhi_EMB3_tmp = parametersMap[CaloCell_ID::CaloSample::EMB3].second;
      }

      if ( parametersMap.find(CaloCell_ID::CaloSample::EME1) != parametersMap.end() ){
        trackEta_EME1_tmp = parametersMap[CaloCell_ID::CaloSample::EME1].first;
        trackPhi_EME1_tmp = parametersMap[CaloCell_ID::CaloSample::EME1].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::EME2) != parametersMap.end() ){
        trackEta_EME2_tmp = parametersMap[CaloCell_ID::CaloSample::EME2].first;
        trackPhi_EME2_tmp = parametersMap[CaloCell_ID::CaloSample::EME2].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::EME3) != parametersMap.end() ){
        trackEta_EME3_tmp = parametersMap[CaloCell_ID::CaloSample::EME3].first;
        trackPhi_EME3_tmp = parametersMap[CaloCell_ID::CaloSample::EME3].second;
      }
      
      if ( parametersMap.find(CaloCell_ID::CaloSample::HEC0) != parametersMap.end() ){
        trackEta_HEC0_tmp = parametersMap[CaloCell_ID::CaloSample::HEC0].first;
        trackPhi_HEC0_tmp = parametersMap[CaloCell_ID::CaloSample::HEC0].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::HEC1) != parametersMap.end() ){
        trackEta_HEC1_tmp = parametersMap[CaloCell_ID::CaloSample::HEC1].first;
        trackPhi_HEC1_tmp = parametersMap[CaloCell_ID::CaloSample::HEC1].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::HEC2) != parametersMap.end() ){
        trackEta_HEC2_tmp = parametersMap[CaloCell_ID::CaloSample::HEC2].first;
        trackPhi_HEC2_tmp = parametersMap[CaloCell_ID::CaloSample::HEC2].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::HEC3) != parametersMap.end() ){
        trackEta_HEC3_tmp = parametersMap[CaloCell_ID::CaloSample::HEC3].first;
        trackPhi_HEC3_tmp = parametersMap[CaloCell_ID::CaloSample::HEC3].second;
      }

      if ( parametersMap.find(CaloCell_ID::CaloSample::TileBar0) != parametersMap.end() ){
        trackEta_TileBar0_tmp = parametersMap[CaloCell_ID::CaloSample::TileBar0].first;
        trackPhi_TileBar0_tmp = parametersMap[CaloCell_ID::CaloSample::TileBar0].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileBar1) != parametersMap.end() ){
        trackEta_TileBar1_tmp = parametersMap[CaloCell_ID::CaloSample::TileBar1].first;
        trackPhi_TileBar1_tmp = parametersMap[CaloCell_ID::CaloSample::TileBar1].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileBar2) != parametersMap.end() ){
        trackEta_TileBar2_tmp = parametersMap[CaloCell_ID::CaloSample::TileBar2].first;
        trackPhi_TileBar2_tmp = parametersMap[CaloCell_ID::CaloSample::TileBar2].second;
      }
      
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileGap1) != parametersMap.end() ){
        trackEta_TileGap1_tmp = parametersMap[CaloCell_ID::CaloSample::TileGap1].first;
        trackPhi_TileGap1_tmp = parametersMap[CaloCell_ID::CaloSample::TileGap1].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileGap2) != parametersMap.end() ){
        trackEta_TileGap2_tmp = parametersMap[CaloCell_ID::CaloSample::TileGap2].first;
        trackPhi_TileGap2_tmp = parametersMap[CaloCell_ID::CaloSample::TileGap2].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileGap3) != parametersMap.end() ){
        trackEta_TileGap3_tmp = parametersMap[CaloCell_ID::CaloSample::TileGap3].first;
        trackPhi_TileGap3_tmp = parametersMap[CaloCell_ID::CaloSample::TileGap3].second;
      }
      
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileExt0) != parametersMap.end() ){
        trackEta_TileBar0_tmp = parametersMap[CaloCell_ID::CaloSample::TileExt0].first;
        trackPhi_TileBar0_tmp = parametersMap[CaloCell_ID::CaloSample::TileExt0].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileExt1) != parametersMap.end() ){
        trackEta_TileExt1_tmp = parametersMap[CaloCell_ID::CaloSample::TileExt1].first;
        trackPhi_TileExt1_tmp = parametersMap[CaloCell_ID::CaloSample::TileExt1].second;
      }
      if ( parametersMap.find(CaloCell_ID::CaloSample::TileExt2) != parametersMap.end() ){
        trackEta_TileExt2_tmp = parametersMap[CaloCell_ID::CaloSample::TileExt2].first;
        trackPhi_TileExt2_tmp = parametersMap[CaloCell_ID::CaloSample::TileExt2].second;
      }            
      
      m_trackEta_PreSamplerB.push_back(trackEta_PreSamplerB_tmp);
      m_trackPhi_PreSamplerB.push_back(trackPhi_PreSamplerB_tmp);
      m_trackEta_PreSamplerE.push_back(trackEta_PreSamplerE_tmp);
      m_trackPhi_PreSamplerE.push_back(trackPhi_PreSamplerE_tmp);

      m_trackEta_EMB1.push_back(trackEta_EMB1_tmp); 
      m_trackPhi_EMB1.push_back(trackPhi_EMB1_tmp); 
      m_trackEta_EMB2.push_back(trackEta_EMB2_tmp); 
      m_trackPhi_EMB2.push_back(trackPhi_EMB2_tmp); 
      m_trackEta_EMB3.push_back(trackEta_EMB3_tmp); 
      m_trackPhi_EMB3.push_back(trackPhi_EMB3_tmp); 

      m_trackEta_EME1.push_back(trackEta_EME1_tmp); 
      m_trackPhi_EME1.push_back(trackPhi_EME1_tmp); 
      m_trackEta_EME2.push_back(trackEta_EME2_tmp); 
      m_trackPhi_EME2.push_back(trackPhi_EME2_tmp); 
      m_trackEta_EME3.push_back(trackEta_EME3_tmp); 
      m_trackPhi_EME3.push_back(trackPhi_EME3_tmp); 

      m_trackEta_HEC0.push_back(trackEta_HEC0_tmp); 
      m_trackPhi_HEC0.push_back(trackPhi_HEC0_tmp); 
      m_trackEta_HEC1.push_back(trackEta_HEC1_tmp); 
      m_trackPhi_HEC1.push_back(trackPhi_HEC1_tmp); 
      m_trackEta_HEC2.push_back(trackEta_HEC2_tmp); 
      m_trackPhi_HEC2.push_back(trackPhi_HEC2_tmp); 
      m_trackEta_HEC3.push_back(trackEta_HEC3_tmp); 
      m_trackPhi_HEC3.push_back(trackPhi_HEC3_tmp); 

      m_trackEta_TileBar0.push_back(trackEta_TileBar0_tmp); 
      m_trackPhi_TileBar0.push_back(trackPhi_TileBar0_tmp); 
      m_trackEta_TileBar1.push_back(trackEta_TileBar1_tmp); 
      m_trackPhi_TileBar1.push_back(trackPhi_TileBar1_tmp); 
      m_trackEta_TileBar2.push_back(trackEta_TileBar2_tmp); 
      m_trackPhi_TileBar2.push_back(trackPhi_TileBar2_tmp); 

      m_trackEta_TileGap1.push_back(trackEta_TileGap1_tmp); 
      m_trackPhi_TileGap1.push_back(trackPhi_TileGap1_tmp); 
      m_trackEta_TileGap2.push_back(trackEta_TileGap2_tmp); 
      m_trackPhi_TileGap2.push_back(trackPhi_TileGap2_tmp); 
      m_trackEta_TileGap3.push_back(trackEta_TileGap3_tmp); 
      m_trackPhi_TileGap3.push_back(trackPhi_TileGap3_tmp); 

      m_trackEta_TileExt0.push_back(trackEta_TileExt0_tmp);
      m_trackPhi_TileExt0.push_back(trackPhi_TileExt0_tmp);
      m_trackEta_TileExt1.push_back(trackEta_TileExt1_tmp);
      m_trackPhi_TileExt1.push_back(trackPhi_TileExt1_tmp);
      m_trackEta_TileExt2.push_back(trackEta_TileExt2_tmp);
      m_trackPhi_TileExt2.push_back(trackPhi_TileExt2_tmp);

      m_nTrack++;
    }
  }
  if(m_doJets)
  {
    for(unsigned int iColl=0; iColl < m_jetContainerNames.size(); iColl++)
    {
      std::string jetCollName(m_jetContainerNames.at(iColl));
      bool isTruthJetColl=(jetCollName.find("Truth")!=std::string::npos);
      const xAOD::JetContainer* jetColl = nullptr;
      CHECK( evtStore()->retrieve( jetColl,jetCollName));
      std::vector<float>& v_pt=m_jet_pt.at(iColl);
      std::vector<float>& v_eta=m_jet_eta.at(iColl);
      std::vector<float>& v_phi=m_jet_phi.at(iColl);
      std::vector<float>& v_E=m_jet_E.at(iColl);
      std::vector<int>& v_flavor=m_jet_flavor.at(iColl);
      v_pt.clear();
      v_eta.clear();
      v_phi.clear();
      v_E.clear();
      v_pt.reserve(jetColl->size());
      v_eta.reserve(jetColl->size());
      v_phi.reserve(jetColl->size());
      v_E.reserve(jetColl->size());
	
      if(isTruthJetColl) 
      {
	v_flavor.clear();
	v_flavor.reserve(jetColl->size());
      }
      for(auto jet : *jetColl)
      {
	xAOD::JetFourMom_t jet_p4;
	if(isTruthJetColl)
	{
	  jet_p4=jet->jetP4();
	  int jetFlavor=-1;
	  bool status = jet->getAttribute<int>("PartonTruthLabelID", jetFlavor);
	  if(status) jet->getAttribute("PartonTruthLabelID",jetFlavor);
	    v_flavor.push_back(jetFlavor);
	}
	else jet_p4=jet->jetP4(xAOD::JetConstitScaleMomentum);
	v_pt.push_back(jet_p4.Pt()*1e-3);
	v_eta.push_back(jet_p4.Eta());
	v_phi.push_back(jet_p4.Phi());
	v_E.push_back(jet_p4.E()*1e-3);
      }
    }
  }
  // Calo clusters
  if(m_doClusters && m_doTaus)
  {

    m_nClusterVec.clear();
    m_cluster_nCells.clear();
    m_cluster_TauAssoc.clear();
    m_cluster_E.clear();
    m_cluster_rawE.clear();
    m_cluster_E_LCCalib.clear();
    m_cluster_Pt.clear();
    m_cluster_Eta.clear();
    m_cluster_Phi.clear();
    m_cluster_Pt_vcorr.clear();
    m_cluster_Eta_vcorr.clear();
    m_cluster_Phi_vcorr.clear();
    m_cluster_ENG_CALIB_TOT.clear();
    m_cluster_ENG_CALIB_OUT_T.clear();
    m_cluster_ENG_CALIB_DEAD_TOT.clear();
    m_cluster_EM_PROBABILITY.clear();
    m_cluster_HAD_WEIGHT.clear();
    m_cluster_OOC_WEIGHT.clear();
    m_cluster_DM_WEIGHT.clear();
    m_cluster_CENTER_MAG.clear();
    m_cluster_FIRST_ENG_DENS.clear();
    m_cluster_CENTER_LAMBDA.clear();
    m_cluster_ISOLATION.clear();
    m_cluster_ENERGY_DigiHSTruth.clear();
    m_cluster_tau_dR.clear();

    std::vector<const xAOD::CaloCluster*> clusterVector;
    std::vector<int> clusterTauAssoc;

    const xAOD::TauJetContainer* tau_cont = nullptr;
    CHECK(evtStore()->retrieve(tau_cont, "TauJets"));
    int clusterAssoc = 0;
    std::multimap<float,unsigned int,std::greater<float> > clusterRanks;

    for(const xAOD::TauJet* recoTau: *tau_cont){
      
      clusterVector.reserve( recoTau->nClusters() );
      clusterTauAssoc.reserve( recoTau->nClusters() ); 

      bool pass_selection = false;
      const auto truthTau = m_tauTruthMatchingTool->applyTruthMatch(*recoTau);

      if(recoTau->auxdata<char>("IsTruthMatched")&& truthTau)
      {
        bool isHadronic = truthTau->auxdata<char>("IsHadronicTau");
        if (isHadronic) { pass_selection = true;}
      }

      if(pass_selection){

        m_nClusterVec.push_back(recoTau->nClusters());

        for ( int i = 0; i < recoTau->nClusters(); i++ ) 
        {
          const xAOD::IParticle* particle = recoTau->cluster(i);
          const xAOD::CaloCluster* cluster = static_cast<const xAOD::CaloCluster*>(particle);           
          float clusterE = cluster->e()*1e-3;
          float clusterEta = cluster->eta();
          //if (clusterE < m_clusterE_min || clusterE > m_clusterE_max || std::abs(clusterEta) > m_clusterEtaAbs_max) continue;
          clusterRanks.emplace_hint(clusterRanks.end(), clusterE, i);
          clusterVector.push_back( cluster );
          clusterTauAssoc.push_back( clusterAssoc );
        }
        clusterAssoc += 1;
      }

    }

    //m_nCluster = clusterVector.size();

    m_nCluster = clusterRanks.size();
	  m_nClusterFound = clusterRanks.size();

    m_cluster_nCells.reserve(m_nCluster);
    m_cluster_E.reserve(m_nCluster);
    m_cluster_rawE.reserve(m_nCluster);
    m_cluster_E_LCCalib.reserve(m_nCluster);
    m_cluster_Pt.reserve(m_nCluster);
    m_cluster_Eta.reserve(m_nCluster);
    m_cluster_Phi.reserve(m_nCluster);
    m_cluster_Pt_vcorr.reserve(m_nCluster);
    m_cluster_Eta_vcorr.reserve(m_nCluster);
    m_cluster_Phi_vcorr.reserve(m_nCluster);
    m_cluster_ENG_CALIB_TOT.reserve(m_nCluster);
    m_cluster_ENG_CALIB_OUT_T.reserve(m_nCluster);
    m_cluster_ENG_CALIB_DEAD_TOT.reserve(m_nCluster);
    m_cluster_EM_PROBABILITY.reserve(m_nCluster);
    m_cluster_HAD_WEIGHT.reserve(m_nCluster);
    m_cluster_OOC_WEIGHT.reserve(m_nCluster);
    m_cluster_DM_WEIGHT.reserve(m_nCluster);
    m_cluster_CENTER_MAG.reserve(m_nCluster);
    m_cluster_FIRST_ENG_DENS.reserve(m_nCluster);
    m_cluster_CENTER_LAMBDA.reserve(m_nCluster);
    m_cluster_ISOLATION.reserve(m_nCluster);
    m_cluster_ENERGY_DigiHSTruth.reserve(m_nCluster);

    m_cluster_tau_dR.reserve(m_nCluster);

    if (m_doClusterCells) 
    {
      m_cluster_cell_ID.assign(m_nCluster,std::vector<size_t>());
      m_cluster_cell_E.assign(m_nCluster,std::vector<float>());
      m_cluster_cell_Eta.assign(m_nCluster,std::vector<float>());
      m_cluster_cell_Phi.assign(m_nCluster,std::vector<float>());
      m_cluster_cell_Sampling.assign(m_nCluster,std::vector<float>());
      m_cluster_cell_hitsE_EM.assign(m_nCluster,std::vector<float>());
      if (m_doCalibHits)
      {
        m_cluster_cell_hitsE_nonEM.assign(m_nCluster,std::vector<float>());
        m_cluster_cell_hitsE_Invisible.assign(m_nCluster,std::vector<float>());
        m_cluster_cell_hitsE_Escaped.assign(m_nCluster,std::vector<float>());      
      }
      if (m_doTruthParticles)
      {
        m_cluster_hitsTruthIndex.assign(m_nCluster,std::vector<int>());  
        m_cluster_hitsTruthE.assign(m_nCluster,std::vector<float>());    
      }
    }

    unsigned int jCluster = 0;
    for (int i = 0; i < clusterVector.size(); i++)
    {
      const xAOD::IParticle* particle = clusterVector[i];
      const xAOD::CaloCluster* calibratedCluster = static_cast<const xAOD::CaloCluster*>(particle);
      auto cluster = calibratedCluster;
      if (m_doUncalibratedClusters) cluster=calibratedCluster->getSisterCluster();

      m_cluster_nCells.push_back(cluster->size());
      m_cluster_E.push_back(cluster->e()*1e-3);
      m_cluster_rawE.push_back(cluster->rawE()*1e-3);
      m_cluster_E_LCCalib.push_back(cluster->e()*1e-3);
      m_cluster_Pt.push_back(cluster->pt()*1e-3);
      m_cluster_Eta.push_back(cluster->eta());
      m_cluster_Phi.push_back(cluster->phi());

      m_cluster_TauAssoc.push_back(clusterTauAssoc[i]);

      if(m_doClusterMoments)
      {
        double cluster_ENG_CALIB_TOT = 0;
        double cluster_ENG_CALIB_OUT_T = 0;
        double cluster_ENG_CALIB_DEAD_TOT = 0;
        double cluster_EM_PROBABILITY = 0;
        double cluster_HAD_WEIGHT = 0;
        double cluster_OOC_WEIGHT = 0;
        double cluster_DM_WEIGHT = 0;
        double cluster_CENTER_MAG = 0;
        double cluster_FIRST_ENG_DENS = 0;
        double cluster_CENTER_LAMBDA = 0;
        double cluster_ISOLATION = 0;
        double cluster_ENERGY_DigiHSTruth = 0;


        if( !cluster->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_TOT, cluster_ENG_CALIB_TOT) ) cluster_ENG_CALIB_TOT=-1.;
        else cluster_ENG_CALIB_TOT*=1e-3;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_T, cluster_ENG_CALIB_OUT_T) ) cluster_ENG_CALIB_OUT_T=-1.;
        else cluster_ENG_CALIB_OUT_T*=1e-3;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_TOT, cluster_ENG_CALIB_DEAD_TOT) ) cluster_ENG_CALIB_DEAD_TOT=-1.;
        else cluster_ENG_CALIB_DEAD_TOT*=1e-3;
        
        if( !cluster->retrieveMoment( xAOD::CaloCluster::CENTER_MAG, cluster_CENTER_MAG) ) cluster_CENTER_MAG=-1.;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::FIRST_ENG_DENS, cluster_FIRST_ENG_DENS) ) cluster_FIRST_ENG_DENS=-1.;
        else cluster_FIRST_ENG_DENS*=1e-3;

        if( !cluster->retrieveMoment( xAOD::CaloCluster::CENTER_LAMBDA, cluster_CENTER_LAMBDA) ) cluster_CENTER_LAMBDA=-1.;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::ISOLATION, cluster_ISOLATION) ) cluster_ISOLATION=-1.;
            
        //for moments related to the calibration, use calibratedCluster or they will be undefined
        if( !cluster->retrieveMoment( xAOD::CaloCluster::EM_PROBABILITY, cluster_EM_PROBABILITY) ) cluster_EM_PROBABILITY = -1.;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::HAD_WEIGHT, cluster_HAD_WEIGHT) ) cluster_HAD_WEIGHT=-1.;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::OOC_WEIGHT, cluster_OOC_WEIGHT) ) cluster_OOC_WEIGHT=-1.;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::DM_WEIGHT, cluster_DM_WEIGHT) ) cluster_DM_WEIGHT=-1.;
        if( !cluster->retrieveMoment( xAOD::CaloCluster::ENERGY_DigiHSTruth, cluster_ENERGY_DigiHSTruth) ) cluster_ENERGY_DigiHSTruth=-999.;

        m_cluster_ENG_CALIB_TOT.push_back(cluster_ENG_CALIB_TOT);
        m_cluster_ENG_CALIB_OUT_T.push_back(cluster_ENG_CALIB_OUT_T);
        m_cluster_ENG_CALIB_DEAD_TOT.push_back(cluster_ENG_CALIB_DEAD_TOT);
        m_cluster_EM_PROBABILITY.push_back(cluster_EM_PROBABILITY);
        m_cluster_HAD_WEIGHT.push_back(cluster_HAD_WEIGHT);
        m_cluster_OOC_WEIGHT.push_back(cluster_OOC_WEIGHT);
        m_cluster_DM_WEIGHT.push_back(cluster_DM_WEIGHT);
        m_cluster_CENTER_MAG.push_back(cluster_CENTER_MAG);
        m_cluster_FIRST_ENG_DENS.push_back(cluster_FIRST_ENG_DENS);
        m_cluster_CENTER_LAMBDA.push_back(cluster_CENTER_LAMBDA);
        m_cluster_ISOLATION.push_back(cluster_ISOLATION);
        m_cluster_ENERGY_DigiHSTruth.push_back(cluster_ENERGY_DigiHSTruth);
      }

      if(m_doClusterCells)
      {

        std::vector<size_t>& cluster_cell_ID=m_cluster_cell_ID[i];	
        std::vector<float>& cluster_cell_E=m_cluster_cell_E[i];
        std::vector<float>& cluster_cell_Eta=m_cluster_cell_Eta[i];
        std::vector<float>& cluster_cell_Phi=m_cluster_cell_Phi[i];
        std::vector<float>& cluster_cell_Sampling=m_cluster_cell_Sampling[i];
        std::vector<float>& cluster_cell_hitsE_EM=m_cluster_cell_hitsE_EM[i];
        std::vector<float>& cluster_cell_hitsE_nonEM=m_cluster_cell_hitsE_nonEM[i];
        std::vector<float>& cluster_cell_hitsE_Invisible=m_cluster_cell_hitsE_Invisible[i];
        std::vector<float>& cluster_cell_hitsE_Escaped=m_cluster_cell_hitsE_Escaped[i];
        std::vector<int>& cluster_hitsTruthIndex=m_cluster_hitsTruthIndex[i];
        std::vector<float>& cluster_hitsTruthE=m_cluster_hitsTruthE[i];

        auto nCells_cl=cluster->size();
        cluster_cell_ID.reserve(nCells_cl);
        cluster_cell_E.reserve(nCells_cl);
        cluster_cell_Eta.reserve(nCells_cl);
        cluster_cell_Phi.reserve(nCells_cl);
        cluster_cell_Sampling.reserve(nCells_cl);
            
        if(m_doCalibHits && m_doCalibHitsPerCell)
        {
          cluster_cell_hitsE_EM.reserve(nCells_cl);
          cluster_cell_hitsE_nonEM.reserve(nCells_cl);
          cluster_cell_hitsE_Invisible.reserve(nCells_cl);
          cluster_cell_hitsE_Escaped.reserve(nCells_cl);
        }

        //keep track of how much each truth particle contributes to cluster's total calibration hits energy
        std::map<unsigned int, float> truthIndexEnergyMap;

        for(CaloClusterCellLink::const_iterator it_cell = cluster->cell_begin(); it_cell != cluster->cell_end(); it_cell++)
        {
          const CaloCell* cell = (*it_cell);
          float cellE = cell->e()*(it_cell.weight())*1e-3;
          if(cellE < m_cellE_thres) continue;
          auto caloDDE = cell->caloDDE();
          cluster_cell_ID.push_back(cell->ID().get_identifier32().get_compact());
          cluster_cell_E.push_back(cellE);//cellE);
          cluster_cell_Eta.push_back(caloDDE->eta());//cell->eta());
          cluster_cell_Phi.push_back(caloDDE->phi());//cell->phi());
          //auto caloDDE = cell->caloDDE();
          cluster_cell_Sampling.push_back(caloDDE->getSampling());
          if(m_doCalibHits)
          {
            float energy_EM=0.;
            float energy_nonEM=0.;
            float energy_Invisible=0.;
            float energy_Escaped=0.;
              
            for(auto hit_container : v_calibHitContainer)
            {	    
              for(auto ch : *hit_container)
              {
                if (ch->cellID()!=cell->ID()) continue;
                energy_EM+=ch->energyEM();
                energy_nonEM+=ch->energyNonEM();
                energy_Invisible+=ch->energyInvisible();
                energy_Escaped+=ch->energyEscaped();
                if(m_doTruthParticles)
                {
                  unsigned int barcode = ch->particleID();
                  const auto mapItr=truthBarcodeMap.find(barcode);
                  if(mapItr!=truthBarcodeMap.end())  truthIndexEnergyMap[mapItr->second]+=ch->energyTotal();
                }
              }
            }//end loop on calib hits containers
            if(m_doCalibHitsPerCell)
            {
              cluster_cell_hitsE_EM.push_back(energy_EM*1e-3);
              cluster_cell_hitsE_nonEM.push_back(energy_nonEM*1e-3);
              cluster_cell_hitsE_Invisible.push_back(energy_Invisible*1e-3);
              cluster_cell_hitsE_Escaped.push_back(energy_Escaped*1e-3);
            }
          }//end m_doCalibHits
        }//end cell loop
        if(m_doTruthParticles && m_doCalibHits) 
        {
          //now sort by energy instead of index
          std::multimap<float, unsigned int, std::greater<float> > truthRanks;
          for(auto mItr : truthIndexEnergyMap) truthRanks.emplace_hint(truthRanks.end(),mItr.second,mItr.first);
          //Should we put a maximum and keep only the top 2 or 3 contributors to the cluster?
          cluster_hitsTruthIndex.reserve(truthRanks.size());
          cluster_hitsTruthE.reserve(truthRanks.size());
          unsigned int maxAssoc=(m_numClusterTruthAssoc<0) ? truthRanks.size() : m_numClusterTruthAssoc;
          for(auto mItr=truthRanks.begin(); (mItr!=truthRanks.end() && cluster_hitsTruthIndex.size() < maxAssoc); mItr++)
          {
            cluster_hitsTruthIndex.push_back(mItr->second);
            cluster_hitsTruthE.push_back(mItr->first*1e-3);
          }
        }//end m_doTruthParticles
      }
    }
  }

  m_eventTree->Fill();
  return StatusCode::SUCCESS;

}

StatusCode MLTreeMaker::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;

}


