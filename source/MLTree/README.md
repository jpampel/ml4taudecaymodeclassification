# Instructions

setupATLAS    
lsetup git    
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git    
cd athena    
git checkout -b 22.0 release/22.0.48    
cd ..    
git clone https://github.com/atlas-calo-ml/MLTree.git athena/MLTree    
echo "+ MLTree" > package_filters.txt    
echo "- .*" >> package_filters.txt    
mkdir build; cd build;    
asetup Athena,22.0.48    
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir    
make