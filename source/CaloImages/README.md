# Setup
This code was tested using the following ATLAS release:
```
    asetup athena,22.0.107
```
A small number of Python libraries are needed, and can be installed as follows:
```
pip3 install --user (--upgrade) numpy progressbar 
```
If any of these packages are already installed, you should include the --upgrade tag in the pip command. This code was developed using the following versions:
- numpy: 1.21.1
- ROOT: 6.24/06
- progressbar: 4.0.0

# Usage
This code takes as input a ROOT file produced by the MLTree framework ([Github link](https://github.com/atlas-calo-ml/MLTree)). The output files included in the testImages_barrel/ and testImages_endcaps/ folder were produced from the following sample: mc16_13TeV.425200.Pythia8EvtGen_A14NNPDF23LO_Gammatautau_MassWeight.merge.AOD.e5468_e5984_s3126_r11153_r11154.    
To run this code do the following:
```
python3 make_calo_image_barrel.py <INPUT FILE>
python3 make_calo_image_endcaps.py <INPUT FILE>
```
