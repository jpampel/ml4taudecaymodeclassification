import ROOT
import sys
import progressbar
import os
import numpy as np
# import h5py # To process very big numpy-arrays that do not fit into the RAM, this packet could be included (and the code adjusted)

#The following is the folder into which the output numpy arrays are saved.
basedir = './testImages_barrel'

# The following method gives back the size of the pictures to be generated depending on which Calorimeter they stem from.
def getBinning( sampling ):
    # EMB1
    if sampling == "EMB1":
        return 128, 4
    # EMB2
    if sampling == "EMB2":
        return 16, 16
    # EMB3
    if sampling == "EMB3":
        return 8, 16
    #Tile1
    if sampling == "Tile1":
        return 4, 4
    #Tile2
    if sampling == "Tile2":
        return 4, 4
    #Tile3
    if sampling == "Tile3":
        return 2, 4

# This is the list of possible decay modes. The input of this dictionary is the number from the variable tauTruthDecayMode
decayModeDict = {
    0: '1p0n',
    1: '1p1n',
    2: '1pXn',
    3: '3p0n',
    4: '3pXn',
    5: 'Other'
}

# This creates the directory for the Numpy arrays.
try:
    os.mkdir(f'{basedir}')
except OSError:
    print ( f"Skipping creation of {basedir} because its creation failed. This is probably the case because it already exists." )

# This processes the input file, given as an argument to the script.
try:
    in_file = ROOT.TFile ( sys.argv[1] )
except IndexError:
    print ( "Missing input root file." )
    sys.exit ( -1 )
except ValueError:
    print ( "Wrong format of input file (needs to be a ROOT.TFile)." )
    sys.exit ( -1 )

selection_tree = in_file.Get ( 'EventTree' )

ROOT.gROOT.SetBatch ( )
ROOT.gErrorIgnoreLevel = 6001

# These arrays are the output of this python script. They are essentially lists of images.
# With my tested input file, they have a length of approx. 580 000 entries (i.e. images) and a size of altogether about 600MB.
# Using lists here is faster than working with numpy.append because the latter always creates a new list instead of appending.
EMB1_out = []
EMB2_out = []
EMB3_out = []
Tile1_out = []
Tile2_out = []
Tile3_out = []
labels_out = [] #labels, specifying one of the decay modes
additional_out = [] #additional information on the tau jet

#loop over events
for index, event in progressbar.progressbar(enumerate(selection_tree)):

    nMatchedTaus = selection_tree.nMatchedTaus
    nClusterPerTau = selection_tree.nClusterVec

    if nMatchedTaus != 1 and nMatchedTaus != 2:
        continue

    eventNumber = selection_tree.eventNumber

    #loop over taus
    for tau in range ( nMatchedTaus ):

        tauE = selection_tree.tauCaloOnlyE[tau]
        tauPhi = selection_tree.tauCaloOnlyPhi[tau]
        tauEta = selection_tree.tauCaloOnlyEta[tau]
        tauPt = selection_tree.tauCaloOnlyPt[tau]
        # the next two variables are not used.
        #tauDecayMode = selection_tree.tauDecayMode[tau]
        #tauDecayModeNN = selection_tree.tauDecayModeNN[tau]
        tauTruthDecayMode = selection_tree.tauTruthDecayMode[tau]


        if abs ( tauEta ) > 1.15:
            # The barrel of the electromagnetic calorimeter covers the pseudorapidity range |eta| < 1.475 (2008 JINST 3 S08003, Table 1.3).
            # But the third layer only covers the range |η| < 1.35
            # The Tile barrel of the hadronic calorimeter covers the pseudorapidity range |eta| < 1, whereas the TileExt barrel covers the range 0.8 < |eta| < 1.7.
            # The pictures cover a range of ( centreEta - 0.2 , centreEta + 0.2 ), so |eta| < 1.15 should be possible.
            continue

        # initializing histograms
        hist_EMB1 = ROOT.TH2D('hist_EMB1_{}_tau{}'.format(eventNumber, tau), 'hist_EMB1_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 128, -0.2, 0.2)
        hist_EMB2 = ROOT.TH2D('hist_EMB2_{}_tau{}'.format(eventNumber, tau), 'hist_EMB2_{}_tau{}'.format(eventNumber, tau), 16, -0.2, 0.2, 16, -0.2, 0.2)
        hist_EMB3 = ROOT.TH2D('hist_EMB3_{}_tau{}'.format(eventNumber, tau), 'hist_EMB3_{}_tau{}'.format(eventNumber, tau), 16, -0.2, 0.2, 8, -0.2, 0.2)
        hist_Tile1 = ROOT.TH2D('hist_Tile1_{}_tau{}'.format(eventNumber, tau), 'hist_Tile1_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 4, -0.2, 0.2)
        hist_Tile2 = ROOT.TH2D('hist_Tile2_{}_tau{}'.format(eventNumber, tau), 'hist_Tile2_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 4, -0.2, 0.2)
        hist_Tile3 = ROOT.TH2D('hist_Tile3_{}_tau{}'.format(eventNumber, tau), 'hist_Tile3_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 2, -0.2, 0.2)
        
        nCluster = sum(nClusterPerTau)
        if nCluster == 0:
            continue
        
        # The following piece of code defines the center of the tau jet by taking the cell with the maximal energy
        # (called maxIndex) and then setting centrePhi and centreEta to the values of that cell.
        centreOnCluster = True
        if centreOnCluster:
            maxIndexCluster = -1
            maxIndexCell = -1
            maxE = -1
            for i, cluster_E in enumerate(selection_tree.cluster_E):

                if selection_tree.cluster_TauAssoc[i] != tau:
                    continue
                nCells = len(selection_tree.cluster_cell_E[i])

                for cell in range(nCells):

                    cell_E = selection_tree.cluster_cell_E[i][cell]

                    if selection_tree.cluster_cell_Sampling[i][cell] != 2 :
                        # The centre of the cell needs to be found in EMB2, i.e. Sampling = 2
                        continue
                    if cell_E > maxE:
                        maxE = cell_E
                        maxIndexCluster = i
                        maxIndexCell = cell  

            if maxIndexCell == -1 or maxIndexCluster == -1:
                centrePhi = 0
                centreEta = 0
            else:
                centrePhi = selection_tree.cluster_cell_Phi[maxIndexCluster][maxIndexCell]
                centreEta = selection_tree.cluster_cell_Eta[maxIndexCluster][maxIndexCell]
        else:
            centrePhi = tauPhi
            centreEta = tauEta

        # This piece of code produces the images by adding each cell of each cluster to the histograms.
        for cluster in range(nCluster):

            cluster_E = selection_tree.cluster_E[cluster]
            cluster_Phi = selection_tree.cluster_Phi[cluster]
            cluster_Eta = selection_tree.cluster_Eta[cluster]
            cluster_Assoc = selection_tree.cluster_TauAssoc[cluster]
            nCells = selection_tree.cluster_nCells[cluster]

            if cluster_E < 1:
                continue

            if cluster_Assoc != tau:
                continue

            nCells = len(selection_tree.cluster_cell_E[cluster])

            for cell in range(nCells):

                cell_E = selection_tree.cluster_cell_E[cluster][cell] / tauE
                cell_Phi = selection_tree.cluster_cell_Phi[cluster][cell] - centrePhi
                cell_Eta = selection_tree.cluster_cell_Eta[cluster][cell] - centreEta
                cell_Sampling = selection_tree.cluster_cell_Sampling[cluster][cell]

                # Source: https://gitlab.cern.ch/atlas/athena/-/blob/21.2/Calorimeter/CaloGeoHelpers/CaloGeoHelpers/CaloSampling.def
                # cell_Sampling = 1 to 3 is EMB
                if cell_Sampling == 1:
                    hist_EMB1.Fill(cell_Phi, cell_Eta, cell_E)
                elif cell_Sampling == 2:
                    hist_EMB2.Fill(cell_Phi, cell_Eta, cell_E)
                elif cell_Sampling == 3:
                    hist_EMB3.Fill(cell_Phi, cell_Eta, cell_E)           
                # cell_Sampling = 12 to 14 is TileBar, cell_Sampling = 18 to 20 is TileExt    
                elif cell_Sampling == 12 or cell_Sampling == 18:
                    hist_Tile1.Fill(cell_Phi, cell_Eta, cell_E)
                elif cell_Sampling == 13 or cell_Sampling == 19:
                    hist_Tile2.Fill(cell_Phi, cell_Eta, cell_E)
                elif cell_Sampling == 14 or cell_Sampling == 20:
                    hist_Tile3.Fill(cell_Phi, cell_Eta, cell_E)


        # Save data in numpy arrays.
        columns , rows = getBinning("EMB1")
        pixels_EMB1 = np.ndarray ( shape = ( getBinning ( "EMB1" )[0] , getBinning ( "EMB1" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_EMB1.GetBinContent( row + 1 , column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_EMB1 [column] [row] = value
        EMB1_out.append ( pixels_EMB1 )

        columns , rows = getBinning("EMB2")
        pixels_EMB2 = np.ndarray ( shape = ( getBinning ( "EMB2" )[0] , getBinning ( "EMB2" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_EMB2.GetBinContent( row + 1 , column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_EMB2 [column] [row] = value
        EMB2_out.append ( pixels_EMB2 )

        columns , rows = getBinning("EMB3")
        pixels_EMB3 = np.ndarray ( shape = ( getBinning ( "EMB3" )[0] , getBinning ( "EMB3" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_EMB3.GetBinContent( row + 1 , column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_EMB3 [column] [row] = value
        EMB3_out.append ( pixels_EMB3 )

        columns , rows = getBinning("Tile1")
        pixels_Tile1 = np.ndarray ( shape = ( getBinning ( "Tile1" )[0] , getBinning ( "Tile1" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_Tile1.GetBinContent( row + 1 , column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_Tile1 [column] [row] = value
        Tile1_out.append ( pixels_Tile1 )

        columns , rows = getBinning("Tile2")
        pixels_Tile2 = np.ndarray ( shape = ( getBinning ( "Tile2" )[0] , getBinning ( "Tile2" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_Tile2.GetBinContent( row + 1 , column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_Tile2 [column] [row] = value
        Tile2_out.append ( pixels_Tile2 )

        columns , rows = getBinning("Tile3")
        pixels_Tile3 = np.ndarray ( shape = ( getBinning ( "Tile3" )[0] , getBinning ( "Tile3" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_Tile3.GetBinContent( row + 1 , column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_Tile3 [column] [row] = value
        Tile3_out.append ( pixels_Tile3 )

        # Save additional data
        additional_out.append ( [ centreEta , centrePhi , tauPt , tauE ] )

        # Save labels.
        labels_out.append ( tauTruthDecayMode )
        

    if index > 10000000: #100000:
        break


EMB1_out = np.array ( EMB1_out , dtype = np.uint8 )
EMB2_out = np.array ( EMB2_out , dtype = np.uint8 )
EMB3_out = np.array ( EMB3_out , dtype = np.uint8 )
Tile1_out = np.array ( Tile1_out , dtype = np.uint8 )
Tile2_out = np.array ( Tile2_out , dtype = np.uint8 )
Tile3_out = np.array ( Tile3_out , dtype = np.uint8 )
additional_out = np.array ( additional_out , dtype = np.float16 )
labels_out = np.array ( labels_out , dtype = np.uint8 )

print ( EMB1_out.shape )
print ( EMB2_out.shape )
print ( EMB3_out.shape )
print ( Tile1_out.shape )
print ( Tile2_out.shape )
print ( Tile3_out.shape )
print ( additional_out.shape )
print ( labels_out.shape )

try:
    np.save ( basedir + "/EMB1.npy" , EMB1_out )
    np.save ( basedir + "/EMB2.npy" , EMB2_out )
    np.save ( basedir + "/EMB3.npy" , EMB3_out )
    np.save ( basedir + "/Tile1.npy" , Tile1_out )
    np.save ( basedir + "/Tile2.npy" , Tile2_out )
    np.save ( basedir + "/Tile3.npy" , Tile3_out )
    np.save ( basedir + "/additional.npy" , additional_out )
    np.save ( basedir + "/labels.npy" , labels_out )
except OSError:
    print("Could not save numpy arrays in the output folder specified in the variable 'basedir'.")
