import ROOT
import sys
import progressbar
import os
import numpy as np
# import h5py # To process very big numpy-arrays that do not fit into the RAM, this packet could be included (and the code adjusted)

#The following is the folder into which the output numpy arrays are saved.
basedir = './testImages_endcaps'

# The following method gives back the size of the pictures to be generated depending on which Calorimeter they stem from.
def getBinning( sampling ):
    # EME1 : 1.5 < η < 1.8 : 128 , 4
    #        1.8 < η < 2.0 : 96 , 4
    #        2.0 < η < 2.4 : 64 , 4
    #        2.4 < η < 2.5 : 16 , 4
    # -> 384 , 4 necessary as smallest common multiple
    # EME2 : 1.5 < η < 2.5 : 16 , 16
    # EME3 : 1.5 < η < 2.5 : 8 , 16

    # EME1
    if sampling == "EME1":
        return 384, 4
    # EME2
    if sampling == "EME2":
        return 16, 16
    # EME3
    if sampling == "EME3":
        return 8, 16
    #HEC1
    if sampling == "HEC1":
        return 4, 4
    #HEC2
    if sampling == "HEC2":
        return 4, 4
    #HEC3
    if sampling == "HEC3":
        return 4, 4
    #HEC4
    if sampling == "HEC4":
        return 4, 4

# This is the list of possible decay modes. The input of this dictionary is the number from the variable tauTruthDecayMode
decayModeDict = {
    0: '1p0n',
    1: '1p1n',
    2: '1pXn',
    3: '3p0n',
    4: '3pXn',
    5: 'Other'
}

# This creates the directory for the Numpy arrays.
try:
    os.mkdir(f'{basedir}')
except OSError:
    print ( f"Skipping creation of {basedir} because its creation failed. This is probably the case because it already exists." )

# This processes the input file, given as an argument to the script.
try:
    in_file = ROOT.TFile ( sys.argv[1] )
except IndexError:
    print ( "Missing input root file." )
    sys.exit ( -1 )
except ValueError:
    print ( "Wrong format of input file (needs to be a ROOT.TFile)." )
    sys.exit ( -1 )

selection_tree = in_file.Get ( 'EventTree' )

ROOT.gROOT.SetBatch ( )
ROOT.gErrorIgnoreLevel = 6001

# These arrays are the output of this python script. They are essentially lists of images.
# With my tested input file, they have a length of approx. 580 000 entries (i.e. images) and a size of altogether about 600MB.
# Using lists here is faster than working with numpy.append because the latter always creates a new list instead of appending.
EME1_Resolution_out = [] #additional layer for resolution information of the EME1 layer
EME1_out = []
EME2_out = []
EME3_out = []
HEC1_out = []
HEC2_out = []
HEC3_out = []
HEC4_out = []
labels_out = [] #labels, specifying one of the decay modes
additional_out = [] #additional information on the tau jet

# ONLY FOR DEBUGGING OF EME1-RESOLUTION:
# set debuggingOutput to True if the EME1-resolution does not seem to fit to the energy values.
debuggingOutput = False
if debuggingOutput == True:
    tauIndex = 0
    firstPixelNumber = []
    firstFullCell_PixelNumber = []
# END OF DEBUGGING

#loop over events
for index, event in progressbar.progressbar(enumerate(selection_tree)):

    nMatchedTaus = selection_tree.nMatchedTaus
    nClusterPerTau = selection_tree.nClusterVec

    if nMatchedTaus != 1 and nMatchedTaus != 2:
        continue

    eventNumber = selection_tree.eventNumber
    
    #loop over taus
    for tau in range ( nMatchedTaus ):
        
        tauE = selection_tree.tauCaloOnlyE[tau]
        tauPhi = selection_tree.tauCaloOnlyPhi[tau]
        tauEta = selection_tree.tauCaloOnlyEta[tau]
        tauPt = selection_tree.tauCaloOnlyPt[tau]
        # the next two variables are not used.
        #tauDecayMode = selection_tree.tauDecayMode[tau]
        #tauDecayModeNN = selection_tree.tauDecayModeNN[tau]
        tauTruthDecayMode = selection_tree.tauTruthDecayMode[tau]


        if abs ( tauEta ) > 2.3 or abs ( tauEta ) < 1.7 :
            # The endcaps of the EM calorimeter cover the pseudorapidity range 1.375 < |eta| < 3.2 (2008 JINST 3 S08003, Table 1.3).
            # Of this, the range > 2.5 is only covered in a resolution of 0.1 x 0.1 in η and φ, so it should be skipped.
            # Also, the third calorimeter layer starts at η > 1.5, so we only cover the range 1.5 < |η| < 2.5.
            # The pictures cover a range of ( centreEta - 0.2 , centreEta + 0.2 ), so 1.7 < |eta| < 2.3 should be possible.
            continue

        # initializing histograms
        hist_EME1_Resolution = ROOT.TH2D('hist_EME1_Resolution_{}_tau{}'.format(eventNumber, tau), 'hist_EME1_Resolution_{}_tau{}'.format(eventNumber, tau), 1, -0.2, 0.2, 384, -0.2, 0.2)
        hist_EME1 = ROOT.TH2D('hist_EME1_{}_tau{}'.format(eventNumber, tau), 'hist_EME1_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 384, -0.2, 0.2)
        hist_EME2 = ROOT.TH2D('hist_EME2_{}_tau{}'.format(eventNumber, tau), 'hist_EME2_{}_tau{}'.format(eventNumber, tau), 16, -0.2, 0.2, 16, -0.2, 0.2)
        hist_EME3 = ROOT.TH2D('hist_EME3_{}_tau{}'.format(eventNumber, tau), 'hist_EME3_{}_tau{}'.format(eventNumber, tau), 16, -0.2, 0.2, 8, -0.2, 0.2)
        hist_HEC1 = ROOT.TH2D('hist_HEC1_{}_tau{}'.format(eventNumber, tau), 'hist_HEC1_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 4, -0.2, 0.2)
        hist_HEC2 = ROOT.TH2D('hist_HEC2_{}_tau{}'.format(eventNumber, tau), 'hist_HEC2_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 4, -0.2, 0.2)
        hist_HEC3 = ROOT.TH2D('hist_HEC3_{}_tau{}'.format(eventNumber, tau), 'hist_HEC3_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 4, -0.2, 0.2)
        hist_HEC4 = ROOT.TH2D('hist_HEC4_{}_tau{}'.format(eventNumber, tau), 'hist_HEC4_{}_tau{}'.format(eventNumber, tau), 4, -0.2, 0.2, 4, -0.2, 0.2)

        nCluster = sum(nClusterPerTau)
        if nCluster == 0:
            continue
        
        # The following piece of code defines the center of the tau jet by taking the cell with the maximal energy
        # (called maxIndex) and then setting centrePhi and centreEta to the values of that cell.
        centreOnCluster = True
        if centreOnCluster:
            maxIndexCluster = -1
            maxIndexCell = -1
            maxE = -1
            for i, cluster_E in enumerate(selection_tree.cluster_E):

                if selection_tree.cluster_TauAssoc[i] != tau:
                    continue
                nCells = len(selection_tree.cluster_cell_E[i])

                for cell in range(nCells):

                    cell_E = selection_tree.cluster_cell_E[i][cell]

                    if selection_tree.cluster_cell_Sampling[i][cell] != 6:
                        # The centre of the cell needs to be found in EME2, i.e. Sampling = 6
                        continue
                    if cell_E > maxE:
                        maxE = cell_E
                        maxIndexCluster = i
                        maxIndexCell = cell  

            if maxIndexCell == -1 or maxIndexCluster == -1:
                centrePhi = 0
                centreEta = 0
            else:
                centrePhi = selection_tree.cluster_cell_Phi[maxIndexCluster][maxIndexCell]
                centreEta = selection_tree.cluster_cell_Eta[maxIndexCluster][maxIndexCell]
        else:
            centrePhi = tauPhi
            centreEta = tauEta

        # The following code takes care of specifying the resolution of our EME1-picture.
        # This cannot be done in the same loop as the cell energies because every cell which no cluster points to is skipped there,
        # but the resolution is needed everywhere.
        # The values of the "points where the resolution changes" are given in https://arxiv.org/pdf/1703.10485.pdf,
        # in contrast to the changes of resolution I found by looking at the input root file.
        # Arxiv: 0.4/384*3  for 1.5 < |η| < 1.8          My own approximation: 1.5125 < |η| < 1.8125      -> 96 cells in total
        # Arxiv: 0.4/384*4  for 1.8 < |η| < 2.0          My own approximation: 1.8125 < |η| < 2.0125      -> 48 cells in total
        # Arxiv: 0.4/384*6  for 2.0 < |η| < 2.4          My own approximation: 2.0125 < |η| < 2.4125      -> 64 cells in total
        # Arxiv: 0.4/384*24 for 2.4 < |η| < 2.5          My own approximation: 2.4125 < |η| < 2.5125      -> 4 cells in total
        for etaCoordinate in range ( 384 ) :
            # Eta-Position of the currently considered pixel.
            pixel_Eta = - 0.2 * 383 / 384 + etaCoordinate * 0.4 / 384
            pixelsPerCell = 0
            etaFirstCell = 0
            # First, the resolution (i.e. the number of pixels per cell) is saved in pixelsPerCell,
            # then, the eta-position of the first pixel of the cell is saved in etaFirstCell.
            # For the latter, the eta-Position relative to the start of the resolution range 
            # (e.g. 1.5125 in the case of some cell in the intervall [1.5125 , 1.8125]) is taken,
            # multiplied by 384/0.4/pixelsPerCell and rounded down to get the number of the cell in the interval.
            if abs ( centreEta + pixel_Eta ) < 1.8125 : #somewhere, I should take care of the case pixel_Eta < 1.5125. Otherwise, in principle, this is already done by the histogram.
                pixelsPerCell = 3 
                if ( centreEta + pixel_Eta > 0 ) :
                    leftResolutionBoundary = 1.5125
                else :
                    leftResolutionBoundary = -1.8125
            elif abs ( centreEta + pixel_Eta ) < 2.0125 :
                pixelsPerCell = 4
                if ( centreEta + pixel_Eta > 0 ) :
                    leftResolutionBoundary = 1.8125
                else :
                    leftResolutionBoundary = -2.0125
            elif abs ( centreEta + pixel_Eta ) < 2.4125 :
                pixelsPerCell = 6
                if ( centreEta + pixel_Eta > 0 ) :
                    leftResolutionBoundary = 2.0125
                else :
                    leftResolutionBoundary = -2.4125
            else :
                pixelsPerCell = 24
                if ( centreEta + pixel_Eta > 0 ) :
                    leftResolutionBoundary = 2.4125
                else :
                    leftResolutionBoundary = -2.5125
            hist_EME1_Resolution.Fill ( 0.0 , pixel_Eta , pixelsPerCell )
            
            # ONLY FOR DEBUGGING OF EME1-RESOLUTION:
            if ( etaCoordinate == 0 and debuggingOutput == True) :
                # The following is the number of the first pixel in our considered eta-interval [ centreEta - 0.2 , centreEta + 0.2 ].
                firstPixelNumber.append ( int ( ROOT.TMath.Floor ( ( centreEta + pixel_Eta - leftResolutionBoundary ) * 384.0 / 0.4 ) ) ) #This is the pixelNumber
                # We want the number which we need to add to firstPixelNumber[tauIndex] in order to make it divisible by pixelsPerCell to
                # get the number of pixels that need to be skipped before reaching the first full cell.
                firstFullCell_PixelNumber.append ( ( pixelsPerCell - firstPixelNumber[tauIndex] ) % pixelsPerCell )
            # END OF DEBUGGING

        # This piece of code produces the images by adding each cell of each cluster to the histograms.
        for cluster in range(nCluster):

            cluster_E = selection_tree.cluster_E[cluster]
            cluster_Phi = selection_tree.cluster_Phi[cluster]
            cluster_Eta = selection_tree.cluster_Eta[cluster]
            cluster_Assoc = selection_tree.cluster_TauAssoc[cluster]
            nCells = selection_tree.cluster_nCells[cluster]

            if cluster_E < 1:
                continue

            if cluster_Assoc != tau:
                continue

            nCells = len(selection_tree.cluster_cell_E[cluster])

            for cell in range(nCells):

                cell_E = selection_tree.cluster_cell_E[cluster][cell] / tauE
                cell_Phi = selection_tree.cluster_cell_Phi[cluster][cell] - centrePhi
                cell_Eta = selection_tree.cluster_cell_Eta[cluster][cell] - centreEta
                cell_Sampling = selection_tree.cluster_cell_Sampling[cluster][cell]

                # Source: https://gitlab.cern.ch/atlas/athena/-/blob/21.2/Calorimeter/CaloGeoHelpers/CaloGeoHelpers/CaloSampling.def
                # cell_Sampling = 5 to 7 is EME
                # cell_Sampling = 5 is EME1 and is treated seperately:
                if cell_Sampling == 5:
                    pixelsPerCell = 0
                    cellLeftEta = 0
                    if abs ( cell_Eta + centreEta ) < 1.8125 :
                        pixelsPerCell = 3
                        if ( cell_Eta + centreEta > 0 ) :
                            leftBoundary = 1.5125
                        else :
                            leftBoundary = -1.8125
                        # This is the number of the cell in the interval [leftBoundary , rightBoundary]
                        cellNumber = int ( ( abs ( centreEta + cell_Eta ) - leftBoundary) * 384.0 / 0.4 / pixelsPerCell ) 
                        # This is the corresponding eta on the left of the cell
                        cellLeftEta = ( leftBoundary - centreEta + pixelsPerCell * 0.4 / 384 * cellNumber )
                    elif abs ( cell_Eta + centreEta ) < 2.0125 :
                        pixelsPerCell = 4
                        if ( cell_Eta + centreEta > 0 ) :
                            leftBoundary = 1.8125
                        else :
                            leftBoundary = -2.0125
                        # This is the number of the cell in the interval [leftBoundary , rightBoundary]
                        cellNumber = int ( ( abs ( centreEta + cell_Eta ) - leftBoundary) * 384.0 / 0.4 / pixelsPerCell ) 
                        # This is the corresponding eta on the left of the cell
                        cellLeftEta = ( leftBoundary - centreEta + pixelsPerCell * 0.4 / 384 * cellNumber )
                    elif abs ( cell_Eta + centreEta ) < 2.4125 :
                        pixelsPerCell = 6
                        if ( cell_Eta + centreEta > 0 ) :
                            leftBoundary = 2.0125
                        else :
                            leftBoundary = -2.4125
                        # This is the number of the cell in the interval [leftBoundary , rightBoundary]
                        cellNumber = int ( ( abs ( centreEta + cell_Eta ) - leftBoundary) * 384.0 / 0.4 / pixelsPerCell ) 
                        # This is the corresponding eta on the left of the cell
                        cellLeftEta = ( leftBoundary - centreEta + pixelsPerCell * 0.4 / 384 * cellNumber )
                    else :
                        pixelsPerCell = 24
                        if ( cell_Eta + centreEta > 0 ) :
                            leftBoundary = 2.4125
                        else :
                            leftBoundary = -2.5125
                        # This is the number of the cell in the interval [leftBoundary , rightBoundary]
                        cellNumber = int ( ( abs ( centreEta + cell_Eta ) - leftBoundary) * 384.0 / 0.4 / pixelsPerCell ) 
                        # This is the corresponding eta on the left of the cell
                        cellLeftEta = ( leftBoundary - centreEta + pixelsPerCell * 0.4 / 384 * cellNumber )
                    # Fill all of the affected cells with 1 / pixelsPerCell of the cell energy.
                    #hist_EME1.Fill ( cell_Phi , cell_Eta , cell_E)
                    #hist_EME1_Resolution.Fill ( cell_Phi , cell_Eta , 0.1 )
                    for pixel in range ( pixelsPerCell ) :
                        hist_EME1.Fill ( cell_Phi , cellLeftEta + 0.4 / 384 * ( pixel + 0.5 ) , cell_E / pixelsPerCell )
                        # Fill the Resolution histogram with the corresponding pixel length.
                        #hist_EME1_Resolution.Fill ( cell_Phi , cellLeftEta + 0.4 / 384 * ( pixel + 0.5 ) , 0.1 )

                elif cell_Sampling == 6:
                    hist_EME2.Fill ( cell_Phi , cell_Eta , cell_E )
                elif cell_Sampling == 7:
                    hist_EME3.Fill ( cell_Phi , cell_Eta , cell_E )               
                # cell_Sampling = 8 to 11 is HEC
                elif cell_Sampling == 8:
                    hist_HEC1.Fill ( cell_Phi , cell_Eta , cell_E )
                elif cell_Sampling == 9:
                    hist_HEC2.Fill ( cell_Phi , cell_Eta , cell_E )
                elif cell_Sampling == 10:
                    hist_HEC3.Fill ( cell_Phi , cell_Eta , cell_E )
                elif cell_Sampling == 11:
                    hist_HEC4.Fill ( cell_Phi , cell_Eta , cell_E )
                    

        # Save data in numpy arrays.
        columns , rows = getBinning("EME1") [0] , 1
        pixels_EME1_Resolution = np.ndarray ( shape = ( getBinning ( "EME1" )[0] , 1 ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = hist_EME1_Resolution.GetBinContent( row + 1, column + 1 ) # Get the bin content.
                pixels_EME1_Resolution [column] [row] = value
        EME1_Resolution_out.append ( pixels_EME1_Resolution )

        columns , rows = getBinning("EME1")
        pixels_EME1 = np.ndarray ( shape = ( getBinning ( "EME1" )[0] , getBinning ( "EME1" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_EME1.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_EME1 [column] [row] = value
        EME1_out.append ( pixels_EME1 )

        columns , rows = getBinning("EME2")
        pixels_EME2 = np.ndarray ( shape = ( getBinning ( "EME2" )[0] , getBinning ( "EME2" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_EME2.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_EME2 [column] [row] = value
        EME2_out.append ( pixels_EME2 )

        columns , rows = getBinning("EME3")
        pixels_EME3 = np.ndarray ( shape = ( getBinning ( "EME3" )[0] , getBinning ( "EME3" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_EME3.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_EME3 [column] [row] = value
        EME3_out.append ( pixels_EME3 )

        columns , rows = getBinning("HEC1")
        pixels_HEC1 = np.ndarray ( shape = ( getBinning ( "HEC1" )[0] , getBinning ( "HEC1" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_HEC1.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_HEC1 [column] [row] = value
        HEC1_out.append ( pixels_HEC1 )

        columns , rows = getBinning("HEC2")
        pixels_HEC2 = np.ndarray ( shape = ( getBinning ( "HEC2" )[0] , getBinning ( "HEC2" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_HEC2.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_HEC2 [column] [row] = value
        HEC2_out.append ( pixels_HEC2 )

        columns , rows = getBinning("HEC3")
        pixels_HEC3 = np.ndarray ( shape = ( getBinning ( "HEC3" )[0] , getBinning ( "HEC3" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_HEC3.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_HEC3 [column] [row] = value
        HEC3_out.append ( pixels_HEC3 )

        columns , rows = getBinning("HEC4")
        pixels_HEC4 = np.ndarray ( shape = ( getBinning ( "HEC4" )[0] , getBinning ( "HEC4" )[1] ), dtype = np.uint8 )
        for column in range ( columns ) :
            for row in range ( rows ) :
                value = ROOT.TMath.Log10(hist_HEC4.GetBinContent( row + 1, column + 1 )) # Get the logarithm of the bin content.
                value = ROOT.TMath.Min(ROOT.TMath.Max(value, -4), 0)              # Restrict it to [-4,0] corresponding to (e^-4,1).
                value = np.uint8 ( (-value) * 255/4 )                     # Rescale to [0,255] and make it into an integer
                pixels_HEC4 [column] [row] = value
        HEC4_out.append ( pixels_HEC4 )

        # Save additional data
        additional_out.append ( [ centreEta , centrePhi , tauPt , tauE ] )

        # Save labels.
        labels_out.append ( tauTruthDecayMode ) 

        # ONLY FOR DEBUGGING OF EME1-RESOLUTION:
        if debuggingOutput == True :
            pixel = firstFullCell_PixelNumber[tauIndex]
            while ( pixel < getBinning ( "EME1" )[0] ) :
                resolution = int ( EME1_Resolution_out [ tauIndex ][ pixel , 0 ] )
                
                for i in range ( resolution ) :
                    if ( pixel + i ) >= 384 :
                        continue
                    if ( resolution != int ( EME1_Resolution_out [ tauIndex ][ pixel + i , 0 ] ) ) :
                        print ( f"EME1_Resolution_out [ {tauIndex} ][ {pixel} , 0 ] = {EME1_Resolution_out [ tauIndex ][ pixel , 0 ]} != {EME1_Resolution_out [ tauIndex ][ pixel + i , 0 ]} = EME1_Resolution_out [ {tauIndex} ][ {pixel + i} , 0 ] , resolution = {resolution} " )
                        debuggingOutput = True
                    if ( int ( EME1_out [ tauIndex ][ pixel , 0 ] ) != EME1_out [ tauIndex ][ pixel + i , 0 ] ) :
                        print ( f"EME1_out [ {tauIndex} ][ {pixel} , 0 ] = {EME1_out [ tauIndex ][ pixel , 0 ]} != {EME1_out [ tauIndex ][ pixel + i , 0 ]} = EME1_out [ {tauIndex} ][ {pixel + i} , 0 ] , resolution = {resolution} " )
                        debuggingOutput = True
                pixel = pixel + resolution
            print ( f"\n ( centreEta + pixel_Eta - leftResolutionBoundary ) = {( centreEta + pixel_Eta - leftResolutionBoundary )} " )
            print ( f"\n ( centreEta + pixel_Eta - leftResolutionBoundary ) * 384.0 / 0.4 = { ( centreEta + pixel_Eta - leftResolutionBoundary ) * 384.0 / 0.4 }" )
            print ( f"\nint ( ( centreEta + pixel_Eta - firstPixelNumber[{tauIndex}] ) * 384.0 / 0.4 ) = {firstPixelNumber[tauIndex]}" )
            print ( f"firstFullCell_PixelNumber[{tauIndex}] = {firstFullCell_PixelNumber[tauIndex]}" )
            tauIndex += 1
        # END OF DEBUGGING


    if index > 10000000: #100000:
        break

EME1_Resolution_out = np.array ( EME1_Resolution_out , dtype = np.uint8 )
EME1_out = np.array ( EME1_out , dtype = np.uint8 )
EME2_out = np.array ( EME2_out , dtype = np.uint8 )
EME3_out = np.array ( EME3_out , dtype = np.uint8 )
HEC1_out = np.array ( HEC1_out , dtype = np.uint8 )
HEC2_out = np.array ( HEC2_out , dtype = np.uint8 )
HEC3_out = np.array ( HEC3_out , dtype = np.uint8 )
HEC4_out = np.array ( HEC4_out , dtype = np.uint8 )
additional_out = np.array ( additional_out , dtype = np.uint8 )
labels_out = np.array ( labels_out , dtype = np.uint8 )

print ( EME1_Resolution_out.shape )
print ( EME1_out.shape )
print ( EME2_out.shape )
print ( EME3_out.shape )
print ( HEC1_out.shape )
print ( HEC2_out.shape )
print ( HEC3_out.shape )
print ( HEC4_out.shape )
print ( additional_out.shape )
print ( labels_out.shape )

try:
    np.save ( basedir + "/EME1_Resolution.npy" , EME1_Resolution_out )
    np.save ( basedir + "/EME1.npy" , EME1_out )
    np.save ( basedir + "/EME2.npy" , EME2_out )
    np.save ( basedir + "/EME3.npy" , EME3_out )
    np.save ( basedir + "/HEC1.npy" , HEC1_out )
    np.save ( basedir + "/HEC2.npy" , HEC2_out )
    np.save ( basedir + "/HEC3.npy" , HEC3_out )
    np.save ( basedir + "/HEC4.npy" , HEC4_out )
    np.save ( basedir + "/labels.npy" , labels_out )
    np.save ( basedir + "/additional.npy" , additional_out )
except OSError:
    print("Could not save numpy arrays in the output folder specified in the variable 'basedir'.")
