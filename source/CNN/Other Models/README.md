These files are code corresponding to parameter scans for a dense model, a model with conv3D layers instead of conv2D layers and a custom convolutional model that concatenates the calorimeter layers images after convoluting them to the same size.

They are remnants of the status during work so they are not as well documented. All of these studies have been done on the barrel only.