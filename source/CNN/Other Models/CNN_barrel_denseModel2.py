# importing libraries 
import tensorflow as tf
NUM_THREADS=1
tf.config.threading.set_inter_op_parallelism_threads(NUM_THREADS)
tf.config.threading.set_intra_op_parallelism_threads(NUM_THREADS)
from tensorflow.keras.layers import Conv2D, Conv3D, MaxPooling2D, MaxPooling3D, Flatten, Dense 
import tensorflow.keras as K 
import numpy as np
import pickle
import sys
import os
import multiprocessing as mp
import sklearn.model_selection as scikit
import sklearn
import math
import tensorflow_addons as tfa # for timeStopping callback (in order to stop the algorithm after running a certain number of hours)

#TODO Add Tracking Information to the input
#TODO Add automatic variation of the parameters defining a model.
#TODO For the endcaps, let the network reweight the points with the same eta all with the same constant.

def getBinning ( sampling ):
    # EMB1
    if sampling == "EMB1" or sampling == 0:
        return 128, 4
    # EMB2
    if sampling == "EMB2" or sampling == 1:
        return 16, 16
    # EMB3
    if sampling == "EMB3" or sampling == 2:
        return 8, 16
    #Tile0
    if sampling == "Tile1" or sampling == 3:
        return 4, 4
    #Tile1
    if sampling == "Tile2" or sampling == 4:
        return 4, 4
    #Tile2
    if sampling == "Tile3" or sampling == 5:
        return 2, 4 #2,4 but it gets rebinned automatically here...
    # additional
    if sampling == "additional" or sampling == 6:
        return 1 , 4

    if sampling == "max":
        return 128, 16

def flatModel ( tracking , deepLearning , denseLayerSize , l1_regularization , l2_regularization ):
    # Define the form of our inputs.
    input_EMB1 = K.layers.Input ( shape = ( getBinning("EMB1")[0], getBinning("EMB1")[1], 1 ) , name = "emb1" )
    input_EMB2 = K.layers.Input ( shape = ( getBinning("EMB2")[0], getBinning("EMB2")[1], 1 ) , name = "emb2" )
    input_EMB3 = K.layers.Input ( shape = ( getBinning("EMB3")[0], getBinning("EMB3")[1], 1 ) , name = "emb3" )
    input_Tile1 = K.layers.Input ( shape = ( getBinning("Tile1")[0], getBinning("Tile1")[1], 1 ) , name = "tile1" )
    input_Tile2 = K.layers.Input ( shape = ( getBinning("Tile2")[0], getBinning("Tile2")[1], 1 ) , name = "tile2" )
    input_Tile3 = K.layers.Input ( shape = ( getBinning("Tile3")[0], getBinning("Tile3")[1], 1 ) , name = "tile3" )
    input_additional = K.layers.Input ( shape = ( getBinning("additional")[1] ) , name = "additional" )
    # Extra layer simulating the existence of tracking information by adding the number of charged pions.
    if ( tracking == True ) : 
        input_tracking = K.layers.Input ( shape = ( 1 ) , name = "tracking" )

    # Flatten the inputs.
    flat1 = Flatten ( ) ( input_EMB1 )
    flat2 = Flatten ( ) ( input_EMB2 )
    flat3 = Flatten ( ) ( input_EMB3 )
    flat4 = Flatten ( ) ( input_Tile1 )
    flat5 = Flatten ( ) ( input_Tile2 )
    flat6 = Flatten ( ) ( input_Tile3 )

    # Combine the flattened inputs.
    combined = K.layers.Concatenate ( axis = 1 ) ( [ flat1 , flat2 , flat3 , flat4 , flat5 , flat6 , input_additional ] )
    if ( tracking == True ) :
        combined = K.layers.Concatenate ( axis = 1 ) ( [ combined , input_tracking ] )
    for i in range ( deepLearning ) :
        combined = Dense ( denseLayerSize , activation = 'relu' , kernel_regularizer = K.regularizers.L1L2 ( l1 = l1_regularization , l2 = l2_regularization ) ) ( combined )
    combined = Dense ( 6 , activation = 'softmax' , name = "Decay_Mode" ) ( combined )

    # Define the Model.
    if ( tracking == True ) :
        model = K.Model ( inputs = [ input_EMB1 , input_EMB2 , input_EMB3 , input_Tile1 , input_Tile2 , input_Tile3 , input_additional , input_tracking ] , outputs = combined )
        return model
    model = K.Model ( inputs = [ input_EMB1 , input_EMB2 , input_EMB3 , input_Tile1 , input_Tile2 , input_Tile3 , input_additional ] , outputs = combined )
    return model

def getData ( train_data_dir , seed , test_ratio , tracking):

    # This method returns two tuples of datasets - train_data and test_data - and the corresponding labels.
    # test_ratio = #test_data / #all_data
    # seed is a randomizer

    print ( "Getting dataset..." )
    EMB1 = np.load ( train_data_dir + "EMB1" + ".npy" )# / 255.
    EMB2 = np.load ( train_data_dir + "EMB2" + ".npy" )# / 255.
    EMB3 = np.load ( train_data_dir + "EMB3" + ".npy" )# / 255.
    Tile1 = np.load ( train_data_dir + "Tile1" + ".npy" )# / 255.
    Tile2 = np.load ( train_data_dir + "Tile2" + ".npy" )# / 255.
    Tile3 = np.load ( train_data_dir + "Tile3" + ".npy" )# / 255.
    labels = np.load ( train_data_dir + "labels" + ".npy" )
    additional = np.load ( train_data_dir + "additional" + ".npy" )
    if not ( EMB1.shape[0] == EMB2.shape[0] == EMB3.shape[0] == Tile1.shape[0] == Tile2.shape[0] == Tile3.shape[0] == labels.shape[0] == additional.shape[0] ) :
        print ( "FATAL ERROR: not all data arrays have the same length:" )
        print ( "EMB1.shape = " + str ( EMB1.shape ) )
        print ( "EMB2.shape = " + str ( EMB2.shape ) )
        print ( "EMB3.shape = " + str ( EMB3.shape ) )
        print ( "Tile1.shape = " + str ( Tile1.shape ) )
        print ( "Tile2.shape = " + str ( Tile2.shape ) )
        print ( "Tile3.shape = " + str ( Tile3.shape ) )
        print ( "labels.shape = " + str ( labels.shape ) )
        print ( "additional.shape = " + str ( additional.shape ) )
        sys.exit( "FATAL ERROR: not all data arrays have the same length." )

    # Add tracking info
    if ( tracking == True ) :
        tracking_data = labels // 3

    # Prepare the labels for categorical crossentropy
    labels = K.utils.to_categorical(labels)

    # Add tracking info
    if ( tracking == True ) :
        train_EMB1 , test_EMB1 , train_EMB2 , test_EMB2 , train_EMB3 , test_EMB3 , train_Tile1 , test_Tile1 , train_Tile2 , test_Tile2 , train_Tile3 , test_Tile3 , train_additional , test_additional , train_tracking , test_tracking , train_labels , test_labels = scikit.train_test_split ( EMB1 , EMB2 , EMB3 , Tile1 , Tile2 , Tile3 , additional , tracking_data , labels , test_size = test_ratio , random_state = seed , shuffle = True )
        return [ train_EMB1 , train_EMB2 , train_EMB3 , train_Tile1 , train_Tile2 , train_Tile3 , train_additional , train_tracking ] , train_labels , [ test_EMB1 , test_EMB2 , test_EMB3 , test_Tile1 , test_Tile2 , test_Tile3 , test_additional , test_tracking ], test_labels
        
    train_EMB1 , test_EMB1 , train_EMB2 , test_EMB2 , train_EMB3 , test_EMB3 , train_Tile1 , test_Tile1 , train_Tile2 , test_Tile2 , train_Tile3 , test_Tile3 , train_additional , test_additional , train_labels , test_labels = scikit.train_test_split ( EMB1 , EMB2 , EMB3 , Tile1 , Tile2 , Tile3 , additional , labels , test_size = test_ratio , random_state = seed , shuffle = True )

    return [ train_EMB1 , train_EMB2 , train_EMB3 , train_Tile1 , train_Tile2 , train_Tile3 , train_additional ] , train_labels , [ test_EMB1 , test_EMB2 , test_EMB3 , test_Tile1 , test_Tile2 , test_Tile3 , test_additional ], test_labels

def evaluateModel ( queue , 
                    train_data , train_labels , validation_data , validation_labels , test_data , test_labels , 
                    steps_per_epoch , validation_steps , batch_size , epochs , patience ,
                    fold_no , job_ID , maxRuntimeHours ,
                    tracking , class_weight , deepLearning , denseLayerSize ,
                    l1_regularization , l2_regularization ):
    tf.random.set_seed ( fold_no )
    # Create new and untouched model
    model , modelName = flatModel ( tracking , deepLearning , denseLayerSize , l1_regularization , l2_regularization ) , "flatModel"

    if ( fold_no == 0 ):
        print ( "The " + modelName + " looks as follows:" )
        model.summary ( )
        trainableParams = np.sum([np.prod(v.get_shape()) for v in model.trainable_weights])
        queue.put ( trainableParams )

    # Define regularization losses
    #reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)

    # Compile model using the regularization losses
    model.compile ( loss = K.losses.CategoricalCrossentropy() ,#+ reg_losses,
                #loss = 'categorical_crossentropy', loss = K.losses.SparseCategoricalCrossentropy(), 'sparse_categorical_crossentropy' 'binary_crossentropy', 
                optimizer = 'Adam',
                metrics = [ 'accuracy' ] ,
                weighted_metrics = [ 'accuracy' ] ) 
    
    # Define a Callback for early stopping that minimizes the validation loss.
    EarlyStopping = K.callbacks.EarlyStopping ( monitor = 'val_loss' ,     # We want the validation loss ...
                                     mode = 'min' ,             # ... to be minimized.
                                     #min_delta = 0.002 ,        # Improvement means that the value decreases at least by 'min_delta'.
                                     patience = patience ,            # The algorithm stops after 'patience' steps of no improvement.
                                     verbose = 1                # We want outputs stating when the model stops.
                                   )

    # Define a Checkpoint that saves the best model with regards of the validation set.
    modelName = "/cephfs/user/pampel/Projekte/04_ML4Pions/parameter_scan/" + modelName + f"_job{job_ID}"
    try:
        os.mkdir( modelName )
    except OSError:
        print ( "Skipping creation of " + modelName + " because it already exists." )

    ModelCheckpoint = K.callbacks.ModelCheckpoint ( modelName + f'/best_model_fold_no_{fold_no}.h5' ,        # Name of the file in which to save the checkpoint.
                                       monitor='val_loss' ,     # We want the validation loss ...
                                       mode = 'min' ,           # ... to be minimized.
                                       save_best_only = True ,  # Only the best model shall be saved.
                                       verbose = 0             # We don't want outputs.
                                     )

    # Define a Callback that stops the algorithm if the allocated time limit is reached (minus two hours of safety distance).
    TimeStopping = tfa.callbacks.TimeStopping ( seconds = 3600 * ( maxRuntimeHours - 4 ) , 
                                                verbose = 1 )
    training_weights = np.array ( [ class_weight[np.argmax ( label )] for label in train_labels ] )
    validation_weights = np.array ( [ class_weight[np.argmax ( label )] for label in validation_labels ] )
    test_weights = np.array ( [ class_weight[np.argmax ( label )] for label in test_labels ] )
    print ( "Finished creating weigths" )

    # Evaluate the model on our current train and validation data.
    history = model.fit ( train_data , train_labels , sample_weight = training_weights ,
        batch_size = batch_size , 
        steps_per_epoch = steps_per_epoch , 
        epochs = epochs , 
        validation_data = ( validation_data , validation_labels , validation_weights ) ,
        validation_steps = validation_steps ,
        verbose = 0 , #2 for outputs per epoch.
        callbacks = [ EarlyStopping , ModelCheckpoint , TimeStopping ] )
    print ( "Finished training" )
    print ( history.history.keys () )
    print ( 'training loss history' )
    print ( history.history ['loss'] )
    print ( 'validation loss history' )
    print ( history.history ['val_loss'] )
    print ( 'training accuracy history' )
    print ( history.history ['accuracy'] )
    print ( 'validation accuracy history' )
    print ( history.history ['val_accuracy'] , flush = True )
    model = K.models.load_model ( modelName + f'/best_model_fold_no_{fold_no}.h5' )

    # Print confusion matrix
    predicted_test_labels = model.predict( test_data , verbose = 2 )
    y_pred = np.argmax ( predicted_test_labels , axis = 1 )
    y_test = np.argmax ( test_labels , axis = 1 )
    disp = sklearn.metrics.confusion_matrix ( y_test , y_pred )
    print ( disp )
    
    # Print and save the scores for our model evaluation.
    scores = model.evaluate ( test_data , test_labels , sample_weight = test_weights , verbose=0 )
    print ( f'Score for fold {fold_no}: {model.metrics_names[0]} of {scores[0]}; {model.metrics_names[1]} of {scores[1]*100}%; {model.metrics_names[2]} of {scores[2]*100}%' )
    queue.put ( scores )
    queue.put ( disp )
    #queue.put ( model ) queue.put ( history )
    queue.close () # Tells the parent process that this process will not put any more data onto the queue.
    return

def main ( ) :
    try:
        job_ID = int ( sys.argv [1] )
    except IndexError:
        print ( "Missing first argument, the Job ID" )
        sys.exit ( -1 )
    except ValueError:
        print ( "Job ID must be an integer" )
        sys.exit ( -1 )
    print ( f'Job ID: {job_ID}' )

    try:
        clusterID = int ( sys.argv [2] )
    except IndexError:
        print ( "Missing second argument, clusterID" )
        sys.exit ( -1 )
    except ValueError:
        print ( "clusterID must be an integer" )
        sys.exit ( -1 )
    print ( f'clusterID: {clusterID}' )

    try:
        maxRuntimeHours = int ( sys.argv [3] )
    except IndexError:
        print ( "Missing third argument, maxRuntimeHours" )
        sys.exit ( -1 )
    except ValueError:
        print ( "maxRuntimeHours must be an integer" )
        sys.exit ( -1 )
    print ( f'maxRuntimeHours: {maxRuntimeHours}' )

    try:
        os.mkdir( "/cephfs/user/pampel/Projekte/04_ML4Pions/parameter_scan" )
    except OSError:
        print ( "Skipping creation of parameter_scan because it already exists." )


    train_data_dir = '/cephfs/user/pampel/Projekte/04_ML4Pions/testImages_barrel/'
    nb_train_samples = 8000 #8000   # The number of steps per epoch is nb_train_samples // batch_size
    nb_validation_samples = 10000   # Only important if no crossvalidation is done, i.e. num_folds is set to 1.
    num_folds = 5 #10               # k in k-fold crossvalidation
    nb_test_samples = 100000        # Number of test samples. The total sample number is roughly 580k.
                                    # If this is a float <1 then this is the fraction that the test samples are of all samples.
    epochs = 1000 #1000             # Number of epochs to be run.
    patience = 50                   # Number of epochs with no improvement of validation score before early stopping.

    # TODO Paramaters over which I could scan:
    tracking = False                # True if the number of charged pions shall be added in order to 'simulate the existence of tracking info'.
    reweighting = False             # True if the classes shall be reweighted in order to make them 'equally big' from the perspective of the network.
    batch_size = 32 #32
    deepLearning = 0                # Can be 0,1,2 and determines the number of extra dense layers.
    denseLayerSize = 128            # Determines the number of nodes in each dense layer.
    layerThicknessFactor = 0.5      # In each convolution layer, the third dimension (starting at 6 due to the 6 calorimeter images) 
                                    # is multiplied by xPooling * yPooling * layerThicknessFactor
    strideSizeFactor = 1            # The size of striding in x direction is int ( strideSizeFactor * poolSizeX + 1 ).
    finalXSize = 4                  # Length of the x-axis in pixels of the images after convoluting
    finalYSize = 4                  # Length of the y-axis in pixels of the images after convoluting
                                    # These determine the pool size xPooling and yPooling.
    convolutionNumber = 5         # Number of concatination layers

    # TODO Make sure that somehow the pooling does not result in negative sizes of the picture size. 
    # Maybe by specifying the final x and y size and the number of concatenations, the pool size can be calculated on a logarithmic scale.
    # Not sure how though...

    # Parameter-Scan: Depending on the job number, which is the input of this python script,
    # the parameters are chosen.
    parameterList = []
    index = 0

    import random
    random.seed ( 1000 * clusterID + job_ID )
    job_ID_random = random.randint ( 0 , 588 )
    #job_ID_random = job_ID

    job_ID_set = False
    for temp_tracking in [ False ] : #[ False, True ] :
        for temp_reweighting in [ False ] : #[ False, True ] :
            for temp_batch_size in [ 512 , 1024 ] : #[ 8 , 32 ] : #[ 1 , 2 , 4 , 8 , 16 , 32 , 64 , 128 ] :
                for temp_deepLearning in [ 8 , 12 ] : #[ 2 , 4 ] : #[ 0 , 1 , 2 ] : # deepLearning = 0 means no dense layer
                    for temp_denseLayerSize in [ 128 , 256 , 512 ] : #[ 64 , 128 ] : #[ 16 , 32 , 64 , 128 , 256 , 512 , 1024 ] :
                        for temp_l1_regularization in [ 0 , 0.000005 , 0.00001 , 0.00002 , 0.00004 , 0.00008 , 0.00016 ] :
                            for temp_l2_regularization in [ 0 , 0.00005 , 0.0001 , 0.0002 , 0.0004 , 0.0008 , 0.0016 ] :
                                if index == job_ID_random :
                                    tracking = temp_tracking
                                    reweighting = temp_reweighting
                                    batch_size = temp_batch_size
                                    deepLearning = temp_deepLearning
                                    denseLayerSize = temp_denseLayerSize
                                    l1_regularization = temp_l1_regularization
                                    l2_regularization = temp_l2_regularization
                                    job_ID_set = True
                                index = index + 1
    print ( f"number of parameter possibilities: {index}" )
    if job_ID_set == False :
        print ( f"Job_ID_random = {job_ID_random} out of bounds of parameter list. Aborting")
        sys.exit ( -1 )



    # The weights with which the classes are reweighted. If these are all 1, then nothing is changed. 
    # With the reweighting values, the classes are roughly the same size (from the perspective of the neural network)
    if reweighting :
        class_weight = { 0 : 2 ,    # 7903 samples in this category
                         1 : 1 ,    # 16771 samples in this category
                         2 : 2 ,    # 6663 samples in this category
                         3 : 2 ,    # 6360 samples in this category
                         4 : 4 ,    # 3376 samples in this category
                         5 : 1 }    # 56  samples in this category, ignore this for now...
    else : 
        class_weight = { 0 : 1 , 
                         1 : 1 , 
                         2 : 1 , 
                         3 : 1 , 
                         4 : 1 , 
                         5 : 1 }

    # Get Data
    train_data , train_labels , test_data , test_labels = getData ( train_data_dir = train_data_dir , 
                                                                    test_ratio = nb_test_samples ,
                                                                    seed = 42 ,
                                                                    tracking = tracking )
    
    # Define per-fold score containers and utilities for multiprocessing
    acc_per_fold = []
    weighted_acc_per_fold = []
    loss_per_fold = []
    fold_numbers = []
    processes = [] # The processes that are about to evaluate the model on data.
    queues = []    # The queues in which the returned values are saved.
    confusion_matrices = []

    # Define the K-fold Cross Validator
    # If tracking info is added (i.e. tracking == True), these lines add the corresponding additional data input layer for the neural networks.
    AdditionalTrackingIndex = 0 
    if ( tracking == True ) :
        AdditionalTrackingIndex = 1
    
    # Now, the jobs need to be run in the function "evaluateModel", first in the case that no crossvalidation is done...
    if ( num_folds == 1 ) : 
        validation_data = [ i for i in range ( train_data.len() ) ] # ( 7 + AdditionalTrackingIndex ) ] # There are 7 lists in train_data and validation_data
        # TODO Maybe use validation_data = [ i for i in range ( train_data.len() ) ]
        # TODO Maybe use for i in range ( train_data.len() ) :
        for i in range ( train_data.len() ) : # ( 7 + AdditionalTrackingIndex ) :
            train_data[i] , validation_data[i] = scikit.train_test_split ( train_data[i] , test_size = nb_validation_samples , random_state = 42 , shuffle = True )
        train_labels , validation_labels = scikit.train_test_split ( train_labels , test_size = nb_validation_samples , random_state = 42 , shuffle = True )
        fold_no = 0
        # Make the numpy-arrays into multiprocessing arrays so that their memory is shared across processes.
        for i in range ( train_data.len() ) : # ( 8 + AdditionalTrackingIndex ) :
            train_data[i] = mp.Array ( 'B' , train_data[i] )
        queues.append ( mp.Queue() )
        kwargs = { # Define the arguments for the job
                "queue" :               queues[fold_no] ,
                "train_data" : train_data , "train_labels" : train_labels , "validation_data" : validation_data , "validation_labels" : validation_labels , "test_data" : test_data , "test_labels" : test_labels ,
                "steps_per_epoch" :     nb_train_samples // batch_size ,
                "validation_steps" :    nb_validation_samples // batch_size , 
                "batch_size" : batch_size , "epochs" : epochs , "fold_no" : fold_no , "job_ID" : job_ID , "maxRuntimeHours" : maxRuntimeHours , "patience" : patience ,
                "tracking" : tracking , "class_weight" : class_weight , "deepLearning" : deepLearning , "denseLayerSize" : denseLayerSize , 
                "l1_regularization" : l1_regularization , "l2_regularization" : l2_regularization
               }
        processes.append (
            mp.Process (    target = evaluateModel , 
                            kwargs = kwargs # Hand over the arguments to the job.
            )
        )
        print ( "Starting process number " + str (fold_no) + "." )
        processes[fold_no].start()

    elif ( num_folds < 1 ) :
        print ( "ERROR: num_folds < 1" )
        sys.exit ( -1 )
    
    # ... then in the case of k-fold crossvalidation.
    else :
        kfold = scikit.KFold ( n_splits = num_folds , shuffle = False )

        # K-fold Cross Validation model evaluation
        fold_no = 0
        
        for train_index, validation_index in kfold.split ( train_labels ):
            queues.append ( mp.Queue() )
            kwargs = { # Define the arguments for the job
                "queue" :               queues[fold_no] ,
                "train_data" :          [ channel[train_index] for channel in train_data ] , 
                "train_labels" :        train_labels[train_index] , 
                "validation_data" :     [ channel[validation_index] for channel in train_data ] ,
                "validation_labels" :   train_labels[validation_index] , 
                "test_data" :           test_data , 
                "test_labels" :         test_labels ,
                "steps_per_epoch" :     nb_train_samples // batch_size ,
                "validation_steps" :    nb_validation_samples // batch_size , 
                "batch_size" : batch_size , "epochs" : epochs , "fold_no" : fold_no , "job_ID" : job_ID , "maxRuntimeHours" : maxRuntimeHours , "patience" : patience ,
                "tracking" : tracking , "class_weight" : class_weight , "deepLearning" : deepLearning , "denseLayerSize" : denseLayerSize, 
                "l1_regularization" : l1_regularization , "l2_regularization" : l2_regularization
               }
            processes.append (
                mp.Process (    target = evaluateModel , 
                                kwargs = kwargs # Hand over the arguments to the job.
                )
            )
            print ( "Starting process number " + str (fold_no) + "." )
            processes[fold_no].start()
            fold_no = fold_no + 1
    
    trainableParams = queues[0].get()
    for fold_no in range(num_folds):
        scores_temp = queues[fold_no].get()
        confusion_matrices.append ( queues[fold_no].get() ) 
        loss_per_fold.append ( scores_temp[0] )
        acc_per_fold.append ( scores_temp[1] )
        weighted_acc_per_fold.append ( scores_temp[2] )
        fold_numbers.append ( fold_no )
        processes[fold_no].join()
        print ( "Joined process number " + str (fold_no) + "." )
        processes[fold_no].close()
        print ( "Closed process number " + str (fold_no) + "." )
        

    # == Provide average scores ==
    print('------------------------------------------------------------------------------------------------------------')
    print('Score per fold')
    for i in range(0, len(acc_per_fold)):
        print('------------------------------------------------------------------------------------------------------------')
        print(f'> Fold {i} - Loss: {loss_per_fold[i]} - Accuracy: {acc_per_fold[i]}% - Weighted Accuracy: {weighted_acc_per_fold[i]}%')
    print('------------------------------------------------------------------------------------------------------------')
    print('Average scores for all folds:')
    print(f'> Accuracy: {np.mean(acc_per_fold)} (+- {np.std(acc_per_fold)})')
    print(f'> Weighted Accuracy: {np.mean(weighted_acc_per_fold)} (+- {np.std(weighted_acc_per_fold)})')
    print(f'> Loss: {np.mean(loss_per_fold)}')
    print('------------------------------------------------------------------------------------------------------------')

    # Calculate the average confusion matrix
    # Sum:
    avg_confusion_matrix = confusion_matrices [0]
    for i in range ( 1 , len ( confusion_matrices ) ) :
        avg_confusion_matrix = avg_confusion_matrix + confusion_matrices [i]
    # Average:
    avg_confusion_matrix = avg_confusion_matrix / len ( confusion_matrices )

    # Calculate the standard derivative of the confusion matrices (not the error of the average!)
    # Sum of squares:
    std_confusion_matrix = ( confusion_matrices [0] - avg_confusion_matrix ) * ( confusion_matrices [0] - avg_confusion_matrix )
    for i in range ( 1 , len ( confusion_matrices ) ) :
        std_confusion_matrix = std_confusion_matrix + ( confusion_matrices [i] - avg_confusion_matrix ) * ( confusion_matrices [i] - avg_confusion_matrix )
    # Standard derivative:
    std_confusion_matrix = np.uint32 ( np.sqrt ( std_confusion_matrix / len ( confusion_matrices ) ) + 0.9999 )

    # Print confusion matrix and standard derivative
    avg_confusion_matrix = np.uint32 ( avg_confusion_matrix )
    print ( "The average confusion matrix and the fluctuations of it are:")
    print ( avg_confusion_matrix )
    print ( "+-" )
    print ( std_confusion_matrix )

    print ( f"jobnumber ; job_ID_random ; tracking ; reweighting ; batch_size ; deepLearning ; denseLayerSize ; layerThicknessFactor ; strideSizeFactor ; finalXSize ; finalYSize ; convolutionNumber ; l1_regularization ; l2_regularization ; Accuracy ; +- Std; Weighted Accuracy ; +- Std ; trainableParams" )
    print ( f"{job_ID} ; {job_ID_random} ; {tracking} ; {reweighting} ; {batch_size} ; {deepLearning} ; {denseLayerSize} ; {layerThicknessFactor} ; {strideSizeFactor} ; {finalXSize} ; {finalYSize} ; {convolutionNumber} ; {l1_regularization} ; {l2_regularization} ; {np.mean(acc_per_fold)} ; {np.std(acc_per_fold)} ; {np.mean(weighted_acc_per_fold)} ; {np.std(weighted_acc_per_fold)} ; {trainableParams}" )

    # Print the results to a csv-file.
    with open ( f"/cephfs/user/pampel/Projekte/04_ML4Pions/parameter_scan/output{clusterID}.csv" , "a" ) as myfile : # "a" means that the line is appended
        myfile.write ( f"{job_ID} ; {job_ID_random} ; {tracking} ; {reweighting} ; {batch_size} ; {deepLearning} ; {denseLayerSize} ; {layerThicknessFactor} ; {strideSizeFactor} ; {finalXSize} ; {finalYSize} ; {convolutionNumber} ; {l1_regularization} ; {l2_regularization} ; {np.mean(acc_per_fold)} ; {np.std(acc_per_fold)} ; {np.mean(weighted_acc_per_fold)} ; {np.std(weighted_acc_per_fold)} ; {trainableParams} \n" )

    #history = model.fit ( training_data , training_labels , 
    #    batch_size = batch_size , 
    #    steps_per_epoch = nb_train_samples // batch_size , 
    #    epochs = epochs , 
    #    test_data = ( test_data , test_labels ) )

    # I do not know what the following code does. Probably save the output.
    #with open ( 'history_CNN.pickle' , 'wb' ) as handle:
    #    pickle.dump ( history.history , handle , protocol=pickle.HIGHEST_PROTOCOL )

    #model.save ( 'model_CNN.h5' )

if __name__ == '__main__':
    main ()