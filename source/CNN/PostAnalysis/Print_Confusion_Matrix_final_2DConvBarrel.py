import numpy as np
import matplotlib.pyplot as plt
dpi = 600

ListOfMatrices=[[[11066,4756,225,2736,175,0],
[3644,31929,2879,1790,696,0],
[463,8791,6589,313,390,0],
[3298,3134,157,8128,656,0],
[431,3942,708,1603,1336,0],
[16,34,0,106,9,0],],
[[11180,4712,337,2503,226,0],
[3679,31198,3730,1462,869,0],
[458,8054,7363,239,432,0],
[3492,3090,259,7666,866,0],
[473,3590,965,1443,1549,0],
[17,32,0,100,16,0],],
[[11190,4753,207,2678,130,0],
[3828,32588,2375,1611,536,0],
[488,9415,6029,300,314,0],
[3379,3233,136,8112,513,0],
[481,4161,646,1626,1106,0],
[16,32,0,109,8,0],],
[[10662,4580,372,3046,298,0],
[3339,29681,4601,1970,1347,0],
[411,6981,8111,333,710,0],
[2894,2855,232,8441,951,0],
[380,3119,954,1641,1926,0],
[14,27,1,111,12,0],],
[[10205,5293,308,2934,218,0],
[3043,32415,3102,1609,769,0],
[357,8751,6763,268,407,0],
[2783,3354,223,8144,869,0],
[327,3903,863,1471,1456,0],
[11,31,1,106,16,0]]]

mean = [ [ 0 for j in range (6) ] for i in range (6) ]
std = [ [ 0 for j in range (6) ] for i in range (6) ]
mean_normalized = [ [ 0 for j in range (6) ] for i in range (6) ]
std_normalized = [ [ 0 for j in range (6) ] for i in range (6) ]
for row in range ( 6 ) :
    column_sum = 0.
    for col in range ( 6 ) :
        temp = [ ListOfMatrices[i][row][col] for i in range ( 5 ) ]
        mean [row][col] = int ( np.mean ( temp ) )
        column_sum = column_sum + mean [row][col]
        std [row][col] = int ( np.std ( temp ) + 0.9999 )
    for col in range ( 6 ) : 
        temp = [ ListOfMatrices[i][row][col] for i in range ( 5 ) ]
        mean_normalized [row][col] = np.mean ( temp ) / column_sum
        std_normalized [row][col] = np.std ( temp ) / column_sum
mean = np.array ( mean )
mean_normalized = np.array ( mean_normalized )
std_normalized = np.array ( std_normalized )
std = np.array ( std )
fig, picture = plt.subplots() 
picture.imshow ( mean , interpolation='nearest' , cmap=plt.cm.viridis , vmin=0 , vmax=32000 )

#Write down axis names
axis_names = ['1p0n','1p1n','1pXn','3p0n','3pXn','Other']
picture.set_title ( "Confusion Matrix" )
picture.set_xticks ( [x for x in range(0,6)] )
picture.set_xticklabels ( axis_names )
picture.set_yticks ( [x for x in range(0,6)] )
picture.set_yticklabels ( axis_names )

#Show both labels
picture.xaxis.set_tick_params(labeltop=True)
picture.set_xlabel( "Reconstructed decay mode" )
picture.yaxis.set_tick_params(labeltop=True)
picture.set_ylabel( "True decay mode" )

#Create list containing all the text fragments in order to be able to make them invisible later on
texts = []

# Show text in bins
for i in range( 6 ):
    for j in range( 6 ):
        if mean[i,j] < 15000 :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="w", ha="center", va="center", fontweight="bold") )
        else :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="k", ha="center", va="center", fontweight="bold") )
fig.show()
plt.savefig ("ConfusionMatrix.png" , dpi = dpi )
# Clear text
for txt in texts : 
    txt.set_visible ( False )
    #txt.remove()



picture.set_title ( "Confusion Matrix, normalized within each true category" )
picture.imshow ( mean_normalized , interpolation='nearest' , cmap=plt.cm.viridis , vmin=0 , vmax=1 )
# Show text in bins
for i in range( 6 ):
    for j in range( 6 ):
        if mean_normalized[i,j] < 0.5 :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="w", ha="center", va="center", fontweight="bold") )
        else :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="k", ha="center", va="center", fontweight="bold") )
fig.show()
plt.savefig ("ConfusionMatrix_normalized.png" , dpi = dpi )
# Clear text
for txt in texts : 
    txt.set_visible ( False )
    #txt.remove()

picture.imshow ( mean_normalized , interpolation='nearest' , cmap=plt.cm.viridis , vmin=0 , vmax=1 )
# Show normalized text in bins
for i in range( 6 ):
    for j in range( 6 ):
        if mean_normalized[i,j] < 0.5 :
            texts.append ( picture.text(j,i, '%2.1f%%\n$\pm$%1.1f%%' % ( mean_normalized[i,j] * 100 , std_normalized[i,j] * 100 ) , 
                color="w", ha="center", va="center", fontweight="bold") )
        else :
            texts.append ( picture.text(j,i, '%2.1f%%\n$\pm$%1.1f%%' % ( mean_normalized[i,j] * 100 , std_normalized[i,j] * 100 ) , 
                color="k", ha="center", va="center", fontweight="bold") )
fig.show()
plt.savefig ("ConfusionMatrix_normalized_normalizedText.png" , dpi = dpi )
# Clear text
for txt in texts : 
    txt.set_visible ( False )
    #txt.remove()