The file Print_Confusion_Matrix_final_2DConvBarrel.py produces the confusion matrix given specific hard coded values that can be copied from the console output of CNN_barrel.py since no more involved solution was needed for me. 
It produces three plots:
 - one with absolute values
 - one with values normalized to each truth category (i.e. the sum over each row is 1)
 - one whose text is the absolute value but whose color coding is according to the normalized values

The "_turned" variant has the truth value on the x-axis and the predicted value on the y-axis, which is the ATLAS standard.

The file Print_Acc_Loss_History_final_2DConvBarrel.py produce a plot of the accuracy and loss during training using specific hard coded loss functions, again copied from the output of CNN_barrel.py. Since five fold crossvalidation is done in CNN_barrel.py, this produces five seperate plots for accuracy and loss, and some combinations of these.

The "Endcaps" variant is always the same as the "Barrel" variant, but treating the endcap model.