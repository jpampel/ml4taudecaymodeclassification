import numpy as np
import matplotlib.pyplot as plt
dpi = 600


ListOfMatrices=[[[10177,5199,279,2970,89,0],
[3628,31251,3500,2339,378,0],
[426,9428,5976,512,216,0],
[3459,4185,276,7074,314,0],
[540,4324,993,1772,528,0],
[18,48,2,96,3,0],],
[[9254,5282,256,3746,176,0],
[3212,31028,3629,2601,626,0],
[378,9239,6023,556,362,0],
[2816,4087,238,7684,483,0],
[387,4116,961,1889,804,0],
[13,37,3,105,9,0],],
[[10041,4889,221,3429,134,0],
[3769,30834,3289,2624,580,0],
[484,9325,5767,605,377,0],
[3302,3876,217,7564,349,0],
[483,4044,845,2056,729,0],
[18,38,3,99,9,0],],
[[9517,5545,274,3269,109,0],
[3212,31684,3393,2351,456,0],
[364,9518,5943,438,295,0],
[2952,4316,277,7392,371,0],
[452,4333,978,1755,639,0],
[16,50,2,93,6,0],],
[[10406,4922,332,2844,210,0],
[3786,30086,4410,2165,649,0],
[458,8600,6667,443,390,0],
[3524,3951,308,6916,609,0],
[512,3874,1172,1654,945,0],
[13,38,5,97,14,0]]]

Temp = np.zeros((5,6,6))

print(Temp[0][1][2])

for matrix in range(5) :
    for row in range(6) :
        for column in range(6) :
            Temp[matrix,row,column] = ListOfMatrices[matrix][column][5-row]
ListOfMatrices = Temp.tolist()


mean = [ [ 0 for j in range (6) ] for i in range (6) ]
std = [ [ 0 for j in range (6) ] for i in range (6) ]
mean_normalized = [ [ 0 for j in range (6) ] for i in range (6) ]
std_normalized = [ [ 0 for j in range (6) ] for i in range (6) ]
for col in range ( 6 ) :
    row_sum = 0.
    for row in range ( 6 ) :
        temp = [ ListOfMatrices[i][row][col] for i in range ( 5 ) ]
        mean [row][col] = int ( np.mean ( temp ) )
        row_sum = row_sum + mean [row][col]
        std [row][col] = int ( np.std ( temp ) + 0.9999 )
    for row in range ( 6 ) : 
        temp = [ ListOfMatrices[i][row][col] for i in range ( 5 ) ]
        mean_normalized [row][col] = np.mean ( temp ) / row_sum
        std_normalized [row][col] = np.std ( temp ) / row_sum
mean = np.array ( mean )
mean_normalized = np.array ( mean_normalized )
std_normalized = np.array ( std_normalized )
std = np.array ( std )
fig, picture = plt.subplots() 
picture.imshow ( mean , interpolation='nearest' , cmap=plt.cm.viridis , vmin=0 , vmax=32000 )

#Write down axis names
picture.set_title ( "Confusion Matrix" )
picture.set_xticks ( [x for x in range(0,6)] )
picture.set_xticklabels ( ['1p0n','1p1n','1pXn','3p0n','3pXn','Other'] )
picture.set_yticks ( [x for x in range(0,6)] )
picture.set_yticklabels ( ['Other','3pXn','3p0n','1pXn','1p1n','1p0n'] )

#Show both labels
picture.xaxis.set_tick_params(labeltop=True)
picture.set_xlabel( "True decay mode" )
picture.yaxis.set_tick_params(labeltop=True)
picture.set_ylabel( "Reconstructed decay mode" )

#Create list containing all the text fragments in order to be able to make them invisible later on
texts = []

# Show text in bins
for i in range( 6 ):
    for j in range( 6 ):
        if mean[i,j] < 15000 :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="w", ha="center", va="center", fontweight="bold") )
        else :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="k", ha="center", va="center", fontweight="bold") )
fig.show()
plt.savefig ("ConfusionMatrix.png" , dpi = dpi )
# Clear text
for txt in texts : 
    txt.set_visible ( False )
    #txt.remove()



picture.set_title ( "Confusion Matrix, normalized within each true category" )
picture.imshow ( mean_normalized , interpolation='nearest' , cmap=plt.cm.viridis , vmin=0 , vmax=1 )
# Show text in bins
for i in range( 6 ):
    for j in range( 6 ):
        if mean_normalized[i,j] < 0.5 :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="w", ha="center", va="center", fontweight="bold") )
        else :
            texts.append ( picture.text(j,i, f'{mean[i,j]} \n$\pm$ {std[i,j]}', 
                color="k", ha="center", va="center", fontweight="bold") )
fig.show()
plt.savefig ("ConfusionMatrix_normalized.png" , dpi = dpi )
# Clear text
for txt in texts : 
    txt.set_visible ( False )
    #txt.remove()

picture.imshow ( mean_normalized , interpolation='nearest' , cmap=plt.cm.viridis , vmin=0 , vmax=1 )
# Show normalized text in bins
for i in range( 6 ):
    for j in range( 6 ):
        if mean_normalized[i,j] < 0.5 :
            texts.append ( picture.text(j,i, '%2.1f%%\n$\pm$%1.1f%%' % ( mean_normalized[i,j] * 100 , std_normalized[i,j] * 100 ) , 
                color="w", ha="center", va="center", fontweight="bold") )
        else :
            texts.append ( picture.text(j,i, '%2.1f%%\n$\pm$%1.1f%%' % ( mean_normalized[i,j] * 100 , std_normalized[i,j] * 100 ) , 
                color="k", ha="center", va="center", fontweight="bold") )
fig.show()
plt.savefig ("ConfusionMatrix_normalized_normalizedText.png" , dpi = dpi )
# Clear text
for txt in texts : 
    txt.set_visible ( False )
    #txt.remove()