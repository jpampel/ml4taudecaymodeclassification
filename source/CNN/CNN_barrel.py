# importing libraries 
import tensorflow as tf
# the following forces tensorflow to only use one cpu kernel per calculation in order to enable usage on clusters with many cpus.
NUM_THREADS=1
tf.config.threading.set_inter_op_parallelism_threads(NUM_THREADS)
tf.config.threading.set_intra_op_parallelism_threads(NUM_THREADS)
import tensorflow.keras as K 
import numpy as np
import sys
import os
import multiprocessing as mp
import sklearn.model_selection as scikit
import sklearn
import math
import tensorflow_addons as tfa

def getBinning ( sampling ):
    # EMB1
    if sampling == "EMB1" or sampling == 0:
        return 128 , 4
    # EMB2
    if sampling == "EMB2" or sampling == 1:
        return 16 , 16
    # EMB3
    if sampling == "EMB3" or sampling == 2:
        return 8 , 16
    #Tile0
    if sampling == "Tile1" or sampling == 3:
        return 4 , 4
    #Tile1
    if sampling == "Tile2" or sampling == 4:
        return 4 , 4
    #Tile2
    if sampling == "Tile3" or sampling == 5:
        return 2 , 4
    # additional
    if sampling == "additional" or sampling == 6:
        return 1 , 4

    if sampling == "max":
        return 128 , 16

def upscaleToSameSizeConv2DModelVariablePoolStrideSize ( tracking , deepLearning , denseLayerSize , layerThicknessFactor , finalXSize , finalYSize , strideSizeFactor , convolutionNumber , l1_regularization , l2_regularization ):
    # Define the form of our inputs.
    input_EMB1 = K.layers.Input ( shape = ( getBinning("EMB1")[0], getBinning("EMB1")[1], 1 ) , name = "emb1" )
    input_EMB2 = K.layers.Input ( shape = ( getBinning("EMB2")[0], getBinning("EMB2")[1], 1 ) , name = "emb2" )
    input_EMB3 = K.layers.Input ( shape = ( getBinning("EMB3")[0], getBinning("EMB3")[1], 1 ) , name = "emb3" )
    input_Tile1 = K.layers.Input ( shape = ( getBinning("Tile1")[0], getBinning("Tile1")[1], 1 ) , name = "Tile1" )
    input_Tile2 = K.layers.Input ( shape = ( getBinning("Tile2")[0], getBinning("Tile2")[1], 1 ) , name = "Tile2" )
    input_Tile3 = K.layers.Input ( shape = ( getBinning("Tile3")[0], getBinning("Tile3")[1], 1 ) , name = "Tile3" )
    input_additional = K.layers.Input ( shape = ( getBinning("additional")[1] ) , name = "additional" )
    # Extra layer simulating the existence of tracking information by adding the number of charged pions.
    if ( tracking == True ) : 
        input_tracking = K.layers.Input ( shape = ( 1 ) , name = "tracking" )

    # Resize the layers to the size nPixelsX * nPixelsY.
    initialX , initialY = getBinning ( "max" ) [0] , getBinning ( "max" ) [1]
    resizedx_EMB1 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialX // getBinning ( "EMB1" ) [0] , axis = 1 ) , name = "EMB1_resizedx" ) ( input_EMB1 )
    resized_EMB1 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialY // getBinning ( "EMB1" ) [1] , axis = 2 ) , name = "EMB1_resized" ) ( resizedx_EMB1 )
    resizedx_EMB2 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialX // getBinning ( "EMB2" ) [0] , axis = 1 ) , name = "EMB2_resizedx" ) ( input_EMB2 )
    resized_EMB2 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialY // getBinning ( "EMB2" ) [1] , axis = 2 ) , name = "EMB2_resized" ) ( resizedx_EMB2 )
    resizedx_EMB3 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialX // getBinning ( "EMB3" ) [0] , axis = 1 ) , name = "EMB3_resizedx" ) ( input_EMB3 )
    resized_EMB3 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialY // getBinning ( "EMB3" ) [1] , axis = 2 ) , name = "EMB3_resized" ) ( resizedx_EMB3 )
    resizedx_Tile1 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialX // getBinning ( "Tile1" ) [0] , axis = 1 ) , name = "Tile1_resizedx" ) ( input_Tile1 )
    resized_Tile1 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialY // getBinning ( "Tile1" ) [1] , axis = 2 ) , name = "Tile1_resized" ) ( resizedx_Tile1 )
    resizedx_Tile2 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialX // getBinning ( "Tile2" ) [0] , axis = 1 ) , name = "Tile2_resizedx" ) ( input_Tile2 )
    resized_Tile2 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialY // getBinning ( "Tile2" ) [1] , axis = 2 ) , name = "Tile2_resized" ) ( resizedx_Tile2 )
    resizedx_Tile3 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialX // getBinning ( "Tile3" ) [0] , axis = 1 ) , name = "Tile3_resizedx" ) ( input_Tile3 )
    resized_Tile3 = K.layers.Lambda (lambda x: tf.keras.backend.repeat_elements ( x = x , rep = initialY // getBinning ( "Tile3" ) [1] , axis = 2 ) , name = "Tile3_resized" ) ( resizedx_Tile3 )

    # After convolutionNumber many steps of pooling, the size shall be finalXSize x finalYSize.
    # For this, the poolSize in x direction in each step has to be the convolutionNumber'th square-root of initialXSize / finalXSize.
    # Same for y.

    # Concatenate to 3D-Picture of size nPixelsX * nPixelsY * 6.
    combined = K.layers.Concatenate ( axis = 3 ) ( [ resized_EMB1 , resized_EMB2 , resized_EMB3 , resized_Tile1 , resized_Tile2 , resized_Tile3 ] )
    
    zAxisLength = 6 # At the beginning, there are 6 pictures, so the z-axis has length 6.
    xAxisLength , yAxisLength = initialX , initialY
    stridesX , stridesY = 1 , 1
    for i in range ( convolutionNumber ) :
        # If it is possible without making the xAxisLength too small, the strides are allowed to be changed:
        if xAxisLength // finalXSize >= 2 * ( 1 + strideSizeFactor ) :
            # rounding to the fourth digit is done here in order to deal neglect small floating point errors
            poolSizeX = int ( round ( math.pow ( xAxisLength // finalXSize , 1 / ( 1 + strideSizeFactor ) / ( convolutionNumber - i ) ) , 4 ) )
            stridesX = int ( max ( strideSizeFactor * poolSizeX , 1 ) )
        # The pool size shall be the ( convolutionNumber - i )'th square root of the remaining change of size xAxisLength // finalXSize.
        elif xAxisLength // finalXSize >= 2 :
            poolSizeX = int ( round ( math.pow ( xAxisLength // finalXSize , 1 / ( convolutionNumber - i ) ) , 4 ) )
        else :
            poolSizeX = 1
            
        # If it is possible without making the xAxisLength too small, the strides are allowed to be changed:
        if yAxisLength // finalYSize >= 2 * ( 1 + strideSizeFactor ) :
            poolSizeY = int ( round ( math.pow ( yAxisLength // finalYSize , 1 / ( 1 + strideSizeFactor ) / ( convolutionNumber - i ) ) , 4 ) )
            stridesY = int ( max ( strideSizeFactor * poolSizeY , 1 ) )
        # The pool size shall be the ( convolutionNumber - i )'th square root of the remaining change of size yAxisLength // finalYSize.
        elif yAxisLength // finalYSize >= 2 :
            poolSizeY = int ( round ( math.pow ( yAxisLength // finalYSize , 1 / ( convolutionNumber - i ) ) , 4 ) )
        else :
            poolSizeY = 1
            
        # The length of the axis is adjusted accordingly.
        xAxisLength = math.floor ( xAxisLength / float ( poolSizeX * stridesX ) )
        yAxisLength = math.floor ( yAxisLength / float ( poolSizeY * stridesY ) )

        # The new length of the z axis shall be determined by the pool size and the layer thickness value. It shall never become 0, hence the usage of math.ceil.
        zAxisLength = math.ceil ( zAxisLength * poolSizeX * stridesX * poolSizeY * stridesY * layerThicknessFactor ) 
        
        combined = K.layers.Conv2D ( 
            zAxisLength , 
            ( poolSizeX , poolSizeY ) , 
            strides = ( stridesX , stridesY ) , # ( int ( strideSizeFactor * poolSizeX + 1 ) , int ( strideSizeFactor * poolSizeY + 1 ) ) , 
            activation = 'relu' , padding = "same" , 
            kernel_regularizer = K.regularizers.L1L2 ( l1 = l1_regularization , l2 = l2_regularization )
        ) ( combined )
        combined = K.layers.MaxPooling2D ( pool_size = ( poolSizeX , poolSizeY ) , padding = 'same' ) ( combined )
    
    combined = K.layers.Flatten ( ) ( combined )
    combined = K.layers.Concatenate ( axis = 1 ) ( [ combined , input_additional ] )
    if ( tracking == True ) :
        combined = K.layers.Concatenate ( axis = 1 ) ( [ combined , input_tracking ] )
    # Adding additional layers if deep Learning shall be activated.
    for i in range ( deepLearning ) :
        combined = K.layers.Dense ( denseLayerSize , activation = 'relu' , kernel_regularizer = K.regularizers.L1L2 ( l1 = l1_regularization , l2 = l2_regularization ) ) ( combined )
    combined = K.layers.Dense ( 6 , activation = 'softmax' , name = "Decay_Mode" ) ( combined ) # activation = 'sigmoid' produces results in the interval [0,1)

    # Define the Model.
    if ( tracking == True ) :
        model = K.Model ( inputs = [ input_EMB1 , input_EMB2 , input_EMB3 , input_Tile1 , input_Tile2 , input_Tile3 , input_additional , input_tracking ] , outputs = combined )
        return model
    model = K.Model ( inputs = [ input_EMB1 , input_EMB2 , input_EMB3 , input_Tile1 , input_Tile2 , input_Tile3 , input_additional ] , outputs = combined )
    return model

def getData ( train_data_dir , seed , test_ratio , tracking ):

    # This method returns two tuples of datasets - train_data and test_data - and the corresponding labels.
    # test_ratio = #test_data / #all_data
    # seed is a randomizer

    print ( "Getting dataset..." )
    EMB1 = np.load ( train_data_dir + "EMB1" + ".npy" )# / 255.
    EMB2 = np.load ( train_data_dir + "EMB2" + ".npy" )# / 255.
    EMB3 = np.load ( train_data_dir + "EMB3" + ".npy" )# / 255.
    Tile1 = np.load ( train_data_dir + "Tile1" + ".npy" )# / 255.
    Tile2 = np.load ( train_data_dir + "Tile2" + ".npy" )# / 255.
    Tile3 = np.load ( train_data_dir + "Tile3" + ".npy" )# / 255.
    labels = np.load ( train_data_dir + "labels" + ".npy" )
    additional = np.load ( train_data_dir + "additional" + ".npy" )
    if not ( EMB1.shape[0] == EMB2.shape[0] == EMB3.shape[0] == Tile1.shape[0] == Tile2.shape[0] == Tile3.shape[0] == labels.shape[0] == additional.shape[0] ) :
        print ( "FATAL ERROR: not all data arrays have the same length:" )
        print ( "EMB1.shape = " + str ( EMB1.shape ) )
        print ( "EMB2.shape = " + str ( EMB2.shape ) )
        print ( "EMB3.shape = " + str ( EMB3.shape ) )
        print ( "Tile1.shape = " + str ( Tile1.shape ) )
        print ( "Tile2.shape = " + str ( Tile2.shape ) )
        print ( "Tile3.shape = " + str ( Tile3.shape ) )
        print ( "labels.shape = " + str ( labels.shape ) )
        print ( "additional.shape = " + str ( additional.shape ) )
        sys.exit( "FATAL ERROR: not all data arrays have the same length." )

    # Add simulated tracking info, i.e. the truth number of tracks (which is still less useful than actual tracks)
    if ( tracking == True ) :
        tracking_data = labels // 3

    # Prepare the labels for categorical crossentropy
    labels = K.utils.to_categorical ( labels , num_classes = 6 )

    # Add tracking info
    if ( tracking == True ) :
        train_EMB1 , test_EMB1 , train_EMB2 , test_EMB2 , train_EMB3 , test_EMB3 , train_Tile1 , test_Tile1 , train_Tile2 , test_Tile2 , train_Tile3 , test_Tile3 , train_additional , test_additional , train_tracking , test_tracking , train_labels , test_labels = scikit.train_test_split ( EMB1 , EMB2 , EMB3 , Tile1 , Tile2 , Tile3 , additional , tracking_data , labels , test_size = test_ratio , random_state = seed , shuffle = True )
        return [ train_EMB1 , train_EMB2 , train_EMB3 , train_Tile1 , train_Tile2 , train_Tile3 , train_additional , train_tracking ] , train_labels , [ test_EMB1 , test_EMB2 , test_EMB3 , test_Tile1 , test_Tile2 , test_Tile3 , test_additional , test_tracking ], test_labels
        
    train_EMB1 , test_EMB1 , train_EMB2 , test_EMB2 , train_EMB3 , test_EMB3 , train_Tile1 , test_Tile1 , train_Tile2 , test_Tile2 , train_Tile3 , test_Tile3 , train_additional , test_additional , train_labels , test_labels = scikit.train_test_split ( EMB1 , EMB2 , EMB3 , Tile1 , Tile2 , Tile3 , additional , labels , test_size = test_ratio , random_state = seed , shuffle = True )
    return [ train_EMB1 , train_EMB2 , train_EMB3 , train_Tile1 , train_Tile2 , train_Tile3 , train_additional ] , train_labels , [ test_EMB1 , test_EMB2 , test_EMB3 , test_Tile1 , test_Tile2 , test_Tile3 , test_additional ], test_labels

def evaluateModel ( queue , 
                    train_data , train_labels , validation_data , validation_labels , test_data , test_labels , 
                    steps_per_epoch , validation_steps , batch_size , epochs , patience ,
                    fold_no , job_ID , maxRuntimeHours ,
                    tracking , class_weight , deepLearning , denseLayerSize , layerThicknessFactor , finalXSize , finalYSize , strideSizeFactor , convolutionNumber ,
                    l1_regularization , l2_regularization ):
    tf.random.set_seed ( fold_no )
    # Create new and untouched model
    model , modelName = upscaleToSameSizeConv2DModelVariablePoolStrideSize ( tracking , deepLearning , denseLayerSize , layerThicknessFactor , finalXSize , finalYSize , strideSizeFactor , convolutionNumber , l1_regularization , l2_regularization ) , "upscaleToSameSizeConv2DModelVariablePoolStrideSize"
    modelName = modelName + "_barrelFinal"


    if ( fold_no == 0 ):
        print ( "The " + modelName + " looks as follows:" )
        model.summary ( )
        print ( f"l1_regularization = {l1_regularization} , l2_regularization = {l2_regularization}" )
        trainableParams = np.sum ( [ np.prod ( v.get_shape ( ) ) for v in model.trainable_weights ] )
        queue.put ( trainableParams )

    # Compile model 
    model.compile ( loss = K.losses.CategoricalCrossentropy() ,
                optimizer = 'Adam',
                metrics = [ 'accuracy' ] ,
                weighted_metrics = [ 'accuracy' ] ) 
    
    # only in this final step, EarlyStopping is deactivated.
    #######################################################################################################################################
    #######################################################################################################################################
    #######################################################################################################################################
    # Define a Callback for early stopping that minimizes the validation loss.
    #EarlyStopping = K.callbacks.EarlyStopping ( monitor = 'val_loss' ,     # We want the validation loss ...
    #                                 mode = 'min' ,             # ... to be minimized.
    #                                 #min_delta = 0.002 ,        # Improvement means that the value decreases at least by 'min_delta'.
    #                                 patience = patience ,            # The algorithm stops after 'patience' steps of no improvement.
    #                                 verbose = 1                # We want outputs stating when the model stops.
    #                               )
    #######################################################################################################################################
    #######################################################################################################################################
    #######################################################################################################################################

    # Define a Checkpoint that saves the best model with regards of the validation set.
    modelName = "parameter_scan/" + modelName + f"_job{job_ID}"
    try:
        os.mkdir( modelName )
    except OSError:
        print ( "Skipping creation of " + modelName + " because it already exists." )

    ModelCheckpoint = K.callbacks.ModelCheckpoint ( modelName + f'/best_model_fold_no_{fold_no}.h5' ,        # Name of the file in which to save the checkpoint.
                                       monitor='val_loss' ,     # We want the validation loss ...
                                       mode = 'min' ,           # ... to be minimized.
                                       save_best_only = True ,  # Only the best model shall be saved.
                                       verbose = 0             # We don't want outputs.
                                     )

    # Define a Callback that stops the algorithm if the allocated time limit is reached (minus two hours of safety distance).
    TimeStopping = tfa.callbacks.TimeStopping ( seconds = 3600 * ( maxRuntimeHours - 12 ) , 
                                                verbose = 1 )
    training_weights = np.array ( [ class_weight[np.argmax ( label )] for label in train_labels ] )
    validation_weights = np.array ( [ class_weight[np.argmax ( label )] for label in validation_labels ] )
    test_weights = np.array ( [ class_weight[np.argmax ( label )] for label in test_labels ] )
    print ( "Finished creating weigths" )

    # Evaluate the model on our current train and validation data.
    history = model.fit ( train_data , train_labels , sample_weight = training_weights ,
        batch_size = batch_size , 
        steps_per_epoch = steps_per_epoch , 
        epochs = epochs , 
        validation_data = ( validation_data , validation_labels , validation_weights ) ,
        validation_steps = validation_steps ,
        verbose = 0 , #2 for outputs per epoch.
        callbacks = [ ModelCheckpoint , TimeStopping ] )#[ EarlyStopping , ModelCheckpoint , TimeStopping ] )
    print ( "Finished training" )
    print ( history.history.keys () )
    print ( 'training loss history' )
    print ( history.history ['loss'] )
    print ( 'validation loss history' )
    print ( history.history ['val_loss'] )
    print ( 'training accuracy history' )
    print ( history.history ['accuracy'] )
    print ( 'validation accuracy history' )
    print ( history.history ['val_accuracy'] , flush = True )

    #delete the latest model
    K.backend.clear_session()

    # load the best model
    model = K.models.load_model ( modelName + f'/best_model_fold_no_{fold_no}.h5' )

    # Print confusion matrix
    predicted_test_labels = model.predict ( test_data , verbose = 2 )
    y_pred = np.argmax ( predicted_test_labels , axis = 1 )
    y_test = np.argmax ( test_labels , axis = 1 )
    disp = sklearn.metrics.confusion_matrix ( y_test , y_pred )
    print ( disp )
    
    # Print and save the scores for our model evaluation.
    scores = model.evaluate ( test_data , test_labels , sample_weight = test_weights , verbose=0 )
    print ( f'Score for fold {fold_no}: {model.metrics_names[0]} of {scores[0]}; {model.metrics_names[1]} of {scores[1]*100}%; {model.metrics_names[2]} of {scores[2]*100}%' )
    queue.put ( scores )
    queue.put ( disp )
    #queue.put ( model ) queue.put ( history )
    queue.close () # Tells the parent process that this process will not put any more data onto the queue.
    return

def main ( ) :
    try:
        job_ID = int ( sys.argv [1] )
    except IndexError:
        sys.exit ( "Missing first argument, the Job ID.\nUsage: CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>" )
    except ValueError:
        sys.exit ( "First argument, the Job ID, must be an integer.\nUsage: CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>" )
    print ( f'Job ID: {job_ID}' )

    try:
        clusterID = int ( sys.argv [2] )
    except IndexError:
        sys.exit ( "Missing second argument, clusterID.\nUsage: CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>" )
    except ValueError:
        sys.exit ( "clusterID must be an integer.\nUsage: CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>" )
    print ( f'clusterID: {clusterID}' )

    try:
        maxRuntimeHours = int ( sys.argv [3] )
    except IndexError:
        sys.exit ( "Missing third argument, maxRuntimeHours.\nUsage: CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>" )
    except ValueError:
        sys.exit ( "maxRuntimeHours must be an integer.\nUsage: CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>" )
    print ( f'maxRuntimeHours: {maxRuntimeHours}' )

    try:
        os.mkdir( "parameter_scan" )
    except OSError:
        print ( "Skipping creation of parameter_scan because it already exists." )


    train_data_dir = 'testImages_barrel/'
    nb_train_samples = 8192         # The number of steps per epoch is nb_train_samples // batch_size
    nb_validation_samples = 10000   # Only important if no crossvalidation is done, i.e. num_folds is set to 1.
    num_folds = 5                   # k in k-fold crossvalidation
    nb_test_samples = 100000        # Number of test samples. The total sample number is roughly 580k.
                                    # If this is a float <1 then this is the fraction that the test samples are of all samples.
    epochs = 10000                  # Number of epochs to be run.
    patience = 50                   # Number of epochs with no improvement of validation score before early stopping.

    tracking = False                # True if the number of charged pions shall be added in order to 'simulate the existence of tracking info'.
    reweighting = False             # True if the classes shall be reweighted in order to make them 'equally big' from the perspective of the network.
    batch_size = 32 #32
    deepLearning = 0                # Can be 0,1,2 and determines the number of extra dense layers.
    denseLayerSize = 128            # Determines the number of nodes in each dense layer.
    layerThicknessFactor = 0.5      # In each convolution layer, the third dimension (starting at 6 due to the 6 calorimeter images) 
                                    # is multiplied by xPooling * yPooling * layerThicknessFactor
    strideSizeFactor = 1            # The size of striding in x direction is int ( strideSizeFactor * poolSizeX + 1 ).
    finalXSize = 4                  # Length of the x-axis in pixels of the images after convoluting
    finalYSize = 4                  # Length of the y-axis in pixels of the images after convoluting
                                    # These determine the pool size xPooling and yPooling.
    convolutionNumber = 5           # Number of concatination layers

    # Parameter-Scan: Depending on the job number, which is the input of this python script,
    # the parameters are chosen.
    index = 0

    #import random
    #random.seed ( 1000 * clusterID + job_ID )
    #job_ID_random = random.randint ( 0 , 10 )
    #job_ID_random = job_ID
    best_index_for_48h_runtime = [ 9 ]
    job_ID_random = best_index_for_48h_runtime[job_ID]

    print (f"job_ID_random = {job_ID_random}")

    job_ID_set = False
    for temp_tracking in [ False ] : #[ False, True ] :
        for temp_reweighting in [ False ] : #[ False, True ] :
            for temp_finalXSize , temp_finalYSize in [ ( 8 , 2 ) ] :
                for temp_batch_size in [ 512 ] : 
                    for temp_deepLearning , temp_convolutionNumber in [ ( 4 , 4 ) , ( 6 , 3 ) , ( 6 , 4 ) , ( 8 , 3 ) , ( 8 , 4 ) ] : # deepLearning = 0 means no dense layer
                        for temp_denseLayerSize in [ 256 ] : 
                            for temp_layerThicknessFactor in [ 0.7 ] : 
                                for temp_strideSizeFactor in [ 0 ] : # 0 is the best option
                                    for temp_l1_regularization in [ 0.00001 ] :
                                        for temp_l2_regularization in [ 0 , 0.00005 ] :
                                            if index == job_ID_random :
                                                tracking = temp_tracking
                                                reweighting = temp_reweighting
                                                batch_size = temp_batch_size
                                                deepLearning = temp_deepLearning
                                                denseLayerSize = temp_denseLayerSize
                                                layerThicknessFactor = temp_layerThicknessFactor
                                                strideSizeFactor = temp_strideSizeFactor
                                                finalXSize = temp_finalXSize
                                                finalYSize = temp_finalYSize
                                                convolutionNumber = temp_convolutionNumber
                                                l1_regularization = temp_l1_regularization
                                                l2_regularization = temp_l2_regularization
                                                job_ID_set = True
                                            index = index + 1
    print ( f"number of parameter possibilities: {index}" )
    if job_ID_set == False :
        print ( f"Job_ID_random = {job_ID_random} out of bounds of parameter list. Aborting")
        sys.exit ( -1 )



    # The weights with which the classes are reweighted. If these are all 1, then nothing is changed. 
    # With the reweighting values, the classes are roughly the same size (from the perspective of the neural network)
    if reweighting :
        class_weight = { 0 : 2 ,    # 7903 samples in this category
                         1 : 1 ,    # 16771 samples in this category
                         2 : 2 ,    # 6663 samples in this category
                         3 : 2 ,    # 6360 samples in this category
                         4 : 4 ,    # 3376 samples in this category
                         5 : 1 }    # 56  samples in this category, ignore this for now...
    else : 
        class_weight = { 0 : 1 , 
                         1 : 1 , 
                         2 : 1 , 
                         3 : 1 , 
                         4 : 1 , 
                         5 : 1 }

    # Get Data
    train_data , train_labels , test_data , test_labels = getData ( train_data_dir = train_data_dir , 
                                                                    test_ratio = nb_test_samples ,
                                                                    seed = 42 ,
                                                                    tracking = tracking )
    
    # Define per-fold score containers and utilities for multiprocessing
    acc_per_fold = []
    weighted_acc_per_fold = []
    loss_per_fold = []
    fold_numbers = []
    processes = [] # The processes that are about to evaluate the model on data.
    queues = []    # The queues in which the returned values are saved.
    confusion_matrices = []

    # Define the K-fold Cross Validator
    # If tracking info is added (i.e. tracking == True), these lines add the corresponding additional data input layer for the neural networks.
    AdditionalTrackingIndex = 0 
    if ( tracking == True ) :
        AdditionalTrackingIndex = 1
    
    # Now, the jobs need to be run in the function "evaluateModel", first in the case that no crossvalidation is done...
    if ( num_folds == 1 ) : 
        validation_data = [ i for i in range ( len(train_data) ) ]
        for i in range ( len(train_data) ) : 
            train_data[i] , validation_data[i] = scikit.train_test_split ( train_data[i] , test_size = nb_validation_samples , random_state = 42 , shuffle = True )
        train_labels , validation_labels = scikit.train_test_split ( train_labels , test_size = nb_validation_samples , random_state = 42 , shuffle = True )
        fold_no = 0
        queues.append ( mp.Queue() )
        kwargs = { # Define the arguments for the job
                "queue" :               queues[fold_no] ,
                "train_data" : train_data , "train_labels" : train_labels , "validation_data" : validation_data , "validation_labels" : validation_labels , "test_data" : test_data , "test_labels" : test_labels ,
                "steps_per_epoch" :     nb_train_samples // batch_size ,
                "validation_steps" :    nb_validation_samples // batch_size , 
                "batch_size" : batch_size , "epochs" : epochs , "fold_no" : fold_no , "job_ID" : job_ID , "maxRuntimeHours" : maxRuntimeHours , "patience" : patience ,
                "tracking" : tracking , "class_weight" : class_weight , "deepLearning" : deepLearning , "denseLayerSize" : denseLayerSize , 
                "layerThicknessFactor" : layerThicknessFactor , "strideSizeFactor" : strideSizeFactor , "finalXSize" : finalXSize , "finalYSize" : finalYSize , "convolutionNumber" : convolutionNumber ,
                "l1_regularization" : l1_regularization , "l2_regularization" : l2_regularization
               }
        processes.append (
            mp.Process (    target = evaluateModel , 
                            kwargs = kwargs # Hand over the arguments to the job.
            )
        )
        print ( "Starting process number " + str (fold_no) + "." )
        processes[fold_no].start()

    elif ( num_folds < 1 ) :
        print ( "ERROR: num_folds < 1" )
        sys.exit ( -1 )
    
    # ... then in the case of k-fold crossvalidation.
    else :
        kfold = scikit.KFold ( n_splits = num_folds , shuffle = False )

        # K-fold Cross Validation model evaluation
        fold_no = 0
        
        for train_index, validation_index in kfold.split ( train_labels ):
            queues.append ( mp.Queue() )
            kwargs = { # Define the arguments for the job
                "queue" :               queues[fold_no] ,
                "train_data" :          [ channel[train_index] for channel in train_data ] , 
                "train_labels" :        train_labels[train_index] , 
                "validation_data" :     [ channel[validation_index] for channel in train_data ] ,
                "validation_labels" :   train_labels[validation_index] , 
                "test_data" :           test_data , 
                "test_labels" :         test_labels ,
                "steps_per_epoch" :     nb_train_samples // batch_size ,
                "validation_steps" :    nb_validation_samples // batch_size , 
                "batch_size" : batch_size , "epochs" : epochs , "fold_no" : fold_no , "job_ID" : job_ID , "maxRuntimeHours" : maxRuntimeHours , "patience" : patience ,
                "tracking" : tracking , "class_weight" : class_weight , "deepLearning" : deepLearning , "denseLayerSize" : denseLayerSize, 
                "layerThicknessFactor" : layerThicknessFactor , "strideSizeFactor" : strideSizeFactor , "finalXSize" : finalXSize , "finalYSize" : finalYSize , "convolutionNumber" : convolutionNumber ,
                "l1_regularization" : l1_regularization , "l2_regularization" : l2_regularization
               }
            processes.append (
                mp.Process (    target = evaluateModel , 
                                kwargs = kwargs # Hand over the arguments to the job.
                )
            )
            print ( "Starting process number " + str (fold_no) + "." )
            processes[fold_no].start()
            fold_no = fold_no + 1
    
    trainableParams = queues[0].get()
    for fold_no in range(num_folds):
        scores_temp = queues[fold_no].get()
        confusion_matrices.append ( queues[fold_no].get() ) 
        loss_per_fold.append ( scores_temp[0] )
        acc_per_fold.append ( scores_temp[1] )
        weighted_acc_per_fold.append ( scores_temp[2] )
        fold_numbers.append ( fold_no )
        processes[fold_no].join()
        print ( "Joined process number " + str (fold_no) + "." )
        processes[fold_no].close()
        print ( "Closed process number " + str (fold_no) + "." )
        

    # == Provide average scores ==
    print('------------------------------------------------------------------------------------------------------------')
    print('Score per fold')
    for i in range(0, len(acc_per_fold)):
        print('------------------------------------------------------------------------------------------------------------')
        print(f'> Fold {i} - Loss: {loss_per_fold[i]} - Accuracy: {acc_per_fold[i]}% - Weighted Accuracy: {weighted_acc_per_fold[i]}%')
    print('------------------------------------------------------------------------------------------------------------')
    print('Average scores for all folds:')
    print(f'> Accuracy: {np.mean(acc_per_fold)} (+- {np.std(acc_per_fold)})')
    print(f'> Weighted Accuracy: {np.mean(weighted_acc_per_fold)} (+- {np.std(weighted_acc_per_fold)})')
    print(f'> Loss: {np.mean(loss_per_fold)}')
    print('------------------------------------------------------------------------------------------------------------')

    # Calculate the average confusion matrix
    # Sum:
    avg_confusion_matrix = confusion_matrices [0]
    for i in range ( 1 , len ( confusion_matrices ) ) :
        avg_confusion_matrix = avg_confusion_matrix + confusion_matrices [i]
    # Average:
    avg_confusion_matrix = avg_confusion_matrix / len ( confusion_matrices )

    # Calculate the standard derivative of the confusion matrices (not the error of the average!)
    # Sum of squares:
    std_confusion_matrix = ( confusion_matrices [0] - avg_confusion_matrix ) * ( confusion_matrices [0] - avg_confusion_matrix )
    for i in range ( 1 , len ( confusion_matrices ) ) :
        std_confusion_matrix = std_confusion_matrix + ( confusion_matrices [i] - avg_confusion_matrix ) * ( confusion_matrices [i] - avg_confusion_matrix )
    # Standard derivative:
    std_confusion_matrix = np.uint32 ( np.sqrt ( std_confusion_matrix / len ( confusion_matrices ) ) + 0.9999 )

    # Print confusion matrix and standard derivative
    avg_confusion_matrix = np.uint32 ( avg_confusion_matrix )
    print ( "The average confusion matrix and the fluctuations of it are:")
    print ( avg_confusion_matrix )
    print ( "+-" )
    print ( std_confusion_matrix )

    print ( f"jobnumber ; job_ID_random ; tracking ; reweighting ; batch_size ; deepLearning ; denseLayerSize ; layerThicknessFactor ; strideSizeFactor ; finalXSize ; finalYSize ; convolutionNumber ; l1_regularization ; l2_regularization ; Accuracy ; +- Std; Weighted Accuracy ; +- Std ; trainableParams" )
    print ( f"{job_ID} ; {job_ID_random} ; {tracking} ; {reweighting} ; {batch_size} ; {deepLearning} ; {denseLayerSize} ; {layerThicknessFactor} ; {strideSizeFactor} ; {finalXSize} ; {finalYSize} ; {convolutionNumber} ; {l1_regularization} ; {l2_regularization} ; {np.mean(acc_per_fold)} ; {np.std(acc_per_fold)} ; {np.mean(weighted_acc_per_fold)} ; {np.std(weighted_acc_per_fold)} ; {trainableParams}" )

    # Print the results to a csv-file.
    with open ( f"parameter_scan/output{clusterID}.csv" , "a" ) as myfile : # "a" means that the line is appended
        myfile.write ( f"{job_ID} ; {job_ID_random} ; {tracking} ; {reweighting} ; {batch_size} ; {deepLearning} ; {denseLayerSize} ; {layerThicknessFactor} ; {strideSizeFactor} ; {finalXSize} ; {finalYSize} ; {convolutionNumber} ; {l1_regularization} ; {l2_regularization} ; {np.mean(acc_per_fold)} ; {np.std(acc_per_fold)} ; {np.mean(weighted_acc_per_fold)} ; {np.std(weighted_acc_per_fold)} ; {trainableParams} \n" )


if __name__ == '__main__':
    main ()