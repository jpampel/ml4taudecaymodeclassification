# Setup
This code can run without using any specific ATLAS dataset.
A small number of Python libraries are needed, and can be installed as follows:
```
pip3 install --user (--upgrade) numpy tensorflow sklearn tensorflow_addons 
```
If any of these packages are already installed, you should include the --upgrade tag in the pip command. This code was developed using the following versions:
- tensorflow: 2.13.0
- numpy: 1.24.1
- sklearn: 1.2.0
- tensorflow_addons: 0.21.0

# Usage
This code takes as input the .npy files produced by the CaloImages script in the other folder of this Github. These are supposed to be saved in a subfolder of the execution folder. The name of said subfolder should be testImages_barrel/ resp. testImages_endcaps/, the same as the CaloImages framework produces, so just run these scripts from the same folder as the CaloImages script.
To run this code do the following:
```
python3 CNN_barrel.py <jobID> <clusterID> <maxRuntimeHours>
python3 CNN_endcaps.py <jobID> <clusterID> <maxRuntimeHours>
```
Make sure that maxRuntimeHours is always > 12 because 12 hours are specified to be used for making the output (most likely, this is too much). For testing purposes, maxRuntimeHours can be set to exactly 12, then the model only trains for exactly one epoch.
