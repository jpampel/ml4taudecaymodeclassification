In order to download the testimages_barrel and testimages_endcaps folders in the run subfolder, Git LFS is needed. 
This can be installed following the guide on https://docs.gitlab.com/ee/topics/git/lfs/.

Additional information can be found in the README.me files in the subfolders of the source folder.